#ifdef __0
int fpga_write_beep_ctrl(int value)
{
   int ret=0;
   int aux=0;


    ret = fpga_write_reg(GENERAL_IO_BEEP_OFFSET, UIO_AXI_GENERAL_IO, 0);

    if(value)
        aux = (1 << (value - 1));

    ret += fpga_write_reg(GENERAL_IO_BEEP_OFFSET, UIO_AXI_GENERAL_IO, aux);
   return ret;
}

int fpga_read_beep_ctrl(int *value)
{

   return fpga_read_reg(GENERAL_IO_BEEP_OFFSET, UIO_AXI_GENERAL_IO, value);
}

int fpga_write_watchdog_ctrl(int value)
{


    return fpga_write_reg(WATCHDOGS_CTRL_REG_OFFSET, UIO_AXI_WATCHDOGS, value);
 }

int fpga_read_watchdog_ctrl(int *value)
{

    return fpga_read_reg(WATCHDOGS_CTRL_REG_OFFSET, UIO_AXI_WATCHDOGS, value);
}

int fpga_write_heartbeat_ctrl(int value)
{
   int ret=0;

   for (int i = 0; i < 4; i++)
   {
      ret += fpga_write_reg(WATCHDOGS_HEARTBEAT_B1_OFFSET + i, UIO_AXI_WATCHDOGS, ( (value >> (8*i)) & 0x000000FF) );
   }
   return ret;
}

int fpga_read_heartbeat_ctrl(int *value)
{
   int value_aux = 0, aux=0;
   int ret = 0;



   for (int i = 0; i < 4; i++)
   {
      ret += fpga_read_reg(WATCHDOGS_HEARTBEAT_B1_OFFSET + i, UIO_AXI_WATCHDOGS, &value_aux);
      aux += (value_aux << (8 * i));
   }
   if( !ret )
       *value = aux;

   return ret;
}

int fpga_write_reset_ctrl(int node, int value)
{
    int aux = node & 0x7F;


    if(!value)
        aux|= 0x80;
    return fpga_write_reg(ONE_WIRE_TEST_REG_OUT, UIO_AXI_ONE_WIRE, aux);
}

int fpga_read_reset_ctrl(int *node, int *value)
{
    int aux = 0;


    if(fpga_read_reg(ONE_WIRE_TEST_REG_OUT, UIO_AXI_ONE_WIRE, &aux) < 0)
        return -1;

    *node = aux & 0x7F;
    *value = (aux & 0x80) ? 1: 0;

    return 0;
}
#endif
