#include "cXBareMetalDMA.h"
#include <iostream>
#include <array>
#include <bsp/sleep.h>

#define DDR_BASE_ADDR		XPAR_PS7_DDR_0_S_AXI_BASEADDR
#define MEM_BASE_ADDR		(DDR_BASE_ADDR + 0x1000000)
#define TX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00100000)
#define RX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00300000)

cXdmaAXI::cXdmaAXI(u32 device_id)
{
    if( XAxiDma_Config *config = XAxiDma_LookupConfig( device_id ); config )
        if ( XAxiDma_CfgInitialize( phy, config ) == XST_SUCCESS)
            return;
}

void cXdmaAXI::setPolling()
{
    for( const uint32_t & disable : { XAXIDMA_DEVICE_TO_DMA, XAXIDMA_DMA_TO_DEVICE } )
        XAxiDma_IntrDisable( phy, static_cast<u32>(XAXIDMA_IRQ_ALL_MASK), disable );
}

cXdmaAXI &cXdmaAXI::autotest()
{
    XAxiDma_Reset( phy );
    int reset_done = XAxiDma_ResetIsDone( phy );
    while( reset_done != 1 ){}
    std::cout << __PRETTY_FUNCTION__ << ": reset done\n";
    std::cout << __PRETTY_FUNCTION__ << ": selftest(" <<  XAxiDma_Selftest( phy ) << ")\n";
    XAxiDma_HasSg( phy );
    setPolling();

    std::array<uint8_t,1024> b;
    const uint8_t pattern{0b10011001};
    uint8_t *TxBufferPtr = reinterpret_cast<u8 *>(TX_BUFFER_BASE);
    for( size_t i{0}; i < b.size(); i++ )
        TxBufferPtr[i] = ( i % 2 ) ? pattern : ~pattern;

    uint8_t *RxBufferPtr = reinterpret_cast<u8 *>(RX_BUFFER_BASE);
    Xil_DCacheFlushRange(reinterpret_cast<INTPTR>(TxBufferPtr), b.size());
    Xil_DCacheFlushRange(reinterpret_cast<INTPTR>(RxBufferPtr), b.size());
    if( int ret_val = XAxiDma_SimpleTransfer( phy, reinterpret_cast<UINTPTR>(RxBufferPtr), b.size(), XAXIDMA_DEVICE_TO_DMA); ret_val == XST_SUCCESS )
    {
        if( ( ret_val = XAxiDma_SimpleTransfer( phy,reinterpret_cast<UINTPTR>(TxBufferPtr), b.size(), XAXIDMA_DMA_TO_DEVICE) )  == XST_SUCCESS )
        {
            while ((XAxiDma_Busy(phy,XAXIDMA_DEVICE_TO_DMA)) || (XAxiDma_Busy(phy,XAXIDMA_DMA_TO_DEVICE)))
            {
                sleep( 1 );
                std::cout << __PRETTY_FUNCTION__ << ": wait\n";
            }
        }
        else
        {
            std::cout << __PRETTY_FUNCTION__ << " : TX failure(" << ret_val << ")\n";
        }
    }
    else
    {
        std::cout << __PRETTY_FUNCTION__ << " : RX failure(" << ret_val << ")\n";
    }


    return *this;
}

cXdmaPS::cXdmaPS( u16 device_id, u16 gic_id )
{
    std::cout << "\n";
    xSemaphore = xSemaphoreCreateMutex();
    if( XDmaPs_Config *config = XDmaPs_LookupConfig(device_id); config )
        if( XDmaPs_CfgInitialize( phy, config,config->BaseAddress ) == XST_SUCCESS )
            if( gicSetUp( gic_id ) == XST_SUCCESS )
            {
                for( size_t i = 0; i < noOfChannels; i++ )
                     XDmaPs_ResetChannel( phy, i );
                std::cout << __PRETTY_FUNCTION__ << ": PL330 DMAUnit up and running\n";
            }
}

#include <bitset>
#define DMA_LENGTH	1024
#define DMA_FAULT_INTR	XPAR_XDMAPS_0_FAULT_INTR
void cXdmaPS::commandDone( unsigned int Channel, XDmaPs_Cmd *DmaCmd, void *arg )
{
    static constexpr bool verbose{false};
    BaseType_t xHPTWoken{pdFALSE};
    cXdmaPS * dma_ps = reinterpret_cast<cXdmaPS*>(arg);
    dma_ps->activity = 1 << Channel;
    xSemaphoreGiveFromISR( dma_ps->xSemaphore, & xHPTWoken );
    portYIELD_FROM_ISR( xHPTWoken );
    if constexpr ( verbose )
            std::cout << __PRETTY_FUNCTION__ << "(" << DmaCmd->BD.Length << ")" << std::bitset<8>(dma_ps->activity) << "\n\n";
}

static uint32_t Dst[DMA_LENGTH] __attribute__ ((aligned (32)));
static std::array<uint32_t,DMA_LENGTH> b;
#include <cstdlib>
#include <ctime>
#include "xtime_l.h"
#include "util.h"
cXdmaPS &cXdmaPS::autotest()
{
    static constexpr bool verbose{false};
    unsigned int Channel{0};
    std::cout << __PRETTY_FUNCTION__ << "#" << Channel << "(" << b.size() << ")\n";
    XDmaPs_Cmd DmaCmd{0};
    DmaCmd.ChanCtrl.SrcBurstSize = 4;
    DmaCmd.ChanCtrl.SrcBurstLen = 4;
    DmaCmd.ChanCtrl.SrcInc = 1;
    DmaCmd.ChanCtrl.DstBurstSize = 4;
    DmaCmd.ChanCtrl.DstBurstLen = 4;
    DmaCmd.ChanCtrl.DstInc = 1;
    DmaCmd.BD.SrcAddr = (uintptr_t)b.data();
    DmaCmd.BD.DstAddr = (uintptr_t)Dst;
    DmaCmd.BD.Length = b.size() * sizeof( b[0] );

    if constexpr ( verbose )
    {
        /* Maybe an accelerator ? */
        XDmaPs_GenDmaProg(phy, Channel, & DmaCmd);
        XDmaPs_Print_DmaProg( & DmaCmd );
        XDmaPs_FreeDmaProg(phy, Channel, & DmaCmd);
    }

    const uint32_t pattern{0b10011001100110011001100110011001};

    using namespace ranges;
    bool flip{true};
    for_each( b, [ & flip ]( uint32_t & val ){ val = ( flip = ! flip ) ? pattern : ~pattern; });

    XDmaPs_SetDoneHandler( phy, Channel, commandDone, (void *)this );

    int elapsed{200};
    constexpr int delay{10};
    const TickType_t xDelay{ delay/ portTICK_PERIOD_MS};

    XTime init, end;
    XTime_GetTime( & init);
    srand( init );

    XTime_GetTime( & init);
    XDmaPs_Start(phy, Channel, & DmaCmd, 0);
    while ( elapsed -= delay )
        if( xSemaphoreTake( xSemaphore, xDelay) == pdTRUE )
            if( std::bitset<8>(activity).test(Channel) )
                break;
    XTime_GetTime( & end  );

    for( size_t i{0}; i < 10; i++ )
    {
        size_t index =  ( rand() % ( b.size() -1 ) ) +1;
        std::cout << std::dec << "index(" << index << ")" << std::hex << Dst[ index ] << ":" << Dst[index +1 ] << "\n";
    }

    std::cout << __PRETTY_FUNCTION__ << ": Channel("
              << Channel << ") " << ( XDmaPs_IsActive(phy,Channel) ? "Active" : "Relaxed" ) << " # DMA-handler activity( "
              << std::bitset<8>(activity)
              << std::dec << " )(" << end - init  << ")" << "\n\n";
    return *this;
}

#include <map>
int cXdmaPS::gicSetUp( u16 gic_id )
{
    Xil_ExceptionInit();
    if( XScuGic_Config *config = XScuGic_LookupConfig(gic_id); config )
    {
        int Status = XScuGic_CfgInitialize(gic, config, config->CpuBaseAddress);
        if ( XScuGic_CfgInitialize(gic, config, config->CpuBaseAddress) == XST_SUCCESS)
        {
            Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_IRQ_INT,(Xil_ExceptionHandler)XScuGic_InterruptHandler,gic);
            if( XScuGic_Connect(gic,DMA_FAULT_INTR,(Xil_InterruptHandler)XDmaPs_FaultISR,(void *)phy) == XST_SUCCESS)
            {
                std::map<int,void (*)(XDmaPs *)> m = {
                { XPAR_XDMAPS_0_DONE_INTR_0, XDmaPs_DoneISR_0 },
                { XPAR_XDMAPS_0_DONE_INTR_1, XDmaPs_DoneISR_1 },
                { XPAR_XDMAPS_0_DONE_INTR_2, XDmaPs_DoneISR_2 },
                { XPAR_XDMAPS_0_DONE_INTR_3, XDmaPs_DoneISR_3 },
                { XPAR_XDMAPS_0_DONE_INTR_4, XDmaPs_DoneISR_4 },
                { XPAR_XDMAPS_0_DONE_INTR_5, XDmaPs_DoneISR_5 },
                { XPAR_XDMAPS_0_DONE_INTR_6, XDmaPs_DoneISR_6 },
                { XPAR_XDMAPS_0_DONE_INTR_7, XDmaPs_DoneISR_7 },
            };

                for( auto & i : m )
                    Status |= XScuGic_Connect(gic,i.first,(Xil_InterruptHandler)i.second,(void *)phy);
                if ( Status == XST_SUCCESS )
                    for( auto & i : m )
                        XScuGic_Enable(gic, i.first );
                Xil_ExceptionEnable();
                return XST_SUCCESS;
            }
        }
    }
    return XST_FAILURE;

}
