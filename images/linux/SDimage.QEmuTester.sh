#!/bin/bash
# uZed system emulation specifications

C4='\033[00;34m'
C5='\033[00;32m'
RC='\033[0m'
auditor="${C5}`basename $0`:${RC}"

#simulator="/opt/petalinux/2019.2/sysroots/x86_64-petalinux-linux/usr/bin/qemu-xilinx/qemu-system-aarch64"
simulator="/home/juanba/ChallengeX/branches/zynq/qemu/xilinx.qemu/build/aarch64-softmmu/qemu-system-aarch64"
simulator="./qemu-system-aarch64"

display="-serial /dev/null -serial mon:stdio -display none"
dtb="-dtb system.dtb"
#dtb="-dtb ChallengeX.QEMU.dtb"
machine="-M arm-generic-fdt-7series -machine linux=on"

device1="-device loader,addr=0xf8000008,data=0xDF0D,data-len=4"
device2="-device loader,addr=0xf8000140,data=0x00500801,data-len=4"
device3="-device loader,addr=0xf800012c,data=0x1ed044d,data-len=4"
device4="-device loader,addr=0xf8000108,data=0x0001e008,data-len=4"
device5="-device loader,addr=0xF8000910,data=0xF,data-len=0x4"

# if machine PCI is provided -> edu="-device edu"
chipset_setup="$device1 $device2 $device3 $device4 $device5"

embedded="-kernel zImage"
system_image="qemu_sd.img"
boot_spec="-drive file=${system_image},if=sd,format=raw,index=0 -boot mode=3"

export LANG=""
printf "\n"
date 

printf "${auditor} Loading ${C4}μZed-system${RC} arquitecture to deploy ${C4}${system_image}${RC}\n"
printf "ctrl-a c :interacts with QEMU console, ctrl-a x :quit emulation\n\n"


[ "$1" == "dhcp" ] && { 
    network_images="/tftptboot"
    printf "${auditor} dhcp mode toggled: images path ${C4}${network_images}${RC}\n"
    fsbl_spec="-device loader,file=./zynq_fsbl.elf,cpu-num=0"
    uboot_spec="-device loader,file=./u-boot.elf"
    ftp_options="-net user,tftp=${network_images}";
    boot_spec="-boot mode=3"
    bootargs="console=ttyPS0,115200n8 ip=dhcp root=/dev/nfs nfsroot=10.0.2.16:/opt/shared/korolev-rootfs,v3,tcp,nolock"
}

network="-net nic -net tap,ifname=tap1"
co_design="-machine-path ./co-design.tmp"
debug="-s"

# petalinux-boot --qemu --kernel --qemu-args "-net nic -net tap,ifname=tap1"

read -t 2

${simulator} ${machine} ${dtb} ${display} ${chipset_setup} ${boot_spec} ${embedded} ${network} ${debug}

# --append "console=ttyPS0,115200n8 ip=dhcp \
#           root=/dev/nfs nfsroot=10.0.2.16:/opt/shared/korolev-rootfs,port=2049,tcp nfsrootdebug"
# ${co_design}
# ${debug}

