#include "arm_neon.h"
#include <array>
#include <iostream>

static std::array<float32_t,4> w;
static std::array<float32_t,3> poles{ 1,1.321, -0.633};
static std::array<float32_t,3> zeros{ 0.063, 0.125, 0.063 };
static float32x4_t weights;

template< bool debug=false>
inline float bq_compiler( const float & sample )
{
    float32_t acc{0};
    acc += w[0]   *  zeros[1];
    acc += w[1]   *  zeros[2];
    acc += w[2]   * -poles[1];
    acc += w[3]   * -poles[2];
    acc += sample *  zeros[0];

    if constexpr ( debug )
    {
        using namespace std;
        cout <<  zeros[1]*w[0] << "/";
        cout <<  zeros[2]*w[1] << "/";
        cout << -poles[1]*w[1] << "/";
        cout << -poles[2]*w[3] << "\n";
    }

    // states suffling
    w[1] = w[0];
    w[0] = sample;
    w[3] = w[2];
    return w[2] = acc;
}

template< bool debug=false>
inline float bq_intrinsics( const float & sample )
{
    float32x4_t W{ w[0], w[1], w[2], w[3] };
    float32x4_t q = vmulq_f32( W, weights);
    float32x2_t d = vadd_f32(vget_low_f32(q),vget_high_f32(q));
    float acc = ( sample * zeros[0] ) + vget_lane_f32( vpadd_f32(d,d), 0 );

    if constexpr ( debug )
    {
        using namespace std;
        cout << w[0] << "/" << w[1] <<"/" << w[2] << "/" << w[3] << "\n";
        cout << q[0] << "/" << q[1] <<"/" << q[2] << "/" << q[3] << "\n";
        cout << acc << "\n";
    }

    // states suffling
    w[1] = w[0];
    w[0] = sample;
    w[3] = w[2];
    return w[2] = acc;

}

#include "xpm_counter.h"
#include "util.h"
#include "xtime_l.h"

static void filtering_testing()
{
    XTime tmp;
    s32 pm_spec{XPM_CNTRCFG3};
    std::array<u32,XPM_CTRCOUNT> pm_values;

    XTime_GetTime( & tmp);
    Xpm_SetEvents( pm_spec );
    float32_t val;
    val = bq_compiler(100);
    val = bq_intrinsics(100);
    Xpm_GetEventCounters( pm_values.data() );

    XTime now;
    XTime_GetTime( & now );

    std::cout << __PRETTY_FUNCTION__ << ": " << val << " :" << now - tmp << " : PMU values "
              << std::hex << pm_values[ pm_spec ] << "\n";

    using namespace ranges;
    for_each(pm_values,[](u32 r){ std::cout << std::hex << r << "\n"; });
}

