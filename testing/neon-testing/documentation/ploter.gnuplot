fft="./output.cFFTView.txt"; 
signal="./output.cBiQuad2.txt"; 
set grid

set multiplot

set object 1 rect from 100,10000 to 300,50000
set object 1 rect fc rgb 'white' fillstyle solid 0.0 noborder

plot fft u 1 w lp t "", fft u 2 w lp t "FFT.LowEss.Online"

set origin 0.37,0.33
set size 0.6,0.6
unset xlabel
unset ylabel
unset label
set tics scale 0.1 front
plot signal u 1 w lp t "Raw", signal u 2 w lp t "BiQuad2 1000/20Hz"

unset multiplot

pause -1 "Any key or button will terminate\n"