#include "cIIODevice.h"
#include "iio/iio.h"
#include <fstream>
#include <rtimers/cxx11.hpp>
#include "rtimers/core.hpp"

cIIOdevice::cIIOdevice(const std::string & iio_name_, const std::string & iio_trigger_ )
    : Module("","cIIOdevice") , iio_name( iio_name_ ), iio_trigger( iio_trigger_ )
{
    using namespace std;
    stringstream oss;

    oss << endl;
    auditor_val.resize(256);
    if( ( ctx = iio_create_default_context() ) == nullptr )
    {
        auditorError();
        return;
    }

    auto timenow = chrono::system_clock::to_time_t(chrono::system_clock::now());
    oss << endl << ctime( & timenow ) << "context info: " << scm();
    auditor( oss );
    scanContext();

}

cIIOdevice::~cIIOdevice()
{
    iio_context_destroy(ctx);
    iio_buffer_destroy(rx_buffer);
}

void cIIOdevice::latencyObserver(){ iio_latency(); }

size_t cIIOdevice::setAlarmParams(const alarm_params_t &alarms_params){ return iio_alarm->setAlarmParams( alarms_params ); }
iAlarm::eventScope cIIOdevice::subscribeAlarm(const eventScope & mask, std::shared_ptr<iNotifier<> > notifier )
{ return iio_alarm->subscribeAlarm( mask, notifier ); }

iChannel::state_t cIIOdevice::channelState( const iAlarm::eventScope &mask )
{
    state_t state;
    ranges::for_each( channels, [&]( cIIOchannel & channel ){
        if( mask.test( channel.xadc_index() ) )
            state.push_back( { channel.state(), channel.raw() } );
    });
    return state;
}

void cIIOdevice::deployAlarms(const std::vector<cIIOchannel>&, const char *){ return; }

void cIIOdevice::iio_latency()
{
    using namespace std;
    cout << "\nLatency testing #\n\n";
    auto latency_snapshot = [&]( auto acquisition_mode, const std::string & auditory )
    {
        using namespace ranges;
        using namespace rtimers;
        double tmp{};
        ofstream ofile;
        ofile.open ( auditory + ".txt");

        {
            cxx11::DefaultTimer tmr(  auditory + std::to_string(channels.size()) + ")" );
            std::array<array<double,9>,1<<10> buffer;
            for( size_t i = 0; i < ( 1 << 10 ); i++ )
            {
                tmr.start();
                array<double,9> snapshot;
                size_t j = 0;
                ranges::for_each( channels, [&]( cIIOchannel & channel)
                                 {
                                     snapshot[j] = tmp = acquisition_mode == Slow  ? channel.state() : channel.phy_state();
                                     j++ ;
                                 }
                                 );
                buffer[i] = snapshot;
                tmr.stop();
            }
            ranges::for_each( channels, [&]( cIIOchannel & channel) { std::cout << channel.dump() << std::endl; } );
            std::stringstream oss;
            oss << 1 / ((  tmr.getStats().mean * 1000 )  / channels.size() ) << "Khz";
            auditor( oss );

            ranges::for_each( buffer, [&](array<double,9>& tmp){
                ranges::for_each( tmp,[&]( double aux ){ ofile << aux << " ";});
                ofile << "\n";
            });

            ofile.close();
        }
    };

    latency_snapshot( Slow,"iio-latency");
    // latency_snapshot( Fast,"iio-latency.local");

}

#include <thread>

void cIIOdevice::scanContext()
{
    using namespace std;

    auto channel_decoder = [&]( struct iio_channel* channel, int index )
    {
        bool output = iio_channel_is_output(channel);
        stringstream oss;
        oss << "ch#" << index << " "
            << setw(10) << iio_channel_get_id(channel)
            << "("     << ( output ? "output" : "input" ) << ")";
        std::cout << "";
        if( ! output )
        {
            cIIOchannel tmp( channel, index );
            channels.push_back( tmp );

            bool scannable{ iio_channel_is_scan_element(channel) };
            if ( scannable )
            {
                iio_channel_enable( channel );
                scanning_mask.set( tmp.xadc_index() );
            }
            oss << setw(10) << tmp.whoami() << ( scannable ? "(S)" : "(NS)"  ) << ":" << tmp.cached();
        }
        auditor( oss, Moderate );
    };

    if( ( device = iio_context_find_device( ctx,iio_name.c_str() ) ) )
    {
        stringstream oss;
        size_t channels = iio_device_get_channels_count(device);
        oss << iio_name
            << ( iio_device_is_trigger(device) ? ".trigger" :"" )
            << " attribs(" <<  iio_device_get_attrs_count(device) << ")"
            << " maps " << channels << " channels on " << iio_device_get_id(device) << "\n"
            << " sample sizes(" << iio_device_get_sample_size(device) << ")\n";

        for( size_t j = 0; j < channels; j++ )
            if( struct iio_channel *channel;( channel = iio_device_get_channel( device, j ) ) )
                channel_decoder( channel, j );
        string tmp( string(cIIOchannel::iio_path) + iio_device_get_id(device) );
        ( iio_alarm = make_unique<cIIOAlarm>(*this) )->deployAlarms( this->channels, tmp.c_str() );
        Numerology report = Verbose;

        if( ( trigger = iio_context_find_device( ctx,iio_trigger.c_str() ) ) )
        {
            if( iio_device_is_trigger(trigger) )
            {
                oss << " trigger(" << iio_trigger << ")";
                const char* sampling{"sampling_frequency"};
                if( iio_device_find_attr(trigger, sampling ) )
                {
                    oss << " settled to ";
                    iio_device_attr_read_longlong(trigger, sampling, & scanning_rate );
                    oss << scanning_rate << "\n";
                }

                if ( iio_device_set_trigger(device, trigger) < 0 )
                {
                    oss << " trigger(" << iio_trigger << ") chaining to "<< iio_name << " fails with : errno("
                        << errno << ") " << strerror(errno) << "\n";
                    report = Warning;
                }

                if( ( rx_buffer = iio_device_create_buffer( device, buffer_length, false) ) == nullptr )
                {
                    oss << " iio-buffering(" << buffer_length << ") for "<< iio_name
                        << " masked as 0x" << std::hex << std::setfill('0') << scanning_mask.to_ulong() << " fails with : errno("
                        << errno << ") " << strerror(errno) << "\n";
                    report = Warning;
                }
                else
                    std::thread( & cIIOdevice::triggerCallback<true>, this ).detach();
            }
        }

        auditor( oss, report );
    }

}

std::string cIIOdevice::internalAuditory() const { return name + ":"; }

const std::string cIIOdevice::scm() const
{
    using namespace std;
    stringstream scm;
    unsigned int major, minor;
    char git_tag[8];
    iio_context_get_version( ctx, & major, & minor, git_tag );
    scm << iio_context_get_name(ctx) << ".v" << major << "r" << minor << "git(" << string(git_tag) << ")";
    scm << endl << iio_context_get_description(ctx);
    return scm.str();
}

cIIOdevice &cIIOdevice::auditorError()
{
    std::stringstream oss;
    iio_strerror( errno, auditor_val.data(), auditor_val.size());
    oss << auditor_val;
    auditor( oss, Warning );
    return *this;
}

template <bool debug>
void cIIOdevice::triggerCallback()
{
    const char* backtrace = __FUNCTION__ ;
    size_t sample_size = iio_channel_get_data_format(channels[0].phy())->length;
    std::vector<uint8_t> payload;
    payload.resize( sample_size * buffer_length );

    if constexpr ( debug )
    {
        std::stringstream oss;
        oss << backtrace << " : " << payload.size() << "/" << sample_size << "\n";
        auditor( oss );
    }

    using namespace ranges;
    do {

        if constexpr ( debug )
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(5000));
            std::stringstream oss;
            oss << backtrace << " on 0x" << std::hex << std::setfill('0') << scanning_mask.to_ulong() << "\n";
            auditor( oss );
            continue;
        }

        if (int nbytes_rx = iio_buffer_refill( rx_buffer ); nbytes_rx < 0)
        {
            std::stringstream oss;
            oss << backtrace << ": failure refilling buffer err( " << (int)nbytes_rx << ")";
            auditor( oss, Warning );
            continue;
        }

        ranges::for_each(channels,[&](cIIOchannel & channel ){
            if( scanning_mask.test( channel.xadc_index() ) == false )
                return;
            iio_channel_read( channel.phy(), rx_buffer, payload.data(), sample_size * buffer_length);
        });

    } while( 1 );


}
