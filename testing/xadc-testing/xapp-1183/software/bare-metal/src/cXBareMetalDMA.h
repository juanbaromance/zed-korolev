#pragma once
#include "bsp/xparameters.h"
#include "bsp/xaxidma.h"
#include "bsp/xdmaps.h"
#include "bsp/xscugic.h"

#include "bsp/FreeRTOS.h"
#include "bsp/semphr.h"

class cXdmaAXI
{
public:
    cXdmaAXI( u32 device_id = XPAR_AXIDMA_0_DEVICE_ID );
    void setPolling();
    cXdmaAXI & autotest();

private:
    XAxiDma phy_, *phy{ & phy_ };
};

class cXdmaPS
{
public:
    cXdmaPS(u16 device_id = XPAR_XDMAPS_1_DEVICE_ID, u16 gic_id = XPAR_SCUGIC_SINGLE_DEVICE_ID  );
    cXdmaPS & autotest();

private:
    static void commandDone( unsigned int Channel, XDmaPs_Cmd *DmaCmd, void *arg );
    int gicSetUp(u16 gic_id );

    static constexpr uint8_t noOfChannels{8};
    SemaphoreHandle_t xSemaphore{nullptr};
    uint8_t activity{0};
    XDmaPs phy_, *phy{ & phy_ };
    XScuGic gic_, *gic{ & gic_ };
};
