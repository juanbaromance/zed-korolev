#include <cmath>
#include <string.h>
#include <stdio.h>

#include "geometry/geometry.h"
#include <iostream>
#include "cAfin.h"
using namespace std;

extern "C" void* matrix_mul_float(void *dst, void *m1, const void *m2);

#include <memory>
#include <variant>
#include "util.h"
#include "neon-10/NE10_math.h"
template < typename T>
T deg2rad (T degrees) { return degrees * 4.0 * atan (1.0) / 180.0; }

template <int dimension = 4, class NeoMatType = ne10_mat4x4f_t, class NeoVectorType = ne10_vec4f_t >
class cNeoMatrix {

private:
    NeoMatType M, inverted_M;
    NeoVectorType v;

public:
    cNeoMatrix(){}
    cNeoMatrix( void* m_ )
    {
       if constexpr ( dimension == 4 )
       {
           float m[ 4 ][ 4 ];
           memcpy( m, m_, 16 * sizeof( float ) );
           createColumnMajorMatrix4x4(
               & M,
               m[0][0],m[0][1],m[0][2],m[0][3],
               m[1][0],m[1][1],m[1][2],m[1][3],
               m[2][0],m[2][1],m[2][2],m[2][3],
               m[3][0],m[3][1],m[3][2],m[3][3]
               );
       }
       else if constexpr ( dimension == 3 )
       {

       }
       else if constexpr ( dimension == 2 )
       {

       }
    }
    cNeoMatrix & set( const NeoMatType & m ){ M=m; return *this; }
    NeoMatType & value(){ return M; }

    template<class T = float_t>
    T rand(T val = 100){ return static_cast<T>( std::rand()/ RAND_MAX * val ); };

    /*!
  @brief multiply two 4x4 float matrixes
  @param[in]  m1
  @param[in]  m2
  @param[out] o_mul
*/
    inline NeoMatType & mul44compiler( const void *m2 )
    {
        int u, v, k;
        float aux_m1[ 4 ][ 4 ];
        float aux_m2[ 4 ][ 4 ];
        float aux_p [ 4 ][ 4 ];

        memcpy( aux_m1, & M, sizeof(M) );
        memcpy( aux_m2, m2, sizeof(M) );
        for( u = 0; u < 4; u++ )
        {
            for( v = 0; v < 4; v++ )
            {
                aux_p[ u ][ v ] = 0;
                for( k = 0; k < 4; k++ )
                {
                    aux_p[ u ][ v ] += aux_m1[ u ][ k ] * aux_m2[ k ][ v ];
                }
            }
        }
        memcpy( & M, aux_p, 16 * sizeof( float ) );
        return M;
    }

    inline NeoMatType & mul44asm( const void *m2 )
    {
        matrix_mul_float( & M, & M, m2 );
        return M;
    }

    inline NeoMatType & mulneon( void* m2 )
    {
        if constexpr ( dimension == 4 )
            ne10_mulmat_4x4f( & M, & M, (NeoMatType*)m2, 1 );
        if constexpr ( dimension == 3 )
            ne10_mulmat_3x3f( & M, & M, (NeoMatType*)m2, 1 );
        if constexpr ( dimension == 2 )
            ne10_mulmat_2x2f( & M, & M, (NeoMatType*)m2, 1 );
        return M;
    }

    cNeoMatrix scaling( NeoVectorType p ) const
    {
        float tmp[4][4] = {
            { p.x(),     0,     0, 0 },
            { 0    , p.y(),     0, 0 },
            { 0    ,     0, p.z(), 0 },
            { 0    ,     0,     0, 1 }
        };
        return cNeoMatrix( tmp );
    }

    cNeoMatrix rotZ( float deg ) const
    {
        float  angle{deg2rad(deg)};
        float tmp[4][4] = {
            {  cos(angle), -sin(angle), 0, 0 },
            {  sin(angle),  cos(angle), 0, 0 },
            {           0,           0, 1, 0 },
            {           0,           0, 0, 1 }
        };
        return cNeoMatrix( tmp );
    }

    cNeoMatrix rotY( float deg ) const
    {
        float  angle{deg2rad(deg)};
        float tmp[4][4] = {
            {  cos(angle), 0, sin(angle), 0 },
            {           0, 1,          0, 0 },
            { -sin(angle), 0, cos(angle), 0 },
            {           0, 0,          0, 1 }
        };
        return cNeoMatrix( tmp );
    }

    cNeoMatrix rotX( float deg ) const
    {
        float  angle{deg2rad(deg)};
        float tmp[4][4] = {
            {  1,          0,           0, 0 },
            {  0, cos(angle), -sin(angle), 0 },
            {  0, sin(angle),  cos(angle), 0 },
            {  0,          0,           0, 1 }

        };
        return cNeoMatrix( tmp );
    }

    inline NeoVectorType mul41( const NeoVectorType & v )
    {
        float ( &aux_m1 )[4][4] = *reinterpret_cast<float (*)[4][4]>( &M );
        const float ( &aux_m2 )[4] = *reinterpret_cast<const float (*)[4]>( &v );
        float (  &aux_p )[4] = *reinterpret_cast<float (*)[4]>( & this->v );
        for( int i = 0; i < 4; i++ )
        {
            aux_p[ i ] = 0;
            for( int j = 0; j < 4; j++ )
                aux_p[ i ] += aux_m1[ i ][ j ] * aux_m2[ j ];
        }
        return this->v;
    }


    inline NeoVectorType mulneon( NeoVectorType & v )
    {
        ne10_mulcmatvec_cm4x4f_v4f( & v, & M, & v, 1 );
        return v;
    }

    cNeoMatrix identity()
    {
        if constexpr ( dimension == 4 )
            ne10_identitymat_4x4f( & M, 1 );
        if constexpr ( dimension == 3 )
            ne10_identitymat_3x3f( & M, 1 );
        if constexpr ( dimension == 2 )
            ne10_identitymat_2x2f( & M, 1 );
        return *this;
    }

    inline NeoMatType inverse()
    {
        ne10_invmat_4x4f( & inverted_M, & M, 1 );
        return inverted_M;
    }

    inline NeoMatType inverse_compiler()
    {
        int i, j;
        float (&mi)[4][4] = *reinterpret_cast<float (*)[4][4]>( &inverted_M );
        float (&mm)[4][4] = *reinterpret_cast<float (*)[4][4]>( &M );

        for( i = 0; i < 3; i++ ){ //transpose 3x3
            for( j = 0; j < 3; j++ ){
                mi[ i ][ j ] = mm[ j ][ i ];
            }
        }
        mi[ 3 ][ 0 ] = 0;
        mi[ 3 ][ 1 ] = 0;
        mi[ 3 ][ 2 ] = 0;
        mi[ 3 ][ 3 ] = 1;
        mi[ 0 ][ 3 ] = 0;
        mi[ 1 ][ 3 ] = 0;
        mi[ 2 ][ 3 ] = 0;
        for( i = 0; i < 3; i++ ){
            for( j = 0; j < 3; j++ ){
                mi[ i ][ 3 ] -= mm[ j ][ 3 ] * mm[ j ][ i ];
            }
        }
        return inverted_M;
    }


    void dump(  NeoVectorType & v, const std::string & name = "unnamed" ) const
    {
        fprintf( stdout,"\nv(%10s)\n", name.c_str() );
        const float (&data)[4] = *reinterpret_cast<float (*)[4]>(&v);
        for( int j = 0; j < 4; j++ )
            fprintf( stdout, "% -5.2f \t", data[ j ] );
        fprintf( stdout, "\n\n");
    }

    bool dump( const std::string & name = "unnamed" )
    {
        fprintf( stdout,"\nM(%10s)\n", name.c_str() );
        float (&data)[4][4] = *reinterpret_cast<float (*)[4][4]>(&M);
        for( int i = 0; i < 4; i++ )
        {
            for( int j = 0; j < 4; j++ )
            {
                fprintf( stdout, "% -5.2f \t", data[ i ][ j ] );
            }
            fprintf( stdout, "\n");
        }
        fprintf( stdout, "\n");
        return true;
    }

private:
};



#include "cZynqSnapshot.h"

template<int type=1, int length = 1 << 10, bool debug=false>
ne10_mat4x4f_t snapshot4m44( const char* name )
{
    cZynqSnapshot snapshot(name, length);
    float src[4][4]={
        { 10,  0,  0,  0 },
        {  0, 20,  0,  0 },
        {  0,  0, 30,  0 },
        {  0,  0,  0, 40 }
    };

    float mul[]={
        1,0,0,0,
        0,1,0,0,
        0,0,1,0,
        0,0,0,1
    };

    ne10_mat4x4f_t zero;
    ne10_mat4x4f_t & tmp = zero;
    cNeoMatrix m(src);
    for( size_t k = 0; k < snapshot.size(); ++k )
    {
        snapshot.start();
        if constexpr ( type == 1 )
            tmp = m.mulneon( mul );
        else if constexpr ( type == 2 )
            tmp = m.mul44compiler( mul );
        else if constexpr ( type == 3 )
            tmp = m.mul44asm( mul );
        snapshot.stop();
    }

    if constexpr ( debug == true  )
    {
        cout << __PRETTY_FUNCTION__ << endl;
        cout << "source :" << src[0][0] << " " << src[1][1] << " " << src[2][2] << " " << src[3][3] << "\n";
        cNeoMatrix( & tmp ).dump();
    }

    return tmp;

}

template<int type=1, int length = 1 << 10, bool debug=false>
ne10_vec4f_t snapshot4m41( const char* name )
{
    cZynqSnapshot snapshot(name, length);
    ne10_vec4f_t tmp;
    memset( & tmp, 0 ,sizeof( tmp ) );
    tmp.z = 1;

    cNeoMatrix m{cNeoMatrix<>().rotY(-90)};
    m.dump("Rotation(-π/2).Y");
    for( size_t k = 0; k < snapshot.size(); ++k )
    {
        snapshot.start();

        if constexpr ( type == 1 )
            tmp = m.mulneon( tmp );
        else if constexpr ( type == 2 )
            tmp = m.mul41( tmp );

        snapshot.stop();
    }

    m.dump(tmp, name);
    return tmp;
}

template<int type=1, int length = 1 << 10, bool debug=false>
bool inverse( const char *name )
{

    float tmp[] = {
        1,0,-1,0,
        0,2,0,0,
        1,0,3,0,
        0,0,0,1
    };
    cNeoMatrix m(tmp);
    m.dump(name);

    if constexpr ( type == 0 )
        return m.set( m.inverse() ).dump("Inverted Matrix");

    {
        ne10_mat4x4f_t i_m;
        cZynqSnapshot snapshot(name, length);
        snapshot.start();
        for( size_t k = 0; k < snapshot.size(); ++k )
        {
            if constexpr ( type == 1 )
                i_m = m.inverse();
            if constexpr ( type == 2 )
                i_m = m.inverse_compiler();
        }
        snapshot.stop();
        return m.set(i_m).dump("Inversion");
    }
    return true;

}

cAfin::cAfin(){}
bool cAfin::testing()
{
   snapshot4m44<1>("cMatrix(4x4).mul.neo10");
   snapshot4m44<2>("cMatrix(4x4).mul.gcc++");
   snapshot4m44<3>("cMatrix(4x4).mul.asm");

   snapshot4m41<1>("cVector(4x1).rotate.neo10");
   snapshot4m41<2>("cVector(4x1).rotate.gcc++");

// inverse<0>("Feasibility");
// inverse<1>("Neo10.inversion");
// inverse<2>("Neo10.gcc++");
    return true;
}



