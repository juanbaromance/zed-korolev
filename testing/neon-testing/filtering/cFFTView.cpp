#include <cmath>
#include <map>
#include <sstream>
#include <valarray>
#include <vector>
#include "rtimers/cxx11.hpp"
#include "rtimers/posix.hpp"
#include "neon-10/NE10.h"
#include "cFFTView.h"

/* benchmarks
 * 600 usec /512 @ LinuxPC
 * 500 usec /64,
 * 1000 usec / 128 @ Linux PPC */

using namespace  std;
#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include "Lowess.h"
#include "cZynqSnapshot.h"

bool fft_testing( cFFTView<>& view )
{
    using namespace std;
    ifstream myfile;
    ofstream ofile;
    ofile.open ("./output." + view.report()+ ".txt");
    myfile.open ("./oscilator.txt");
    cFFTView<>::buffer_t tmp;
    cFFTView<>::dct_t freq;
    using namespace rtimers;

    if( myfile.is_open() )
    {
        string line;
        size_t i = 0;
        while ( getline( myfile, line ) )
        {
            if( line.substr(0,1).compare("#") )
            {
                std::vector<std::string> results;
                boost::split(results, line, [](char c){return c == ' ';});
                tmp[i] = stof( results[0] );
                freq[i] = i;
                i++ ;
            }
        }

        {
            cZynqSnapshot snapshot(view.report() + std::string(".fft.Neo10(") + to_string(tmp.size()) + ")", 1 << 8 );
            for( size_t j = 0; j < snapshot.size(); j++ )
            {
                snapshot.start();
                view.feed(tmp);
                snapshot.stop();
            }
        }

        {
            cZynqSnapshot snapshot(view.report() + std::string(".fft.compiler(") + to_string(tmp.size()) + ")", 1 << 8 );
            for( size_t j = 0; j < snapshot.size(); j++ )
            {
                snapshot.start();
                view.feed<Compiler>(tmp);
                snapshot.stop();
            }
        }

        cxx11::DefaultTimer tmr( view.report() + std::string(".Lowess"));
        CppLowess::TemplatedLowess<cFFTView<>::dct_t, float> flowess;
        cFFTView<>::dct_t out, tmp1, tmp2;

        for( size_t j = 0; j < ( 1 << 0 ); j++ )
        {
            tmr.start();
            cFFTView<>::dct_t dct = view.valueOfPSD();
            flowess.fit( freq, dct, 0.06, 0, 1.0, out, tmp1, tmp2 );
            tmr.stop();
            for( size_t k = 0; k < dct.size(); ++k )
                ofile << dct[k] << " " << out[k] << "\n";
        }
    }
    ofile.close();
    myfile.close();
    return true;
}




