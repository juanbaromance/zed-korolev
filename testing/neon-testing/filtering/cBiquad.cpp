#include "cBiquad.h"
#include <sstream>
#include <cmath>
#include <cassert>
#include <iostream>
#include "util.h"
using namespace std;

#include <rtimers/cxx11.hpp>
#include <rtimers/posix.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>

#include <valarray>
#include <algorithm>
#include <numeric>
#include "neon-testing/cZynqSnapshot.h"
#define GET_VARIABLE_NAME(Variable) (#Variable)

template <typename F, StrategyEnum strategy=Compiler>
bool offline_testing( F& f, const std::string & details = "" )
{
    using namespace std;
    ifstream myfile;
    ofstream ofile;
    ofile.open ("./output." + f.name() + "." + details + ".txt");
    myfile.open ("./oscilator.txt");
    array<float,1024> v;

    if( myfile.is_open() )
    {
        string line;
        size_t i = 0;
        while ( getline( myfile, line ) && ( i < v.size() ) )
        {
            if( line.substr(0,1).compare("#") )
            {
                std::vector<std::string> results;
                boost::split(results, line, [](char c){return c == ' ';});
                v[++i] = stof( results[0] );
            }
        }

        float_t tmp;
        for( size_t j = 0; j < ( 1 << 0 ); j++ )
        {
            cZynqSnapshot snapshot(f.name()+ "." + details, i);
            for( size_t k = 0; k < i; ++k )
            {
                snapshot.start();
                tmp = f.template step<strategy>( v[k] );
                snapshot.stop();
                ofile << v[k] << " " << tmp << "\n";
            }

        }

    }
    ofile.close();
    myfile.close();
    return true;

}

cBiQuad::cBiQuad( string name, const vector<float> &poles, const vector<float> &zeros, int order )
    : name_( name )
{
    (void)order;
    this->poles = poles;
    this->zeros = zeros;
    z.resize( poles.size() );
    reset(0);
}

cBiQuad::cBiQuad(string name, size_t Fs_, size_t Fc_, double Q_, cBiQuad::Numerology topology_ )
    : name_( name ), Q(Q_), Fc( Fc_ ), Fs( Fs_ ), topology( topology_ )
{
    poles.resize(3);
    zeros.resize( poles.size() );
    z.resize( poles.size() );
    ZeroPoleMap( Fs, Fc, Q, topology );
    cout << report();
}

cBiQuad::~cBiQuad(){ }
bool cBiQuad::testing(){ return offline_testing( *this ); }
int cBiQuad::reset( float input, bool )
{
    for( size_t i = 0; i < z.size() -1; i++ )
        z[i] = input;
    return 0;
}

/*
 * JB#20042020 Neon Learning. inst 26(VFP)/3(ARM)
 *
 * experimental measures 153nsec : scaled with 1.5nsec/cycles 102 cycles
 *
 * vldr(11)  register loader VFP(1/1) = 22
 * vmov(2)   Move Register VFP(1/1)   = 4
 * vstr(3)   Storage Register VFP(1/1) = 6

 * vmul(3)   multiply spfp  SIMD(1/5) = 18
 * vmla(1)   multiply and acc (aka y = ax + b resolver) SIMD(1/8) = 9
 * vadd(3)   Add FSP|FDP(1/4) = 15
 * vnmls(3)  Negative Multiply Substractor (2/9) = 33
 *
 * 107(VFP)cycles
 *
 */
template<StrategyEnum strategy>
float cBiQuad::step(const float &input )
{
    float out = ( input * zeros[ 0 ] ) + z[ 0 ];
    for( size_t i = 0; i < ( z.size() -1 ); i++ )
        z[ i ] = ( input * zeros[ i +1 ] ) - ( poles[ i +1 ]  * out )  + z[ i +1 ];
    return o = out;
}

iFilter<cBiQuad>::spec_t cBiQuad::specs()
{
    return { .poles = this->poles.data(), .zeros = this->zeros.data() };
}

std::string cBiQuad::reportImpl()
{
    std::ostringstream oss;
    oss << "Fs(" << Fs <<  " Hz)" << ",Fc(" << Fc << " Hz),Q( " << Q << " )\n";
    oss << "Zeros(";
    for( size_t i = 0; i < z.size(); i++ )
	oss << "a" << i << " " << zeros[ i ] << " ";
    oss << ")Poles( ";
    for( size_t i = 0; i < z.size(); i++ )
	oss << "b" << i << " " << poles[ i ] << " ";
    oss << ")\n" ;
    return oss.str();
}

void cBiQuad::ZeroPoleMap(size_t Fs, size_t Fc, double Q, cBiQuad::Numerology type)
{

    double a0, a1, a2, b0 = 1, b1, b2, norm;
    double K = tan( 3.14159 * ( Fc * 1.0  / ( Fs ) ) );

    switch (type) {

    case LowPass:
        norm = 1 / (1 + K / Q + K * K);
        a0 = K * K * norm;
        a1 = 2 * a0;
        a2 = a0;
        b1 = 2 * (K * K - 1) * norm;
        b2 = (1 - K / Q + K * K) * norm;
        break;

    case Notch:
        norm = 1 / (1 + K / Q + K * K);
        a0 = (1 + K * K) * norm;
        a1 = 2 * (K * K - 1) * norm;
        a2 = a0;
        b1 = a1;
        b2 = (1 - K / Q + K * K) * norm;
        break;

    default:
        return;
    }

    poles[0] = static_cast<float>(b0);
    poles[1] = static_cast<float>(b1);
    poles[2] = static_cast<float>(b2);
    zeros[0] = static_cast<float>(a0);
    zeros[1] = static_cast<float>(a1);
    zeros[2] = static_cast<float>(a2);
}

cBiQuad2::cBiQuad2(const string &name, const std::array<float, 2> &poles, const std::array<float, 3> &zeros)
    : name_(name)
{
    using namespace std;
    vector<float> tmp_poles = { 1, poles[0], poles[1] };
    vector<float> tmp_zeros = { zeros[0], zeros[1], zeros[2] };
    bq = make_unique<cBiQuad>(name_, tmp_poles, tmp_zeros, 2 );
    specs = bq->specs();

}

cBiQuad2::cBiQuad2(const string &name, size_t Fs, size_t Fc, double Q)
    : name_(name)
{
    using namespace std;
    specs = ( bq = make_unique<cBiQuad>(name_, Fs, Fc, Q ) )->specs();
    weights = float32x4_t{ specs.zeros[1], specs.zeros[2], -specs.poles[1], -specs.poles[2]};
}

void cBiQuad2::settings(size_t Fs, size_t Fc, double Q){ bq->tune(Fs,Fc,Q); }
void cBiQuad2::reset(){ bq->reset(0); }

#include <arm_neon.h>

/*
 * JB#20042020 Neon Learning
 * inst 20(VFP)3(ARM)
 * Experimental roughly 100 ns : 1.5ns(666Mhz)) 70cycles
 *
 * On assembly check
 * https://godbolt.org/z/BAghnk
 * On cycling check
 * https://bitbucket.org/juanbaromance/zed-korolev/downloads/DDI0409G_cortex_a9_neon_mpe_r3p0_trm.pdf
 *
 * keywords:
 * s(single precision floating point)
 *
 * vldr(9) register loader VFP(1/1)
 * vmul(1) multiply spfp  SIMD(1/5)
 * vmla(4) multiply and acc (aka y = ax + b resolver) SIMD(1/8)
 * vmov(2) Move Register VFP(1/1)
 * vstr(4) Storage Register VFP(1/1)
 *
 * So expected 70 cycles
*/
template<StrategyEnum strategy>
inline float cBiQuad2::step( const float & sample ){

    if constexpr ( strategy == SIMD )
        return stepSIMD( sample );
    else if constexpr ( strategy == Compiler )
        return stepCompiler( sample );
    return sample;
}

template< bool debug>
inline float cBiQuad2::stepCompiler( const float & sample )
{
    float32_t acc{0};
    acc += w[0]   *  specs.zeros[1];
    acc += w[1]   *  specs.zeros[2];
    acc += w[2]   * -specs.poles[1];
    acc += w[3]   * -specs.poles[2];
    acc += sample *  specs.zeros[0];

    if constexpr ( debug )
    {
        using namespace std;
        cout <<  specs.zeros[1]*w[0] << "/";
        cout <<  specs.zeros[2]*w[1] << "/";
        cout << -specs.poles[1]*w[1] << "/";
        cout << -specs.poles[2]*w[3] << "\n";
    }

    // states suffling
    w[1] = w[0];
    w[0] = sample;
    w[3] = w[2];
    return w[2] = acc;
}


#include <arm_neon.h>
template< bool debug>
inline float cBiQuad2::stepSIMD( const float & sample )
{
    float32x4_t W{ w[0], w[1], w[2], w[3] };
    float32x4_t q = vmulq_f32( W, weights);
    float32x2_t d = vadd_f32(vget_low_f32(q),vget_high_f32(q));
    float acc = ( sample * specs.zeros[0] ) + vget_lane_f32( vpadd_f32(d,d), 0 );

    if constexpr ( debug )
    {
        using namespace std;
        cout << weights[0] << "/" << weights[1] <<"/" << weights[2] << "/" << weights[3] << "\n";
        cout << w[0] << "/" << w[1] <<"/" << w[2] << "/" << w[3] << "\n";
        cout << q[0] << "/" << q[1] <<"/" << q[2] << "/" << q[3] << "\n";
        cout << acc << "\n";
    }

    // states suffling
    w[1] = w[0];
    w[0] = sample;
    w[3] = w[2];
    return w[2] = acc;

}

bool cBiQuad2::testing()
{
    offline_testing(*this,"Compiler");
    return offline_testing<cBiQuad2,SIMD>(*this,"SIMD");
}
