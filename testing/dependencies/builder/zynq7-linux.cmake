
set ( CMAKE_SYSTEM_PROCESSOR "arm"            CACHE STRING "")
set ( MACHINE                "zynq7"          CACHE STRING "")
set ( CROSS_PREFIX           "arm-xilinx-linux-" CACHE STRING "")
set ( CMAKE_SYSTEM_NAME "Linux" )
set ( PETALINUX "/home/juanba/zynq-development/petalinux-sdk/2019.2" )
set ( SDK_SYSROOT  "${PETALINUX}/sysroots/cortexa9t2hf-neon-xilinx-linux-gnueabi/" )
# -march=armv7-a+simd -mvectorize-with-neon-quad

set ( CMAKE_CXX_FLAGS "-Wno-psabi -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 --sysroot=${SDK_SYSROOT}" )
set ( CMAKE_C_FLAGS "-Wno-psabi -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 --sysroot=${SDK_SYSROOT}" )
set ( CMAKE_C_COMPILER   ${CROSS_PREFIX}gcc )
set ( CMAKE_CXX_COMPILER ${CROSS_PREFIX}g++ )
set ( CMAKE_C_COMPILER_WORKS 1 )
set ( CMAKE_CXX_COMPILER_WORKS 1 )

set ( CMAKE_SYSROOT ${SDK_SYSROOT} )
set ( CMAKE_FIND_ROOT_PATH  ${SDK_SYSROOT} )
set ( CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set ( CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set ( CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
