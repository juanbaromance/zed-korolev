#define MODULE "ads1118_lib"

#include <dlfcn.h>
#include <string>

template <class T>
T plugginLoader( const std::string & symbol_name, const std::string & library = "" )
{
    void *lib_handler = dlopen( "", RTLD_LAZY );
    T e_symbol = nullptr;
    if( ( e_symbol = (T)(dlsym( lib_handler, symbol_name.c_str() ) ) ) )
        return e_symbol;
    if( ( lib_handler = dlopen( library.c_str(), RTLD_LAZY ) ) == nullptr )
        return e_symbol;
    return (T)( dlsym( lib_handler, symbol_name.c_str() ) );
}

#include <fstream>
#include <iostream>
#include <array>
#include <sstream>
#include <bitset>
#include <future>
#include <unistd.h>

static constexpr const char* hwmonEntry{"/sys/class/hwmon/hwmon1/in"};
static std::string gpa_entry{"master"};
static constexpr int noChannels{4};

static std::array<int,noChannels> channels;
static std::atomic<bool> running{true};

#include "fmt/format.h"
#include "fmt/color.h"
#include "fmt/core.h"
static auto mark1   = [](auto token){ return fmt::format( fmt::emphasis::bold | fg(fmt::color::green), token ); };
static auto warning = [](auto token){ return fmt::format( fmt::emphasis::bold | fg(fmt::color::orange), token ); };

#include <cstring>

extern "C" {

int ModuleBoot(void)
{
    auto roi = [](auto offset){ std::stringstream oss; oss << hwmonEntry << offset +1 << "_input"; return oss.str(); };
    std::stringstream oss;
    std::bitset<channels.size()> error;
    for( unsigned i = 0; i < channels.size(); i++ )
        if( error.set(i, access(roi(i).c_str(), R_OK) ).test(i) )
            std::cerr << fmt::format("io failure on {0} : {1}\n", mark1(roi(i)), warning(strerror(errno)));

    static auto f = std::async( std::launch::async, [&](){

        typedef int (*ExternalDecoder)(char *id, int channel, float val);
        using DecoderSignature = ExternalDecoder;
        auto dummy = []( char *entry, int offset, float val ){
            std::clog << fmt::format("{0}: {1}\n", mark1(fmt::format("{0}{1}", entry, offset )), val);
            return -1;
        };
        DecoderSignature system_decoder{dummy};
        if( auto f = plugginLoader<DecoderSignature>( "sys_decode_gpa" ); f )
            system_decoder = f;
        else
            std::cerr << fmt::format("{0} : mux on default stuff\n", warning(dlerror()));
        std::clog << fmt::format("Monitor {0}:{2} switched {1}\n", mark1(hwmonEntry), mark1("on"),
                                 std::bitset<channels.size()>(error).flip().to_string() );
        while( running )
        {
            for( unsigned offset = 0; offset < channels.size(); offset++ )
            {
                if( error.test(offset) == false )
                {
                    int value;
                    std::ifstream myfile;
                    myfile.open( roi(offset).c_str() );
                    if( myfile >> value )
                        system_decoder( gpa_entry.data(), offset, value );
                    else
                        std::cerr << fmt::format("io failure on: {0} : {1}\n", roi(offset), warning(strerror(errno)));
                }
            }
            using namespace std::chrono_literals;
            std::this_thread::sleep_for(250ms);
        }
        std::clog << fmt::format("Monitor {0} switched {1}\n", mark1(hwmonEntry), mark1("off") );

    });

    return 0;
}

int ModuleShutDown(void)
{
    running = false;
    return 0;
}

}
