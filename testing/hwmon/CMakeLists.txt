cmake_minimum_required(VERSION 3.1...3.18)
Project( hwmon-helper VERSION 1.0.0 )
set(Component hwmon-helper )
set(Topology SHARED)
set(CMAKE_CXX_STANDARD 17)
set(Artefact ${Component}.so)
set(Platform zynq7-linux)

include("${CMAKE_CURRENT_LIST_DIR}/dependencies/builder/generics.cmake")
set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -g3 -Wno-write-strings -Wno-unused-result -fstack-protector -fPIC")
add_definitions( -D${Platform} )

file(GLOB sources $(CMAKE_CURRENT_SOURCE_DIR) src/*.cpp  src/*.c )

message( STATUS " ${Blink}${Green}${Component}-${Tag}${Reset} ${Blue}${Platform}${Reset} platform building from  ${Green}${CMAKE_CURRENT_SOURCE_DIR}${Reset}\n" )
link_directories( ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/libs/build.${Platform} )

add_library ( ${Component} ${Topology} ${sources} )
target_include_directories (${Component} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} ./inc ./dependencies/include )
