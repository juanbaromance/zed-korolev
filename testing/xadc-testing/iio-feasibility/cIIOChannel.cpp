#include "cIIOChannel.h"
#include "iio/iio.h"
#include <iostream>
#include <iomanip>

cIIOchannel::cIIOchannel( struct iio_channel *channel_, size_t index_)
    : channel( channel_ ), name( iio_channel_get_name( channel ) ), index( index_ )
{
    name = name == nullptr ? "" : name;
    buf.resize(256);
    sync_attributes();
    std::stringstream oss;
    oss << iio_path << iio_device_get_id(iio_channel_get_device(channel))
        << "/in_" << iio_channel_get_id(channel)
        << "_" << name;
    base_name.assign( oss.str());

}

cIIOchannel::~cIIOchannel(){ }

double cIIOchannel::phy_state()
{
    return val = ( ( ( raw_val = strtod(buf.data(), nullptr ) ) + offset ) * scale );
}

double cIIOchannel::state()
{
    if( iio_channel_attr_read(  channel, "raw", buf.data(), buf.size() ) )
        raw_val = strtod(buf.data(), nullptr );
    return val = ( ( raw_val + offset ) * scale );
}

std::string cIIOchannel::dump() const
{
    std::stringstream oss;
    oss << "ch#" << std::setw(3) << index
        << " " << iio_channel_get_id(channel)
        << std::setw(10) << name << " : " << cached() << "/" << raw_val;
    return oss.str();
}

void cIIOchannel::sync_attributes()
{
    unsigned int nb = iio_channel_get_attrs_count(channel);
    const char *tmp;

    for ( size_t i = 0; i < nb; i++ )
        if( std::string( tmp = iio_channel_get_attr(channel,i)).compare("raw") )
            if( iio_channel_attr_read(  channel, tmp, buf.data(), buf.size() ) )
                attributes[ std::string(tmp) ] = strtod(buf.data(), nullptr );


    offset = ( attributes.find("offset") == attributes.end() ) ? 0 : attributes.find("offset")->second;
    scale  = ( attributes.find("scale") == attributes.end() )  ? 1 : attributes.find("scale")->second;
    state();
}
