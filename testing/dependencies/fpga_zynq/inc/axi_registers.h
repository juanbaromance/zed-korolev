#ifndef __AXI_REGISTERS__
#define __AXI_REGISTERS__

#define REGISTER_SIZE_IN_BYTES             4
#define CTRL_REG_OFFSET                    0x0 // read/write   
#define INT_ENABLE_REG_OFFSET              0x1 // COW/R
#define INT_FLAG_REG_OFFSET                0x2 // W/R
#define VERSION_REG_OFFSET                 0x3



//VERSIONS APP REGISTERS //
#define VERSIONS_CTRL_REG_OFFSET           0x0 // read/write   
#define VERSIONS_INT_ENABLE_REG_OFFSET     0x1 // COW/R
#define VERSIONS_INT_FLAG_REG_OFFSET       0x2 // W/R
#define VERSIONS_VERSION_REG_OFFSET        0x3
#define VERSIONS_SPARE_REG_4               0x4
#define VERSIONS_SPARE_REG_5               0x5
#define VERSIONS_SPARE_REG_6               0x6
#define VERSIONS_SPARE_REG_7               0x7
#define VERSIONS_SPARE_REG_8               0x8
#define VERSIONS_SPARE_REG_9               0x9
#define VERSIONS_SPARE_REG_10              0xA
#define VERSIONS_SPARE_REG_11              0xB
#define VERSIONS_SPARE_REG_12              0xC
#define VERSIONS_SPARE_REG_13              0xD
#define VERSIONS_SPARE_REG_14              0xE
#define VERSIONS_SPARE_REG_15              0xF
#define VERSIONS_BOARD_ID0_B1_OFFSET      0x10
#define VERSIONS_BOARD_ID0_B2_OFFSET      0x11
#define VERSIONS_BOARD_ID1_OFFSET         0x12
#define VERSIONS_BOARD_ID2_OFFSET         0x13
#define VERSIONS_FW_VER_B1_OFFSET         0x14
#define VERSIONS_FW_VER_B2_OFFSET         0x15
#define VERSIONS_DNA_B1_OFFSET            0x16
#define VERSIONS_DNA_B2_OFFSET            0x17
#define VERSIONS_DNA_B3_OFFSET            0x18
#define VERSIONS_DNA_B4_OFFSET            0x19
#define VERSIONS_DNA_B5_OFFSET            0x1A
#define VERSIONS_DNA_B6_OFFSET            0x1B
#define VERSIONS_DNA_B7_OFFSET            0x1C
#define VERSIONS_DNA_B8_OFFSET            0x1D

// WATCHDOGS APP REGISTERS //
#define WATCHDOGS_CTRL_REG_OFFSET          0x0 // read/write
#define WATCHDOGS_INT_ENABLE_REG_OFFSET    0x1 // COW/R
#define WATCHDOGS_INT_FLAG_REG_OFFSET      0x2 // W/R
#define WATCHDOGS_VERSION_REG_OFFSET       0x3 
#define WATCHDOGS_HEARTBEAT_B1_OFFSET      0x4 
#define WATCHDOGS_HEARTBEAT_B2_OFFSET      0x5 
#define WATCHDOGS_HEARTBEAT_B3_OFFSET      0x6 
#define WATCHDOGS_HEARTBEAT_B4_OFFSET      0x7 
#define WATCHDOGS_B1_OFFSET                0x8 
#define WATCHDOGS_B2_OFFSET                0x9 
#define WATCHDOGS_B3_OFFSET                0xA
#define WATCHDOGS_B4_OFFSET                0xB

// ONE WIRE APP REGISTERS //
#define ONE_WIRE_CTRL_REG_OFFSET           0x0 // read/write
#define ONE_WIRE_INT_ENABLE_REG_OFFSET     0x1 // COW/R
#define ONE_WIRE_INT_FLAG_REG_OFFSET       0x2 /// W/R
#define ONE_WIRE_VERSION_REG_OFFSET        0x3
#define ONE_WIRE_TEST_REG_OUT              0x4 //WR

// GENERAL IO APP REGISTERS //
#define GENERAL_IO_CTRL_REG_OFFSET         0x0 // read/write
#define GENERAL_IO_INT_ENABLE_REG_OFFSET   0x1 // COW/R
#define GENERAL_IO_INT_FLAG_REG_OFFSET     0x2 // W/R
#define GENERAL_IO_VERSION_REG_OFFSET      0x3
#define GENERAL_IO_BUTTONS_OFFSET          0x4 
#define GENERAL_IO_LEDS_OFFSET             0x5 
#define GENERAL_IO_BEEP_OFFSET             0x6
#define GENERAL_IO_PWM_R_CONTROL_OFFSET    0x7 
#define GENERAL_IO_PWM_R_PERIOD_B1_OFFSET  0x8 
#define GENERAL_IO_PWM_R_PERIOD_B2_OFFSET  0x9 
#define GENERAL_IO_PWM_R_PERIOD_B3_OFFSET  0xA
#define GENERAL_IO_PWM_R_PERIOD_B4_OFFSET  0xB
#define GENERAL_IO_PWM_R_DUTY_OFFSET       0xC
#define GENERAL_IO_PWM_G_CONTROL_OFFSET    0xD
#define GENERAL_IO_PWM_G_PERIOD_B1_OFFSET  0xE
#define GENERAL_IO_PWM_G_PERIOD_B2_OFFSET  0xF
#define GENERAL_IO_PWM_G_PERIOD_B3_OFFSET  0x10
#define GENERAL_IO_PWM_G_PERIOD_B4_OFFSET  0x11
#define GENERAL_IO_PWM_G_DUTY_OFFSET       0x12
#define GENERAL_IO_PWM_B_CONTROL_OFFSET    0x13
#define GENERAL_IO_PWM_B_PERIOD_B1_OFFSET  0x14
#define GENERAL_IO_PWM_B_PERIOD_B2_OFFSET  0x15
#define GENERAL_IO_PWM_B_PERIOD_B3_OFFSET  0x16
#define GENERAL_IO_PWM_B_PERIOD_B4_OFFSET  0x17
#define GENERAL_IO_PWM_B_DUTY_OFFSET       0x18
#define GENERAL_IO_PWM_BL_CONTROL_OFFSET   0x19
#define GENERAL_IO_PWM_BL_PERIOD_B1_OFFSET 0x1A
#define GENERAL_IO_PWM_BL_PERIOD_B2_OFFSET 0x1B
#define GENERAL_IO_PWM_BL_PERIOD_B3_OFFSET 0x1C
#define GENERAL_IO_PWM_BL_PERIOD_B4_OFFSET 0x1D
#define GENERAL_IO_PWM_BL_DUTY_OFFSET      0x1E
#define GENERAL_IO_GPIO_OUT_B1_OFFSET      0x1F
#define GENERAL_IO_GPIO_OUT_B2_OFFSET      0x20
#define GENERAL_IO_GPIO_IN_B1_OFFSET       0x21
#define GENERAL_IO_GPIO_IN_B2_OFFSET       0x22
#define GENERAL_IO_BRAKES_B1_OFFSET        0x23
#define GENERAL_IO_BRAKES_B2_OFFSET        0x24
#define GENERAL_IO_CLUTCHES_B1_OFFSET      0x25
#define GENERAL_IO_CLUTCHES_B2_OFFSET      0x26
#define GENERAL_IO_DIG_IN_B1_OFFSET        0x27
#define GENERAL_IO_DIG_IN_B2_OFFSET        0x28

// IR APP REGISTERS //
#define IR_CTRL_REG_OFFSET                 0x0 // read/write
#define IR_INT_ENABLE_REG_OFFSET           0x1 // COW/R
#define IR_INT_FLAG_REG_OFFSET             0x2 // W/R
#define IR_VERSION_REG_OFFSET              0x3
#define IR_C_K_CODE_IR                     0x4
#define IR_C_MSG_FLAGS_B1                  0x5
#define IR_C_MSG_FLAGS_B2                  0x6
#define IR_C_MSG_FLAGS_B3                  0x7
#define IR_C_MSG_FLAGS_B4                  0x8

#endif
