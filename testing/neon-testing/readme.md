# Purpose

This repository is a feasibility test program related with the [**Neon**](https://bitbucket.org/juanbaromance/zed-avnet/wiki/Neon) unit

It is a first approach to the development of applications which uses the Neon ARM coproceesor. The repository, however, is focused on the Zynq7000 case of use

It will explore the fourth methods available to encode such kind of applications, it means: c/c++ optimizations/strategies, intrinsics, third party libraries ([Neo10](https://github.com/projectNe10/Ne10)) and minimally support with assembly functions

The calculations under study are **real-time** processing, **data fitting/interpolators** and **Afin transformations**

The deployment language is c++ with the 17 extension

The artefact will use the dynamic dependency of the [**neo10 library**](https://github.com/projectNe10/Ne10), so it must be properly deployed on the target. If it's required, apply on the Makefile rule **deploy** to fix it

The main Xilinx reference on Neon can be checkout on the [xapp1206](https://bitbucket.org/juanbaromance/zed-korolev/downloads/xapp1206-boost-sw-performance-zynq7soc-w-neon.pdf)

## Directory Layout

```
.
├── afin
│   ├── cAfin.cpp
│   ├── cAfin.h
│   ├── CMakeLists.txt
│   └── matrix_asm_sched.s
├── build
├── CMakeLists.txt
├── cZynqSnapshot.cpp
├── cZynqSnapshot.h
├── dependencies -> ../dependencies/
├── documentation
│   ├── biquad.testing.txt
│   ├── data.txt
│   ├── nurbs.195821042020.compressor.txt
│   ├── oscilator.txt
│   ├── output.cBiQuad2.Compiler.txt
│   ├── output.cBiQuad2.SIMD.txt
│   ├── output.cFFTView.txt
│   ├── output.cNurbs.txt
│   ├── output.gBiQuad.txt
│   ├── output.Spline.txt
│   ├── ploter.gnuplot
│   ├── steffen.spline
│   │   ├── 210024082019.compressor.txt
│   │   └── 213524082019.compressor.txt
│   └── zynq.signal.neon.pdf
├── filtering
│   ├── cBiquad.cpp
│   ├── cBiquad.h
│   ├── cFFTView.cpp
│   ├── cFFTView.h
│   ├── cFilter.h
│   ├── cFIR.cpp
│   ├── cFIR.h
│   ├── CMakeLists.txt
│   └── Lowess.h
├── geometry
│   ├── geometry.cpp
│   └── geometry.h
├── interpolators
│   ├── CMakeLists.txt
│   ├── interpolators.cpp
│   ├── interpolators.h
│   ├── nurbs.cpp
│   ├── nurbs.h
│   ├── Segment7Profiler.cpp
│   ├── Segment7Profiler.h
│   └── spline.hpp
├── main.cpp
├── main.h
├── neo10_testing.cpp
├── neo10_testing.h
├── readme.md
└── util.h

```

## Build steps

If the project is properly cloned, you should have the filesystem layout ready to compile the sources

```
mkdir build
cd build
cmake .. -DCMAKE_TOOLCHAIN_FILE=../dependencies/builder/zynq7-linux.cmake
make
```

## Target install

Tweak the top [**CMakelist.txt**](https://bitbucket.org/juanbaromance/zed-korolev/src/master/testing/neon-testing/CMakeLists.txt) with your zynq-7000 target, afterwards apply on the **build.zynq** directory entry

```
make install
```
