/*
 * AXI XADC DMA driver.
 * Configures XADC in continuous stream and DMA in simple mode.
 * Uses DMA Client API to configure Xilinx DMA engine. 
 * Provide interface to read XADC Samples.
 * Copyright (C) 2012 Xilinx, Inc.
 * Copyright (C) 2012 Robert Armstrong
 *
 * Author: Radhey Shyam Pandey <radheys@xilinx.com>
 * 
 * Based on PL330 DMA Driver.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * JB@2020(Port to the 2020 era) :
 * refactors on the Newspaper design pattern, symbol isolation
 */

#include <asm/uaccess.h>
#include <linux/dma/xilinx_dma.h>
#include <linux/cdev.h>
#include <linux/dma-mapping.h>
#include <linux/dmaengine.h>
#include <linux/err.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/of_address.h>
#include <linux/platform_device.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include "axi_xadc_dma.h"

/*ADC channel name */ 
static const char adc_channels[][20] = { {"channel0"},};
static struct axi_xadc_driver *kdrv;

#ifndef nullptr
#define nullptr NULL
#endif

/* Function Declarations */
static int axi_xadc_dma_init(void);
static int axi_xadc_dma_config(void);
static void axi_xadc_slave_rx_callback(void *completion);

static ssize_t axi_xadc_dma_read(struct file * filep, char __user * buf, size_t count, loff_t * f_pos);
static int  axi_xadc_dma_open(struct inode *inode, struct file *filp);
static long axi_xadc_dma_ioctl( struct file *file, unsigned int cmd, unsigned long arg);
static int  axi_xadc_dma_release(struct inode *inode, struct file *filp);
static int  axi_xadc_remove(struct platform_device *);
static int  axi_xadc_probe( struct platform_device *pdev );


/* filter_fn' routine will be called once for each free
channel which has a capability in 'mask'.It is expected to
return 'true' when the desired DMA channel is found
*/
static bool axi_xadc_filter(struct dma_chan *chan, void *param)
{
    dev_info( kdrv->dev, "%s: $%08x vs $%08x\n", __PRETTY_FUNCTION__ ,
              *((int *) chan->private),
              *(int *) param );
    return (*((int *) chan->private) == *(int *) param) ? true : false;
}

/* Pre DMA initialization . Request handle to Xilinx DMA engine */
static int axi_xadc_dma_init()
{
    dma_cap_mask_t mask;
    dma_cap_zero(mask);
    dma_cap_set( DMA_SLAVE | DMA_PRIVATE, mask);
    u32 match = (DMA_DEV_TO_MEM & 0xFF) | XILINX_DMA_IP_DMA;
    kdrv->rx_chan = dma_request_channel( mask, axi_xadc_filter, & match );

    if ( kdrv->rx_chan ) {
        dev_info( kdrv->dev, "Found DMA.rx device\n");
    }
    else {
        dev_err( kdrv->dev, "%s: Did not find DMA.rx channel coupled to $%08x\n",
                 __FUNCTION__ , match );
        return FAILURE;
    }
    if ( axi_xadc_dma_config() ) {
        dma_release_channel(kdrv->rx_chan);
        return FAILURE;
    }
    return SUCCESS;
}

/*Configures buffer/flags,timeouts for DMA */
static int axi_xadc_dma_config()
{
    int ret = SUCCESS;
    size_t buf_size = AXI_XADC_BUFF_SIZE;
    struct dma_device *rx_dev = NULL;

    /* Allocate BD descriptors buffers */
    kdrv->dsts = kzalloc(buf_size, GFP_KERNEL);
    if (!kdrv->dsts)
        goto err_srcbuf;

    // set_user_nice(current, 10);
    kdrv->flags = DMA_CTRL_ACK |DMA_PREP_INTERRUPT;
    rx_dev = kdrv->rx_chan->device;
    /* RX can takes longer */
    kdrv->rx_tmo = msecs_to_jiffies(AXI_XADC_CALLBACK_TIMEOUTMSEC);
    return ret;

  err_srcbuf:
    return -ENOMEM;
}

static void axi_xadc_dma_start(void)
{
    /*flush pending transactions to HW */
    dma_async_issue_pending(kdrv->rx_chan);
}
static void axi_xadc_dma_stop( struct dma_chan *chan )
{
    /* Terminate DMA transactions */
    if (chan)
    {
        struct dma_device *chan_dev = kdrv->rx_chan->device;
        chan_dev->device_terminate_all(kdrv->rx_chan);
    }
}

static void axi_xadc_read_reg(struct axi_xadc_driver *xadc_dev, unsigned int reg, uint32_t * val)
{
    *val = readl(xadc_dev->xadc_virtaddr + reg);
}

/* Read interface to Adapter block */
static void axi_xadc_write_reg(struct axi_xadc_driver *xadc_dev, unsigned int reg, uint32_t val)
{
    writel(val, xadc_dev->xadc_virtaddr + reg);
}

/* Write interface to Adapter block */
static void axi_fifo_write_reg(struct axi_xadc_driver *xadc_dev, unsigned int reg, uint32_t val)
{
    writel( val, xadc_dev->axififo_virtaddr + reg );
}

static void axi_xadc_slave_rx_callback( void *completion )
{
    (void)completion;
    /* Unmap a single streaming mode DMA translation. */
    dma_unmap_single( kdrv->rx_chan->device->dev,
                      kdrv->dma_dsts, AXI_XADC_BUFF_SIZE, DMA_DEV_TO_MEM);
    complete( & kdrv->rx_cmp );
}

static struct file_operations axi_xadc_fops = {
    .owner = THIS_MODULE,
    .read = axi_xadc_dma_read,
    .open = axi_xadc_dma_open,
    .unlocked_ioctl = axi_xadc_dma_ioctl,
    .release = axi_xadc_dma_release
};

static ssize_t axi_xadc_dma_read(struct file * filep, char __user * buf, size_t count, loff_t * f_pos)
{
    if (count > AXI_XADC_BUFF_SIZE)
    {
        dev_err( kdrv->dev, "improper buffer size \n");
        return EINVAL;
    }

    {
        unsigned long rx_tmo = wait_for_completion_timeout( & kdrv->rx_cmp, kdrv->rx_tmo);
        dma_async_is_tx_complete(kdrv->rx_chan, kdrv->rx_cookie, NULL, NULL);
        if ( rx_tmo == 0 ) {
            dev_err(kdrv->dev, "RX test timed out\n");
            return -EAGAIN;
        }
    }
    return ( ssize_t )copy_to_user( buf, kdrv->dsts, count );

}

static int axi_xadc_dma_open( struct inode *inode, struct file *filp )
{
    filp->private_data = ( void * )iminor(inode);
    return SUCCESS;
}

static long axi_xadc_dma_ioctl( struct file *file, unsigned int cmd, unsigned long arg)
{
    (void)file;
    (void)arg;

    switch (cmd) {
    case AXI_XADC_DMA_CONFIG:
    {
        /*Configures DMA transaction */
        struct dma_device *rx_dev = kdrv->rx_chan->device;
        kdrv->dma_dsts = dma_map_single( rx_dev->dev, kdrv->dsts, AXI_XADC_BUFF_SIZE, DMA_DEV_TO_MEM);

        /* Preparing mapped scatter-gather list */
        sg_init_table(&kdrv->rx_sg, AXI_XADC_BUF_COUNT);
        sg_dma_address(&kdrv->rx_sg) = kdrv->dma_dsts;
        /* Configures S2MM data length */
        sg_dma_len(&kdrv->rx_sg) = AXI_XADC_BUFF_SIZE;

        /* Only one interrupt */
#ifdef DEPRECATED_API
        struct xilinx_dma_config config;
        config.coalesc = 1;
        config.delay = 0;
        rx_dev->device_config(axi_xadc_dev->rx_chan, DMA_SLAVE_CONFIG,
                              (unsigned long) &config);
#else

        {
            struct dma_slave_config dma_rx_conf;
            memset( & dma_rx_conf, 0, sizeof( dma_rx_conf ) );
            dma_rx_conf.direction = DMA_DEV_TO_MEM;
            dma_rx_conf.src_addr = (unsigned long) 0x40000000;
            dma_rx_conf.src_addr_width = DMA_SLAVE_BUSWIDTH_4_BYTES;
            dma_rx_conf.src_maxburst = 1; // <--- PL330 driver is hard-coded to 1. This is ignored.
            rx_dev->device_config( kdrv->rx_chan, & dma_rx_conf );
        }

#endif
        /* Obtaining DMA descriptor */
        {
            struct dma_async_tx_descriptor *rxd =
                    rx_dev->device_prep_slave_sg(kdrv->rx_chan,
                                                 &kdrv->rx_sg,
                                                 AXI_XADC_BUF_COUNT,
                                                 DMA_DEV_TO_MEM,
                                                 kdrv->flags, NULL);
            if (!rxd) {
                dma_unmap_single(rx_dev->dev, kdrv->dma_dsts, AXI_XADC_BUFF_SIZE, DMA_DEV_TO_MEM);
                dev_err( kdrv->dev, "dma_unmap  error \n");
                return -EIO;
            }
            init_completion( & kdrv->rx_cmp );

            /* Added callback information */
            rxd->callback = axi_xadc_slave_rx_callback;
            rxd->callback_param = &kdrv->rx_cmp;
            /*Place transaction to DMA engine pending queue */
            kdrv->rx_cookie = rxd->tx_submit(rxd);
        }

        /* Check for dma submit errors */
        if (dma_submit_error(kdrv->rx_cookie)) {
            dev_err(kdrv->dev, "dma_submit error \n");
            return -EIO;
        }
     }
        break;

    case AXI_XADC_DMA_START:
        axi_xadc_dma_start();
        break;

    case AXI_XADC_DMA_STOP:
        axi_xadc_dma_stop(kdrv->rx_chan);
        break;

    default:
        return -EOPNOTSUPP;

    }
    return SUCCESS;

}
static int axi_xadc_dma_release(struct inode *inode, struct file *filp){ return SUCCESS;}


static int axi_xadc_remove(struct platform_device *unused)
{
    (void)unused;
    dev_info( kdrv->dev, "Let's release xadc-dma resources mask $%08x\n", kdrv->resources_mask );
    cdev_del( & kdrv->cdev);
    device_destroy(kdrv->axi_xadc_class,MKDEV(MAJOR(kdrv->devno), AXI_XADC_MINOR_START));
    class_destroy(kdrv->axi_xadc_class);
    unregister_chrdev_region(kdrv->devno, AXI_XADC_MINOR_COUNT);
    if( kdrv->rx_chan )
        dma_release_channel(kdrv->rx_chan);

    if (kdrv->xadc_virtaddr)
        iounmap(kdrv->xadc_virtaddr);

    if ( kdrv->axififo_virtaddr )
        iounmap(kdrv->axififo_virtaddr);
    dev_info( kdrv->dev, "Unload : Success\n");
    kfree(kdrv);
    return SUCCESS;
}

/* Configures Xilinx ADC using AXI interface */
static void axi_xadc_setup(void)
{
    /* Initialize XADC block ,sets sampling frequency,
     * operating modes etc
     */
    uint32_t RegValue = 0;
    u8 SequencerMode = XSM_SEQ_MODE_SINGCHAN;
    u32 ChEnableMask = XSM_SEQ_CH_VPVN;
    u8 Divisor = XSM_DEF_DIVISOR;
    /*
     * Set the specified sequencer mode in the Configuration Register 1.
     */
    axi_xadc_read_reg(kdrv, XSM_CFR1_OFFSET, &RegValue);
    RegValue &= ~((uint32_t)XSM_CFR1_SEQ_VALID_MASK);
    RegValue |= ((SequencerMode << XSM_CFR1_SEQ_SHIFT) &
         XSM_CFR1_SEQ_VALID_MASK);
    axi_xadc_write_reg(kdrv, XSM_CFR1_OFFSET, RegValue);

    /*
     * Enable the specified channels in the ADC Channel Selection Sequencer
     * Registers.
     */
    axi_xadc_write_reg(kdrv,
               XSM_SEQ00_OFFSET,
               (ChEnableMask & XSM_SEQ00_CH_VALID_MASK));

    axi_xadc_write_reg(kdrv,
               XSM_SEQ01_OFFSET,
               (ChEnableMask >> XSM_SEQ_CH_AUX_SHIFT) &
               XSM_SEQ01_CH_VALID_MASK);

    /* Write the divisor value into the Configuration Register #2 */
    axi_xadc_write_reg(kdrv, XSM_CFR2_OFFSET,
               Divisor << XSM_CFR2_CD_SHIFT);
    SequencerMode = XSM_SEQ_MODE_CONTINPASS;
    /* Set the specified sequencer mode in the Configuration Register #1 */
    axi_xadc_read_reg(kdrv, XSM_CFR1_OFFSET, &RegValue);
    RegValue &= ~(uint32_t)XSM_CFR1_SEQ_VALID_MASK;
    RegValue |= ((SequencerMode << XSM_CFR1_SEQ_SHIFT) &
         XSM_CFR1_SEQ_VALID_MASK);
    axi_xadc_write_reg(kdrv, XSM_CFR1_OFFSET, RegValue);

}
static int  axi_xadc_probe( struct platform_device *pdev)
{
    int status = 0;
    struct device *dev = & pdev->dev;
    dev_info( dev, "%s: sequenced\n", __PRETTY_FUNCTION__ );

    struct device_node *node = dev->of_node;
    if ( ( kdrv = kmalloc(sizeof(struct axi_xadc_driver ), GFP_KERNEL) ) == NULL) {
        dev_err( dev, "unable to allocate device structure\n");
        return -ENOMEM;
    }
    memset( kdrv, 0, sizeof(struct axi_xadc_driver));
    kdrv->dev = & pdev->dev;

    if ( ( kdrv->xadc_virtaddr = of_iomap(node, 0) ) == nullptr )
    {
        dev_err( dev, "unable to remap xadc registers\n");
        status = -ENOMEM;
        goto fail1;
    }

    if ( ( kdrv->axififo_virtaddr = of_iomap( node, 1 ) ) == nullptr ) {
        dev_err( dev, "unable to remap axififo registers\n");
        status = -ENOMEM;
        goto fail2;
    }

    if( ( status = alloc_chrdev_region( & kdrv->devno, 0, AXI_XADC_MINOR_COUNT, MODULE_NAME) ) < 0 )
    {
        dev_err( dev, "unable to alloc chrdev\n");
        goto fail3;
    }

    /* Kernel registering as a new character device coupled on */
    cdev_init( & kdrv->cdev, & axi_xadc_fops);
    kdrv->cdev.owner = THIS_MODULE;
    kdrv->cdev.ops = & axi_xadc_fops;

    /* Initialize our device mutex */
    mutex_init( & kdrv->mutex);
    status = cdev_add( & kdrv->cdev, kdrv->devno, AXI_XADC_MINOR_COUNT);
    kdrv->axi_xadc_class = class_create(THIS_MODULE, MODULE_NAME);
    device_create(kdrv->axi_xadc_class, NULL, MKDEV(MAJOR(kdrv->devno), AXI_XADC_MINOR_START), NULL, adc_channels[0]);
    dev_info( dev, "Xilinx axi-XADC character device majored as %d.%d: created : fifo(%p), xadxc(%p)\n",
              MAJOR(kdrv->devno), AXI_XADC_MINOR_START, kdrv->axififo_virtaddr, kdrv->xadc_virtaddr );

    if( axi_xadc_dma_init() == SUCCESS )
    {
        axi_fifo_write_reg( kdrv, 0x00, AXI_XADC_NO_OF_SAMPLES);
        axi_xadc_setup();
        dev_info( dev, "AXI XADC configured \n");
    }

    return SUCCESS;

    //Clean up
fail3:
    iounmap(kdrv->axififo_virtaddr);
fail2:
    iounmap(kdrv->xadc_virtaddr);
fail1:
    kfree(kdrv);
    return status;
}


static const struct of_device_id axi_xadc_dma_of_ids[] = {{.compatible = "xlnx,axi-xadc-dma",},};
static struct platform_driver axi_xadc_dma_of_driver = {
    .driver = {
        .name = MODULE_NAME,
        .owner = THIS_MODULE,
        .of_match_table = axi_xadc_dma_of_ids,
    },
    .probe  = axi_xadc_probe,
    .remove = axi_xadc_remove,
};
module_platform_driver(axi_xadc_dma_of_driver);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Xilinx AXI XADC DMA driver");
MODULE_AUTHOR("Xilinx, Inc.");
MODULE_VERSION("1.00a");
