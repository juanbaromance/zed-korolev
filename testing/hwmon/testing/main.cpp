extern "C" {

extern int ModuleBoot();
extern int ModuleShutDown();

}

#include <future>
#include <iostream>
#include "fmt/core.h"
using namespace std::chrono_literals;

int main()
{
    std::clog << fmt::format("{0}\n", __PRETTY_FUNCTION__ );
    ModuleBoot();
    std::this_thread::sleep_for(2s);
    return ModuleShutDown();

}
