#ifndef __GPIO_A3633_lib__
#define __GPIO_A3633_lib__


#ifdef __cplusplus
extern  "C" {
#endif


#ifndef u16
# define u16 unsigned short
#endif
#ifndef u32
# define u32 unsigned int
#endif

void gpio_boot        ( u32 mask, u32 value );
void gpio_write_lines ( u32 mask, u32 value );
u32  gpio_read_lines  ( u32 mask );
u32  gpio_read_int    ( u32 mask );
void gpio_configure   ( u32 mask, u32 value );

typedef void (*gpio_slot_cb)( unsigned int data, void *user_arg );
int gpio_slot_create( char *name, gpio_slot_cb decode, void *user_arg, u32 mask );


#ifdef __cplusplus
	}
#endif


#endif

