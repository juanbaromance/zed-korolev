#ifndef __UTIL_GEOMETRY_INC
#define __UTIL_GEOMETRY_INC



#ifdef __cplusplus
extern  "C" {
#endif

void  inv_homo_mat ( void *m_inv, void *m );
void  mat_mul41    ( void *p, const void *m1, const void *m2 );
void  mat_mul44    ( void *p, const void *m1, const void *m2 );

void  eye_mat( void *m );
void  zero_pt( void *p );
float cross_prod ( float *p0, float *p1, float *p2, float *p3 );
float dist_pt_pt ( float *a, float *b );

/**
 * para_line_intersect_xxxx family return values
 * \ingroup geometry
 */
enum
{
	NO_INTERSECTION=0,
	INTERSECTION=1
};
/** @fn int para_line_intersect ( float *point, float *p1, float *p2, float *p3, float *v1, float *v2 );
* @brief Find the point of intersection between a vector and a parallelogram in the ecuclidean space R3
* <ol>
* <li>The sense of the vector is taken in account.
* <li>The calculation is generic so the full description problem description must be coherent in terms of the reference frame applied on the problem parameters
* </ol>

* @param[in] p1 Parallelogram top left specification
* @param[in] p2 Parallelogram top right specification
* @param[in] p3 Parallelogram bottom left specification
* @param[in] v1 start vector point
* @param[in] v2 end vector point
* @param[out] intersection point if any
* @return TRUE if the intersection is found
* @return FALSE otherways
*/


/**
 * @fn int   para_line_intersect_strict( float *point, float *p1, float *p2, float *p3, float *p4, float *v1, float *v2 );
 * Check if a parallelogram and a line intersect in a strict form.
 * With this we check if a finite line defined by the vector and a finite plane (a.k.a surface)
 * intersect and in affirmative case, return the intersection point.
 * This is necessary to overload the function \b para_line_intersect, that checks if the infinite line defined by the vector and
 * the infinite plane defined by the 3 points intersect.
 *
 * \remarks This solution is valid if and only if the parallelogram faces are axis-aligned.
 *
 * @param [out] point	Intersection point.
 * @param [in] p1 First surface point.
 * @param [in] p2 Second surface point.
 * @param [in] p3 Third surface point.
 * @param [in] p4 Fourth surface point.
 * @param [in] v1 Vector initial point.
 * @param [in] v2 Vector end point.
 * @return
 * 	- NO_INTERSECTION if no intersection point is found
 * 	- INTERSECTION If any intersection point is found, and it is returned in \bpoint.
 */

int   para_line_intersect ( float *point, float *p1, float *p2, float *p3, float *v1, float *v2 );
int   para_line_intersect_strict( float *point, float *p1, float *p2, float *p3, float *p4, float *v1, float *v2 );

float angle_lines_2d( float a1, float a2, float b1, float b2,
		      float c1, float c2, float d1, float d2 );
float angle_lines_3d( float *P1, float *P2, float *P3, float *P4 );
float vmod_2d( float x, float y );
float vmod_3d( float x, float y, float z );


  void print_4x4(const void *matrix);
  void print_point(const void *point, void *forward, char *device );
  void dump_matrix_4x4( const char *caller, float data[][4] );


#ifdef __cplusplus
}
#endif


#endif
