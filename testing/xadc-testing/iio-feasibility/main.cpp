#include <memory>
#include "cIIODevice.h"

enum {
    DieTemperature = 3,
    PSAuxiliarySupplyVoltage = 2
};

class cIIOAlarmObserver :
    public iAlarm::iNotifier<double>,
    std::enable_shared_from_this<cIIOAlarmObserver>,
    public Module<cIIOAlarmObserver>
{

public:
    cIIOAlarmObserver( size_t mask_ = 0 ) : Module("","cIIOAlarmObserver"), mask{ mask_ } {}
    size_t masking(){ return mask.to_ulong(); }
    std::shared_ptr<cIIOAlarmObserver> reference() { return shared_from_this(); }
    std::string internalAuditory() const { return ""; }

    // iNotifier interface
private:
    const char *whoAmI(){ return "main.cIIOAlarmObserver"; }
    void event( const iAlarm::eventScope & mask, const double & value )
    {
        iAlarm::eventScope tmp( mask & this->mask );
        for( size_t i = 0; tmp.any(); i++ )
            if( tmp.test( i ) )
            {
                std::stringstream oss;
                oss << whoAmI() << " triggered on ch#" << i << " with " << value;
                auditor(oss);
                tmp.reset(i);
            }
    }
    iAlarm::eventScope mask;
    friend Module<cIIOAlarmObserver>;
};


int main()
{
    std::unique_ptr<cIIOdevice> iio_device{ std::make_unique<cIIOdevice>() };
    std::shared_ptr<cIIOAlarmObserver> observer{ std::make_shared<cIIOAlarmObserver>(
        ( 1 << DieTemperature ) |
        ( 1 << PSAuxiliarySupplyVoltage ) |
        ( 1 << 31 ) ) };

    iio_device->subscribeAlarm( observer->masking(), observer );

    iAlarm::alarm_params_t alarms{ {DieTemperature,59000,30}, { PSAuxiliarySupplyVoltage, 1700, 1600 } };
    std::bitset<4>tmp{ (iio_device->setAlarmParams( alarms ) ) };

    // iio_device->latencyObserver();

    std::cout << "\n";
    while( 1 ){
        std::this_thread::sleep_for(std::chrono::milliseconds(5000));
        iChannel::state_t states{ iio_device->channelState( 1 << DieTemperature ) };
        for( auto s : states )
        {
            auto & [ t, raw_t ] = s;
            std::cout << t << ":" << raw_t << "\n";
        }
    }
    return 0;
}

