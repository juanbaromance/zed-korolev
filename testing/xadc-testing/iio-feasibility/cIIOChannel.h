#pragma once

#include <string>
#include <map>
#include <cmath>
#include <vector>
#include <bitset>

class iChannel {
public:
    using analog  = double;
    using digital = size_t;
    using state_t = std::vector<std::tuple<analog,digital>>;
    using channelScope = std::bitset<32>;
    virtual state_t channelState( const channelScope & ) = 0;
};

class cIIOchannel {

public:
    static constexpr const char* iio_path{"/sys/bus/iio/devices/"};
    cIIOchannel( struct iio_channel *channel_ , size_t index_ );
    ~cIIOchannel();

    double phy_state();
    double state();
    std::string dump() const;
    inline double cached() const { return val; }
    inline const char* whoami() const { return name; }
    inline std::string sys_name() const { return base_name; }
    inline size_t xadc_index() const { return index; }
    inline size_t raw() { return static_cast<size_t>( round( raw_val )); }
    inline struct iio_channel* phy(){ return channel; }
    enum {
        Offset,
        Scale
    };
    using converter = std::tuple<double,double>;
    converter scaling() const { return {offset,scale}; }

private:

    void sync_attributes();

    std::map<std::string,double> attributes;
    struct iio_channel *channel;
    const char* name;

    std::string buf, base_name;
    double raw_val, offset, scale, val;
    size_t index;

};
