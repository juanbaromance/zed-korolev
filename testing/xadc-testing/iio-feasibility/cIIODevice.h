#pragma once

#include "util.h"
#include "cIIOChannel.h"
#include "cIIOAlarm.h"
#include <string>
#include <vector>
#include <memory>



class cIIOdevice : public Module<cIIOdevice>, public iAlarm, public iChannel {

public:
    cIIOdevice( const std::string & iio_name_ = "xadc", const std::string & trigger = "instance1" );
    ~cIIOdevice();
    void latencyObserver();

    size_t setAlarmParams( const alarm_params_t & );
    eventScope subscribeAlarm( const eventScope & mask, std::shared_ptr<iNotifier<>> notifier);
    state_t channelState( const channelScope & mask);

private:
    using phy_iio_dev = struct iio_device;
    std::vector<cIIOchannel> channels;
    enum AcquisitionMode { Slow, Fast };

    void deployAlarms(const std::vector<cIIOchannel> &channels, const char *device);
    std::unique_ptr<cIIOAlarm> iio_alarm;

    void iio_latency();
    void scanContext();

    struct iio_context *ctx{nullptr};
    std::string auditor_val;
    std::string internalAuditory() const;
    std::string const iio_name{"xadc"}, iio_trigger{"instance1"};
    const std::string scm() const;

    cIIOdevice & auditorError();
    const phy_iio_dev *device, *trigger;
    int64_t scanning_rate{0};
    int buffer_length{2};

    using phy_iio_buffer = struct iio_buffer;
    phy_iio_buffer  *rx_buffer;
    iChannel::channelScope scanning_mask{0};

    template <bool debug=true>
    void triggerCallback();

    friend Module<cIIOdevice>;

};
