﻿#include "cZynqSnapshot.h"
#include <numeric>
#include <algorithm>
#include <cmath>
#include <iostream>

cZynqSnapshot::cZynqSnapshot(const std::string name_, size_t iterations) : name ( name_ )
{
    latency.resize( iterations );
    k=0;
    init_perfcounters(1, 0);
    overhead = cyclesNow();
    overhead = cyclesNow() - overhead;
}

#include <iomanip>
cZynqSnapshot::~cZynqSnapshot()
{
    auto first = latency.begin() +1;
    unsigned int max = *std::max_element(first,latency.end());
    unsigned int min = *std::min_element(first,latency.end());
    double mean =  std::accumulate(first, latency.end(), 0.0) / ( latency.size() -1 );
    std::transform(first,latency.end(),first,[&](unsigned y){ return y - mean; });
    double sq_sum = std::inner_product( first, latency.end(), first, 0.0);
    unsigned stdev  = static_cast<unsigned>( round( std::sqrt( sq_sum / latency.size() ) ));
    size_t r_mean = static_cast<unsigned>( round(mean) );
    std::cout << std::setw(25) << name  << " #\n latency values : mean(cycles,nsec)/max/min/std/cloud\n"
              << std::setw(5) << r_mean << "," << ( r_mean << 1 ) << " "
              << max   << " "
              << min   << " "
              << stdev << " "
              << latency.size() <<"\n\n";
}



void cZynqSnapshot::init_perfcounters( int32_t do_reset, int32_t enable_divider ) const
{
    /* In general enable all counters (including cycle counter) */
    int32_t value = 1;

    /* Peform reset */
    if (do_reset) {
        value |= 2; /* reset all counters to zero */
        value |= 4; /* reset cycle counter to zero */
    }
    if (enable_divider)
        value |= 8; /* enable "by 64" divider for CCNT */
    value |= 16;
    /* Program the performance-counter control-register */
    asm volatile ("mcr p15, 0, %0, c9, c12, 0\t\n" :: "r"(value));
    /* Enable all counters */
    asm volatile ("mcr p15, 0, %0, c9, c12, 1\t\n" :: "r"(0x8000000f));
    /* Clear overflows */
    asm volatile ("mcr p15, 0, %0, c9, c12, 3\t\n" :: "r"(0x8000000f));
}
