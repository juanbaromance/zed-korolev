#include "interpolators.h"
#include "spline.hpp"

/* 200/3591.76, ZynQ 500/666.66 usec/BogoMips*/
#include <iostream>
#include <fstream>
#include <rtimers/cxx11.hpp>
#include "rtimers/core.hpp"

#include "nurbs.h"
class cNurbs
{
public:
    cNurbs();

    bool testing();

private:
    const std::string name{"cNurbs"};
    struct {
        NurbsDTO dto;
        int (*generator)( NurbsDTO *iface );
    } nurbs;

        std::array<std::tuple<float,float>,15> knots{
            {
                { 0,0 },
                { 4,0 },
                { 8,1 },
                { 10, 22 },
                { 13 ,19 },
                { 26 ,17 },
                { 28 ,22 },
                { 28 ,88 },
                { 32 ,142 },
                { 36 ,185 },
                { 40 ,210 },
                { 44 ,222 },
                { 48 ,227 },
                { 52 ,229 },
                { 56 ,230},
                }
        };
};


cNurbs::cNurbs()
{
    nurbs.generator = nurbs_generator;
    size_t i= 0;
    for ( const auto & k : knots )
    {
        nurbs.dto.data.x[i] = std::get<0>(k);
        nurbs.dto.data.y[i] = std::get<1>(k);
        i++;
    }
    nurbs.dto.order = 7;
    nurbs.dto.noKnots = knots.size();
    nurbs.dto.soInterpolation = 100;
}

bool cNurbs::testing()
{
    using namespace std;
    using namespace rtimers;
    {
        cxx11::DefaultTimer tmr( name );
        tmr.start();
        nurbs.generator( & nurbs.dto );
        tmr.stop();
    }

    ofstream ofile;
    ofile.open( "./output." + name + ".txt" );
    for( const auto & k : knots )
    {
        ofile << "# " << std::get<0>(k) << " " << std::get<1>(k) << "\n";
    }
    for( int i = 0; i < nurbs.dto.soInterpolation; i++ )
        ofile << nurbs.dto.data.x[i] << " " << nurbs.dto.data.y[i] << "\n";
    ofile.close();
    return false;
}

#include "neon-testing/cZynqSnapshot.h"
#include <algorithm>
class cSpline {
public:
    cSpline(){}
    bool testing()
    {
        using namespace std;
        ofstream ofile;
        ofile.open( "./output.Spline.txt" );
        array<double,100> spline;
        double min = *std::min_element(x.begin(),x.end());
        double max = *std::max_element(x.begin(),x.end());
        double resolver = abs(max - min)/spline.size();

        {
            cZynqSnapshot snapshot("cSpliner.c++.regression", 1 << 5 );
            for( size_t i = 0; i < snapshot.size(); i++ )
            {
                snapshot.start();
                spline_generator.set_points(x,y);
                snapshot.stop();
            }
        }

        cZynqSnapshot snapshot("cSpliner.c++.interpolators", spline.size() );
        for( size_t i = 0; i < spline.size(); i++ )
        {
            snapshot.start();
            spline[i] = spline_generator(resolver*i);
            snapshot.stop();
            ofile << resolver*i << " " << spline[i] << "\n";
        }
        ofile.close();
        return true;
    }
private:
    tk::spline spline_generator;
    const std::vector<double> x{0, 4, 8, 12, 16, 20, 24, 28,  32,  36,  40,  44,  48,  52,  56};
    const std::vector<double> y{0, 0, 1,  3,  8, 20, 45, 88, 142, 185, 210, 222, 227, 229, 230};
};

cInterpolators::cInterpolators(){}

bool cInterpolators::testing()
{
    cNurbs().testing();
    cSpline().testing();
    return true;
}
