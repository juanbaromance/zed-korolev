#ifndef libfmt_TAG_inc
#define libfmt_TAG_inc
#define libfmt_COMPILED_BY          ""
#define libfmt_COMPILER_HOST        "popov"
#define libfmt_COMPILER_HOST_TYPE   "Linux 5.4.0-84-generic"
#define libfmt_COMPILED_DATE        "Sun Sep 19 22:53:30 CEST 2021"
#define libfmt_COMPILER             "gcc-x86_64-linux-gnu-9"
#define libfmt_URL_ADDRESS          ""
#define libfmt_PACKAGE_MAJOR        ""
#define libfmt_PACKAGE_MINOR        ""
#define libfmt_PACKAGE_REVISION     ""
#define libfmt_PACKAGE_SUBVERSION   ""
#define libfmt_PACKAGE_NAME         "libfmt"
#define libfmt_PACKAGE_TAG          "v" libfmt_PACKAGE_MAJOR "r" libfmt_PACKAGE_MINOR "p" libfmt_PACKAGE_REVISION "." libfmt_PACKAGE_SUBVERSION 
#endif
