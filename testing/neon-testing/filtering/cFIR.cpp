#include "neon-10/NE10_dsp.h"
#include "neon-testing/cZynqSnapshot.h"
#include "filtering/cFIR.h"

#include <arm_neon.h>
#include <array>
#include <iostream>

template <size_t no_taps=4, size_t block_size=8, size_t no_blocks=2>
class  cNeoFir {
private:
    static constexpr size_t buffer_size = no_blocks * block_size;

public:
    using taps_t = std::array<ne10_float32_t,no_taps>;
    using buffer_t = std::array<ne10_float32_t,buffer_size>;
    enum { debug = true, silent = false };

    cNeoFir( const taps_t & probe_taps = {} ){ loadTaps( probe_taps );}
    bool loadTaps( const taps_t & probe_taps )
    {
        if ( ne10_fir_init_float( & cfg, no_taps, ( taps = probe_taps ).data(), status.data(), block_size) != NE10_OK)
            error = true;
        return error;
    }

    buffer_t step( buffer_t src )
    {
        buffer_t o;
        cZynqSnapshot snapshot("cFIR-neon" + std::to_string(block_size),no_blocks);
        for ( size_t b = 0; b < no_blocks; ++b )
        {
            snapshot.start();
            ne10_fir_float( & cfg, src.data() + ( b * block_size ), o.data() + ( b * block_size ), block_size );
            snapshot.stop();
        }
        return o;
    }

    template< bool debug=false >
    float32_t step( const float32_t & sample )
    {
        float32x4_t W{ sample, w[1], w[2], w[3] };
        const float32x4_t weights = float32x4_t{ taps[3], taps[2], taps[1], taps[0] };
        float32x4_t q = vmulq_f32( W, weights);
        float32x2_t d = vadd_f32(vget_low_f32(q),vget_high_f32(q));
        if constexpr ( debug )
        {
            using namespace std;
            cout << weights[0] << "/" << weights[1] <<"/" << weights[2] << "/" << weights[3] << "\n";
            cout << sample << "/" << w[1] <<"/" << w[2] << "/" << w[3] << "\n";
            cout << q[0] << "/" << q[1] <<"/" << q[2] << "/" << q[3] << "\n";
        }

        w[3] = w[2];
        w[2] = w[1];
        w[1] = sample;
        return vget_lane_f32( vpadd_f32(d,d), 0 );
    }

private:
    bool error{false};
    ne10_fir_instance_f32_t cfg;
    taps_t taps;
    std::array<ne10_float32_t, no_taps + block_size -1> status;
    std::array<float32_t,4> w;
};

cFIR::cFIR(){}

bool cFIR::testing()
{
    cNeoFir<>::taps_t taps;
    for ( size_t i = 0; i < taps.size(); ++i )
        taps[i] = (ne10_float32_t)rand() / RAND_MAX * 5.0f;

    cNeoFir neo_fir( taps );
    cNeoFir<>::buffer_t i_signal;
    for ( size_t i = 0; i < i_signal.size(); ++i )
        i_signal[ i ] = (ne10_float32_t)rand() / RAND_MAX * 20.0f;

    cNeoFir<>::buffer_t o_signal = neo_fir.step( i_signal );
    auto dump = [&](){
        for (size_t i = 0; i < o_signal.size(); i++ )
            printf( "%02d %9.4f %9.4f\n", i, i_signal[i], o_signal[i] );
    };
    dump();

    {
        cZynqSnapshot snapshot("cFIR.intrinsics", o_signal.size() );
        for( size_t i = 0; i < o_signal.size(); ++i )
        {
            snapshot.start();
            o_signal[i] = neo_fir.step<cNeoFir<>::silent>( i_signal[i] );
            snapshot.stop();
        }
    }
    dump();

    return 0;
}
