#pragma once

#include <string>
#include "util.h"

template <typename T=void>
class iFilter : public crtp<T,iFilter>
{
public:
    float step( float measure ){ return this->underly().step(measure); }
    int   reset( float input = 0, bool deep_reset = false ){ return this->underly().reset(input,deep_reset); }
    float state( ){return this->underly().state(); }
    void  setSampling ( int msec ){ this->underly().samplingImpl(msec); }
    std::string report(){ return this->underly().reportImpl(); };
    void  tune( size_t _Fs, size_t _Fc, double Q = 0.7071 ){ this->underly().tune(_Fs,_Fc,Q); }

    using  spec_t = struct { float *poles; float *zeros; };
    spec_t specs(){ return this->underly().specs(); }
    bool testing(){ return this->underly().testing(); }
    std::string name(){ return this->underly().name(); }

private:
    iFilter(){}
    friend T;

};
