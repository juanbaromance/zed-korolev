#define VERBOSE
#define MODULE	"geometry"
#define IN_RANGE(x,y,z)        ( ( ((y-z)<=x) && ((y+z)>=x) ) ?1 : 0 )
#define IS_BETWEEN(x,min,max)  ( ( (min<=x) && (max>=x) ) ?1 : 0 )
/* Headers --------------------------------------------------------- */
#include <math.h>
#include <string.h>
#include <stdio.h>

#include "geometry.h"
#include <iostream>
using namespace std;

#ifndef pi 
#define pi M_PI
#endif

/*---------------------------------------------------------- Headers */



//****************************************************************************80
//
//  Purpose:
//
//    SQRT_CORDIC returns the square root of a value using the CORDIC method.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    21 June 2007
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, double X, the number whose square root is desired.
//
//    Input, int N, the number of iterations to take.
//    This is essentially the number of binary digits of accuracy, and
//    might go as high as 53.
//
//    Output, double SQRT_CORDIC, the approximate square root of X.
//
double sqrt_cordic ( double x, int n = 5 )
{
  int i;
  double poweroftwo;
  double y = 0.0;

  if ( x < 0.0 )
  {
    return( -1 );
  }

  if ( x == 0.0 )
  {
    y = 0.0;
    return y;
  }

  if ( x == 1.0 )
  {
    y = 1.0;
    return y;
  }

  poweroftwo = 1.0;

  if ( x < 1.0 )
  {
    while ( x <= poweroftwo * poweroftwo )
    {
      poweroftwo = poweroftwo / 2.0;
    }
    y = poweroftwo;
  }
  else if ( 1.0 < x )
  {
    while ( poweroftwo * poweroftwo <= x )
    {
      poweroftwo = 2.0 * poweroftwo;
    }
    y = poweroftwo / 2.0;
  }

  for ( i = 1; i <= n; i++ )
  {
    poweroftwo = poweroftwo / 2.0;
    if ( ( y + poweroftwo ) * ( y + poweroftwo ) <= x )
    {
      y = y + poweroftwo;
    }
  }

  cout << y << " versus " << sqrt(x) << endl;

  return y;
}
//****************************************************************************80


/*!
  @brief multiply two 4x4 float matrixes
  @param[in]  m1
  @param[in]  m2
  @param[out] o_mul
*/
void mat_mul44( void *o_mul, const void *m1, const void *m2){
    int u, v, k;
    float aux_m1[ 4 ][ 4 ];
    float aux_m2[ 4 ][ 4 ];
    float aux_p [ 4 ][ 4 ];

    memcpy( aux_m1, m1, 16 * sizeof( float ) );
    memcpy( aux_m2, m2, 16 * sizeof( float ) );
    for( u = 0; u < 4; u++ )  //multiply p = m1 * m2
    {
        for( v = 0; v < 4; v++ )
        {
            aux_p[ u ][ v ] = 0;
            for( k = 0; k < 4; k++ )
            {
                aux_p[ u ][ v ] += aux_m1[ u ][ k ] * aux_m2[ k ][ v ];
            }
        }
    }
    memcpy( o_mul, aux_p, 16 * sizeof( float ) );

}

/*!
  @brief Multiply a 4x4 float matrix against a 4x1 float matrix (i.e used to apply reference frames on 3D points )
  @param[in]  m1 4x4 rotation matrix
  @param[in]  m2 4x1 point
  @param[out] o_mul
    */
void mat_mul41( void *o_mul, const void *m1, const void *m2 ){
    int i,j;
    float aux_m1[ 4 ][ 4 ];
    float aux_m2[ 4 ];
    float  aux_p[ 4 ];

    memcpy( aux_m1, m1, 16 * sizeof( float ) );
    memcpy( aux_m2, m2,  4 * sizeof( float ) );
    for( i = 0; i < 4; i++ ) //multiply p = m1 * m2
    {
        aux_p[ i ] = 0;
        for( j = 0; j < 4; j++ )
        {
            aux_p[ i ] += aux_m1[ i ][ j ] * aux_m2[ j ];
        }
    }
    memcpy( o_mul, aux_p, 4 * sizeof(float ) );

}



/*! @brief calculate distance between 2 points */
float dist_pt_pt(float *a, float *b){
    return( sqrt(
				( a[ 0 ] - b[ 0 ] ) * ( a[ 0 ] - b[ 0 ] ) +
				( a[ 1 ] - b[ 1 ] ) * ( a[ 1 ] - b[ 1 ] ) +
				( a[ 2 ] - b[ 2 ] ) * ( a[ 2 ] - b[ 2 ] )
				)
		);
}


static float minimum(float a,float b,float c,float d)
{
    return min(min(a,b),min(c,d));
}

static float maximum(float a,float b,float c,float d)
{
    return max(max(a,b),max(c,d));
}


int para_line_intersect_strict ( float *point, float *p1, float *p2, float *p3, float *p4, float *v1, float *v2 )
{
	int retval=NO_INTERSECTION;

//	LOG_TRACE("[%s:%s] Checking intersection between plane:"
//				"\n\tp1[%0.2f,%0.2f,%0.2f]\n\tp2[%0.2f,%0.2f,%0.2f]\n\tp3[%0.2f,%0.2f,%0.2f]\n\tp4[%0.2f,%0.2f,%0.2f] AND vector:"
//				" \n\tfrom[%0.2f,%0.2f,%0.2f] to [%0.2f,%0.2f,%0.2f]",MODULE,__func__,p1[0],p1[1],p1[2],p2[0],p2[1],p2[2],p3[0],p3[1],p3[2],p4[0],p4[1],p4[2],v1[0],v1[1],v1[2],v2[0],v2[1],v2[2]);
	retval =para_line_intersect(point,p1,p2,p3,v1,v2);
	if ( retval == INTERSECTION)
	{
		/* Check if the point is INSIDE the specified surface and IN the segment specified by the vector */
		if (
				(IS_BETWEEN(point[0],	minimum(p1[0],p2[0],p3[0],p4[0]), maximum(p1[0],p2[0],p3[0],p4[0])) ) &&
				(IS_BETWEEN(point[1],	minimum(p1[1],p2[1],p3[1],p4[1]), maximum(p1[1],p2[1],p3[1],p4[1]))) &&
				(IS_BETWEEN(point[2],	minimum(p2[2],p2[2],p3[2],p4[2]), maximum(p2[2],p2[2],p3[2],p4[2]))) &&
                (IS_BETWEEN(point[0], min(v1[0],v2[0]), max(v1[0],v2[0]))) &&
                (IS_BETWEEN(point[1], min(v1[1],v2[1]), max(v1[1],v2[1]))) &&
                (IS_BETWEEN(point[2], min(v1[2],v2[2]), max(v1[2],v2[2])))
		)
		{
			return INTERSECTION;
		}
	}
	memset(point,0,3);
	return NO_INTERSECTION;
}


/* Further info see https://en.wikipedia.org/wiki/Fast_inverse_square_root */
float invSqrt(float x)
{
  float xhalf = 0.5f * x;
  union
  {
    float x;
    int i;
  } u;
  u.x = x;
  u.i = 0x5f3759df - (u.i >> 1);
  /* The next line can be repeated any number of times to increase accuracy */
  u.x = u.x * (1.5f - xhalf * u.x * u.x);
  return u.x;
}


int para_line_intersect ( float *point, float *p1, float *p2, float *p3, float *v1, float *v2 ){

    float denom;
    float norm1;
    float norm2;
    float t;
    float a,b,c,d,f,g,h,x0,y0,z0;
	const float TOL = 0.00001E+00;


	//convert explicit plane to implicit form ax+by+cz+d=0
	a = ( p2[1] - p1[1] ) * ( p3[2] - p1[2] ) - ( p2[2] - p1[2] ) * ( p3[1] - p1[1] );
	b = ( p2[2] - p1[2] ) * ( p3[0] - p1[0] ) - ( p2[0] - p1[0] ) * ( p3[2] - p1[2] );
	c = ( p2[0] - p1[0] ) * ( p3[1] - p1[1] ) - ( p2[1] - p1[1] ) * ( p3[0] - p1[0] );
	d = - p2[0] * a - p2[1] * b - p2[2] * c;


	//check norm of plane vector
    norm1 = sqrt( a * a + b * b + c * c );
	if( norm1 == 0.0E+00 )
	{
		//WARN_TRACE("ilegal plane definition: director vector (norm GROUNDED):SKIPPED/1");
		return( 0 );
	}

	//convert explicit vector to parametric form
	x0 = v1[ 0 ];
	y0 = v1[ 1 ];
	z0 = v1[ 2 ];
	if ( v1[ 0 ] == v2[ 0 ] && v1[ 1 ] == v2[ 1 ] && v1[ 2 ] == v2[ 2 ] )
	{
		f = 0.0E+00;
		g = 0.0E+00;
		h = 0.0E+00;
	}
	else
	{
        norm2 = sqrt(
			( v2[ 0 ] - v1[ 0 ] ) * ( v2[ 0 ] - v1[ 0 ] ) +
			( v2[ 1 ] - v1[ 1 ] ) * ( v2[ 1 ] - v1[ 1 ] ) +
			( v2[ 2 ] - v1[ 2 ] ) * ( v2[ 2 ] - v1[ 2 ] )
			);
		f = ( v2[ 0 ] - v1[ 0 ] ) / norm2;
		g = ( v2[ 1 ] - v1[ 1 ] ) / norm2;
		h = ( v2[ 2 ] - v1[ 2 ] ) / norm2;
	}

	//check norm of intersection vector
    norm2 = sqrt( f * f + g * g + h * h );
	if ( norm2 == 0.0E+00 )
	{
		//WARN_TRACE("ilegal plane definition: director vector (norm GROUNDED): SKIPPED/2");
		return( 0 );
	}

	denom = a * f + b * g + c * h;
	//check if plane and vector are parallel
	if ( fabs ( denom ) < TOL * norm1 * norm2 )
	{
		if ( a * x0 + b * y0 + c * z0 + d == 0.0E+00 )
		{
			//vector is contained within the plane
			point[ 0 ] = x0;
			point[ 1 ] = y0;
			point[ 2 ] = z0;
			//WARN_TRACE("%s: Vector is contained within the plane", __FUNCTION__ );
			return( 1 );
		}
		else
		{
			point[ 0 ] = 0.0E+00;
			point[ 1 ] = 0.0E+00;
			point[ 2 ] = 0.0E+00;
			return( 0 );
		}
	}
	//if they are not parallel they must intersect
	else
	{
		t = - ( a * x0 + b * y0 + c * z0 + d ) / denom;
		point[ 0 ] = x0 + t * f;
		point[ 1 ] = y0 + t * g;
		point[ 2 ] = z0 + t * h;

		//check if vector is pointing towards or against plane
		if( dist_pt_pt( v1, point ) > dist_pt_pt( v2, point ) )
		{
			return( 1 );
		}
		else
		{
#ifdef TOWARDs_LOG
			WARN_TRACE("para-line-intersect. Pointing towards \"%.2f/%.2f\"",
					   dist_pt_pt( v1, point ), dist_pt_pt( v2, point ) );
#endif
			return( 1 );
		}
	}

}


float cross_prod( float *p0, float *p1, float *p2, float *p3){
	float mod1, mod2, mod3;
	float angle;

	p3[ 0 ] =
		( p1[ 1 ] - p0[ 1 ] ) * ( p2[ 2 ] - p0[ 2 ] ) -
		( p1[ 2 ] - p0[ 2 ] ) * ( p2[ 1 ] - p0[ 1 ] );

	p3[ 1 ] =
		( p1[ 2 ] - p0[ 2 ] ) * ( p2[ 0 ] - p0[ 0 ] ) -
		( p1[ 0 ] - p0[ 0 ] ) * ( p2[ 2 ] - p0[ 2 ] );

	p3[ 2 ] =
		( p1[ 0 ] - p0[ 0 ] ) * ( p2[ 1 ] - p0[ 1 ] ) -
		( p1[ 1 ] - p0[ 1 ] ) * ( p2[ 0 ] - p0[ 0 ] );

	mod1 = vmod_3d(  ( p1[ 0 ] - p0[ 0 ] ), ( p1[ 1 ] - p0[ 1 ] ), ( p1[ 2 ] - p0[ 2 ] ) );
	mod2 = vmod_3d(  ( p2[ 0 ] - p0[ 0 ] ), ( p2[ 1 ] - p0[ 1 ] ), ( p2[ 2 ] - p0[ 2 ] ) );
	mod3 = vmod_3d(  ( p3[ 0 ] - p0[ 0 ] ), ( p3[ 1 ] - p0[ 1 ] ), ( p3[ 2 ] - p0[ 2 ] ) );

	angle = mod3 / ( mod1 * mod2 );

	angle = pi/2;
	if( ( angle >= -1 ) && ( angle <= 1 ) )
		angle = asin( angle );

	return( angle );

}


/* Provides the inverse matrix */
void inv_homo_mat( void *inverted_m, void *m ){
	int i, j;
	float mi[ 4 ][ 4 ];
	float mm[ 4 ][ 4 ];

	memcpy( mm, m, 16 * sizeof( float ) );
	for( i = 0; i < 3; i++ ){ //transpose 3x3
		for( j = 0; j < 3; j++ ){
			mi[ i ][ j ] = mm[ j ][ i ];
		}
	}
	mi[ 3 ][ 0 ] = 0;
	mi[ 3 ][ 1 ] = 0;
	mi[ 3 ][ 2 ] = 0;
	mi[ 3 ][ 3 ] = 1;
	mi[ 0 ][ 3 ] = 0;
	mi[ 1 ][ 3 ] = 0;
	mi[ 2 ][ 3 ] = 0;
	for( i = 0; i < 3; i++ ){
		for( j = 0; j < 3; j++ ){
			mi[ i ][ 3 ] -= mm[ j ][ 3 ] * mm[ j ][ i ];
		}
	}
	memcpy( inverted_m, mi, 16 * sizeof( float ) );

}


void eye_mat( void *m ){
	const float eye[ 4 ][ 4 ] = {
		{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}
	};

	memcpy( m, eye, 16 * sizeof( float ) );

}

void zero_pt(void *p){
	float zero[4] = { 0,0,0,1 };

	memcpy( p, zero, 4 * sizeof( float ) );

}

/* Computes angle (counterclockwise) in radians between 2 vectors.
   Result belongs to [0, 2*pi) as 4 cuadrants are checked for solution.
   Input data are vector components of vectors: a=(a1,a2) b=(b1,b2)
   vector1=(a->b), vector2=(c->d), a=(a1,a2), ...  */
float angle_x_2d( float a1,float a2,float b1,float b2 ){
	float angle;

	//acos of the dot product
	angle = acosf( (a1*b1 + a2*b2) / ( vmod_2d(a1,a2) * vmod_2d(b1,b2) ) );

	//sign of cross product
	// Angles screen tolerance
	if(angle > (0.5*pi/180)){
		if( ((a1*b2 - b1*a2) < 0) )
			angle = 2*pi - angle;
	}
	else{
		angle = 0.00;
		//MSGLOG(("angle corrected: %.2f", angle));
	}
	return( angle );
}


float vmod_2d( float x, float y ){

	return( sqrt( x * x + y * y ) );

}


float vmod_3d( float x, float y, float z ){

	return( sqrt( x * x + y * y + z * z) );

}


float angle_lines_2d( float a1, float a2, float b1, float b2,
					  float c1, float c2, float d1, float d2 ){

	return( angle_x_2d( b1 - a1, b2 - a2, d1 - c1, d2 - c2 ) );

}


float angle_lines_3d( float *P1, float *P2, float *P3, float *P4 ){

	float norm_q, norm_p;
	float p_scalar_q;
	float angle;
	float theta;

	//get euclinean norm por each vector (distance)
	norm_p = dist_pt_pt( P1, P2 );
	norm_q = dist_pt_pt( P3, P4 );

	p_scalar_q = ( P2[0] - P1[0] ) * ( P4[0] - P3[0] ) +
		( P2[1] - P1[1] ) * ( P4[1] - P3[1] ) +
		( P2[2] - P1[2] ) * ( P4[2] - P3[2] );

	//check input data
	if ( norm_p == 0.0E+00 || norm_q == 0.0E+00 )
    {
		//One of the lines is degenerated
		angle = -1;
    }
	else
    {
		theta = p_scalar_q / ( norm_p * norm_q );

		if( theta <= -1 )
			angle = pi;
		else if( theta >= 1 )
			angle = 0;
		else
			angle = acos ( theta );
    }


	return angle;
}



void print_4x4(const void *matrix){
	int i,j;
	float m[ 4 ][ 4 ];

	memcpy( m, matrix, 16*sizeof( float ) );
	printf("Matriz M:\n");
	for( i = 0; i < 4; i++ )
	{
		for( j = 0; j < 4; j++ )
			printf("%.2f \t", m[ i ][ j ] );
		printf("\n");
	}
}


void print_point(const void *point, void *forward, char *device ){
	float p[4];

	memcpy(p,point,4*sizeof(float));
	printf("%-10s point (%s): X=%.2f  Y=%.2f  Z=%.2f  R=%.2f\n",
		   device,
		   forward ? "F" : "R",
		   p[0],p[1],p[2],p[3]);
	return;

}



void dump_matrix_4x4( const char *caller, float data[][4] )
{
	int i;

	for( i=0; i<4; i++ )
	{
		printf( "\t%8.2f\t%8.2f\t%8.2f\t%8.2f\n",
				data[i][0], data[i][1], data[i][2], data[i][3] );

	}
}


void dump_matrix_4x1( const char *caller, float data[4] )
{
	int i;

	for( i=0; i<4; i++ )
	{
		printf( "\t%8.2f\n", data[i] );

	}
}
