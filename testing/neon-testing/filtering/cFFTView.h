#pragma once

#include <vector>
#include <array>
#include <string>
#include "neon-10/NE10.h"
#include "neon-testing/main.h"
#include <iostream>

template <size_t length=256>
class cFFTView {

public:
    cFFTView() { cfg = ne10_fft_alloc_r2c_float32( length << 1 ); }
    ~cFFTView() { ne10_fft_destroy_r2c_float32(cfg); }

    using dct_t    = std::array<float, length>;
    using buffer_t = std::array<float, length << 1>;
    dct_t valueOfPSD() { return psd; }

    bool testing() { return fft_testing(*this); }
    std::string report() { return "cFFTView"; }

    template<StrategyEnum strategy=Neo10>
    void feed( buffer_t & samples )
    {
        if constexpr ( strategy == Compiler )
            ne10_fft_r2c_1d_float32_c( fft_val.data(), samples.data(), cfg );
        else
            ne10_fft_r2c_1d_float32( fft_val.data(), samples.data(), cfg );
        ne10_len_vec2f( psd.data(),reinterpret_cast<ne10_vec2f_t*>(fft_val.data()),psd.size());
    }

private:
    ne10_fft_r2c_cfg_float32_t cfg;
    std::array<ne10_fft_cpx_float32_t,length> fft_val;
    dct_t psd;
};


bool fft_testing( cFFTView<> &);

