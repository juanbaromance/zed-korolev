#pragma once
#include <string>
#include <vector>

class cZynqSnapshot{
public:
    cZynqSnapshot( const std::string name_ = "unknown", size_t iterations = 1024 );
    inline void start () { cycles = cyclesNow(); }
    inline void stop()   { latency[k++] = cyclesNow() - ( cycles + overhead ); }
    inline unsigned size( ) { return latency.size(); }
    ~cZynqSnapshot();

private:
    /* Read CCNT Register */
    inline unsigned int cyclesNow (void) const
    {
        unsigned int value;
        asm volatile ("mrc p15, 0, %0, c9, c13, 0\t\n": "=r"(value));
        return value;
    }
    void init_perfcounters( int32_t do_reset, int32_t enable_divider ) const;
    std::vector<unsigned> latency;
    unsigned cycles, k, overhead;
    std::string name;

};
