diff --git a/GNUlinux_config.cmake b/GNUlinux_config.cmake
index 30fab1d..acd5d82 100644
--- a/GNUlinux_config.cmake
+++ b/GNUlinux_config.cmake
@@ -35,9 +35,22 @@
 #     Target architecture can be specified by setting NE10_LINUX_TARGET_ARCH to
 #     armv7 or aarch64 (Not done yet). Defaut is armv7.
 
-set(GNULINUX_PLATFORM ON)
-set(CMAKE_SYSTEM_NAME "Linux")
-set(CMAKE_SYSTEM_PROCESSOR "arm")
+set ( GNULINUX_PLATFORM ON)
+set ( CMAKE_SYSTEM_NAME "Linux")
+set ( CMAKE_SYSTEM_PROCESSOR "arm")
+set ( MACHINE                "zynq7"          CACHE STRING "")
+set ( CROSS_PREFIX           "arm-xilinx-linux-gnueabi-" CACHE STRING "")
+set ( CMAKE_SYSTEM_NAME "Linux" )
+set ( PETALINUX "/home/juanba/zynq-development/petalinux-sdk/2019.2" )
+set ( SDK_SYSROOT  "${PETALINUX}/sysroots/cortexa9t2hf-neon-xilinx-linux-gnueabi/" )
+
+set ( CMAKE_SYSROOT ${SDK_SYS_ROOT} )
+set ( CMAKE_CXX_FLAGS "-march=armv7-a -mthumb -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 --sysroot=${SDK_SYSROOT}" )
+set ( CMAKE_C_FLAGS "-march=armv7-a -mthumb -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 --sysroot=${SDK_SYSROOT}" )
+set ( CMAKE_C_COMPILER   ${CROSS_PREFIX}gcc )
+set ( CMAKE_CXX_COMPILER ${CROSS_PREFIX}g++ )
+set ( CMAKE_C_COMPILER_WORKS 1 )
+set ( CMAKE_CXX_COMPILER_WORKS 1 )
 
 if(NOT DEFINED ENV{NE10_LINUX_TARGET_ARCH})
    set(NE10_LINUX_TARGET_ARCH "armv7")
@@ -45,16 +58,12 @@ else()
    set(NE10_LINUX_TARGET_ARCH $ENV{NE10_LINUX_TARGET_ARCH})
 endif()
 
+set(TOOLCHAIN_PREFIX arm-xilinx-linux)
+
 if(NE10_LINUX_TARGET_ARCH STREQUAL "armv7")
-   set(CMAKE_C_COMPILER arm-linux-gnueabihf-gcc)
-   set(CMAKE_CXX_COMPILER arm-linux-gnueabihf-g++)
-   set(CMAKE_ASM_COMPILER arm-linux-gnueabihf-as)
-   find_program(CMAKE_AR NAMES "arm-linux-gnueabihf-ar")
-   find_program(CMAKE_RANLIB NAMES "arm-linux-gnueabihf-ranlib")
+   find_program(CMAKE_AR NAMES "arm-xilinx-linux-gnueabi-ar")
+   find_program(CMAKE_RANLIB NAMES "arm-xiinx-linux-gnueabi-ranlib")
 elseif(NE10_LINUX_TARGET_ARCH STREQUAL "aarch64")
-   set(CMAKE_C_COMPILER aarch64-linux-gnu-gcc)
-   set(CMAKE_CXX_COMPILER aarch64-linux-gnu-g++)
-   set(CMAKE_ASM_COMPILER aarch64-linux-gnu-as)
    find_program(CMAKE_AR NAMES "aarch64-linux-gnu-ar")
    find_program(CMAKE_RANLIB NAMES "aarch64-linux-gnu-ranlib")
 endif()
