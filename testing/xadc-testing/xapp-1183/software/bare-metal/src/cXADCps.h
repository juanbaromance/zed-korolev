#pragma once
#include "bsp/xadcps.h"
#include "bsp/xscugic.h"

#include "bsp/FreeRTOS.h"
#include "bsp/event_groups.h"
#include "bsp/task.h"

enum {
    XADC_Alarm0 = 0,
    XADC_Alarm5 = 5,
    EndOfACQ = 15,
};

struct xadc_irq_wrapper {
    u16 action, mask;
    EventGroupHandle_t events;
    XAdcPs *XAdcPtr;
};

class cXadcPS {
    enum {
        Periodic = pdTRUE,
        AnyTriggered = pdFALSE,
        AllTriggered = pdTRUE
    };

public:
    enum device_id { xadc0 = XPAR_XADCPS_0_DEVICE_ID };
    cXadcPS( device_id dev_id = xadc0  );
    cXadcPS & autotest();
    bool run();

private:
    int prepareInterrupts( u16 interrupt_id = XPAR_XADCPS_INT_ID );

private:
    static void trampolineOnBottomHalf( void * argument );
    XAdcPs phy_, *phy{ & phy_ };
    XScuGic gic_ , *gic { & gic_ };
    xadc_irq_wrapper irq_wrapper;
    bool enabled{false};

    TickType_t previous{0}, elapsed{0};
    TaskHandle_t xHandle{nullptr};
    EventBits_t operation_mask {
        static_cast<EventBits_t>( 1 << XADC_Alarm0 |
                                  1 << XADC_Alarm5 |
                                  1 << EndOfACQ)
    };

    void bottomHalf();
};
