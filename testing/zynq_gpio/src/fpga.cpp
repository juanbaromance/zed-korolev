#define MODULE       "fpga_lib"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pthread.h>

#include "uio/libuio.h"
#include "fpga_lib/fpga_lib.h"
#include "fpga_zynq/axi_registers.h"

#define ENABLE_INTERRUPTS_BIT       0

enum class ZynqMinors
{
   UIO_AXI_GENERAL_IO=0,
   UIO_AXI_VERSIONS,
   UIO_AXI_IR,
   UIO_AXI_WATCHDOGS,
   UIO_AXI_ONE_WIRE,
   MAX_AXI_UIO_DEVICES
};


#include "fmt/format.h"
#include "fmt/color.h"
#include "fmt/core.h"
static auto mark1   = [](auto token){ return fmt::format( fmt::emphasis::bold | fg(fmt::color::green), token ); };
static auto warning = [](auto token){ return fmt::format( fmt::emphasis::bold | fg(fmt::color::orange), token ); };
#include <iostream>

#include "common_cpp/c++17patterns.h"

class cZynQ7FPGA::Private {
public:
    Private() { ( std::clog << fmt::format("{0} cames alive\n", mark1(__PRETTY_FUNCTION__)) ).flush(); }
    ~Private(){ ( std::clog << fmt::format("{0} \n", mark1(__PRETTY_FUNCTION__)) ).flush(); }

private:
    std::map<ZynqMinors,uio_info_t*> devices;
    std::bitset<enumSqueeze(ZynqMinors::MAX_AXI_UIO_DEVICES)> error{0b11111};

    bool boot()
    {
        devices.clear();
        ( std::clog << fmt::format("{0}\n", mark1(__PRETTY_FUNCTION__) ) ).flush();
        using myPair_T = std::tuple<std::string,ZynqMinors>;
        std::array<myPair_T,5> descriptors = {
            std::make_tuple("axi-general-io", ZynqMinors::UIO_AXI_GENERAL_IO),
            {"axi-versions" ,ZynqMinors::UIO_AXI_VERSIONS},
            {"axi-ir"       ,ZynqMinors::UIO_AXI_IR   },
            {"axi-watchdogs",ZynqMinors::UIO_AXI_WATCHDOGS   },
            {"axi-one-wire" ,ZynqMinors::UIO_AXI_ONE_WIRE }
        };

        for( const auto & descriptor : descriptors )
        {
            auto name = const_cast<char*>(std::get<0>(descriptor).c_str());
            if( auto tmp = uio_find_by_uio_name( name ); tmp )
            {
                if( uio_open(tmp) == 0 )
                {
                    auto minor = std::get<1>(descriptor);
                    devices[ minor ] = tmp;
                    error.reset( static_cast<size_t>(std::get<1>(descriptor)) );
                    uio_write( minor, INT_FLAG_REG_OFFSET, 0xFFFFFFFF );
                    uio_write( minor, INT_ENABLE_REG_OFFSET, 0 );
                    {
                        auto tmp = uio_read( minor, VERSION_REG_OFFSET );
                        auto version     = (tmp >> 24) & 0x000000FF;
                        auto revision    = (tmp >> 16) & 0x000000FF;
                        auto subrevision = (tmp >> 8) & 0x000000FF;
                        auto state       =  tmp & 0x000000FF;
                        ( std::cout << fmt::format("{4} V{0}R{1}.{2} states {3}\n", version, revision, subrevision, state, name ) ).flush();
                    }
                }
            }
        }

        if( error.test(enumSqueeze(ZynqMinors::UIO_AXI_VERSIONS)) == 0 )
            std::clog << fmt::format( "ZynqGPIO.signatures {0}", mark1(signature()) );
        return ! error.any();
    }

    void shutdown()
    {
        ( std::clog << __PRETTY_FUNCTION__ << "\n" ).flush();
        for( const auto & device: devices )
        {
            uio_close( device.second );
            free( device.second );
        }
    }

    static constexpr auto gpio_minor{ZynqMinors::UIO_AXI_GENERAL_IO};

    int lines()
    {
        int aux{0};
        for (int i = 0; i < 2; i++)
           aux |= (( uio_read( gpio_minor, GENERAL_IO_GPIO_OUT_B1_OFFSET+ i ) & 0xFF) << (8*i));
        return aux;
    }
    int lines( const lines_t & l )
    {
        int ret = 0;
        int aux = ((lines() & ~l.mask) | (l.state & l.mask));
        for ( int i = 0; i < 2; i++ )
            ret = uio_write( gpio_minor, GENERAL_IO_GPIO_OUT_B1_OFFSET + i, ((aux>>(8*i)) & 0x000000FF));
        return ret;
    }

    int gpi()
    {
        int aux{0};
        for (int i = 0; i < 2; i++)
           aux |= (( uio_read( gpio_minor, GENERAL_IO_DIG_IN_B1_OFFSET+ i ) & 0xFF) << (8*i));
        for (int i = 0; i < 2; i++)
           aux |= ( (uio_read( gpio_minor, GENERAL_IO_GPIO_IN_B1_OFFSET+ i ) & 0xFF ) << ((8*i)+16));
        return aux;
    }

    int motion()
    {
        int aux{0};
        for (int i = 0; i < 2; i++)
           aux |= (( uio_read( gpio_minor, GENERAL_IO_BRAKES_B1_OFFSET+ i ) & 0xFF) << (8*i));
        for (int i = 0; i < 2; i++)
           aux |= ( (uio_read( gpio_minor, GENERAL_IO_CLUTCHES_B1_OFFSET+ i ) & 0xFF ) << ((8*i)+16));
        return aux;
    }
    int motion( const motion_t & m )
    {
        int aux = (( motion() & ~m.mask) | (m.state & m.mask));
        for (int i = 0; i < 2; i++)
           uio_write( gpio_minor, GENERAL_IO_BRAKES_B1_OFFSET + i, ((aux >> (8 * i)) & 0x000000FF));
        for (int i = 0; i < 2; i++)
           uio_write( gpio_minor, GENERAL_IO_CLUTCHES_B1_OFFSET + i, ((aux >> ((8 * i)+16)) & 0x000000FF));
        return 0;
    }

    int gpo()     { return uio_read( gpio_minor, GENERAL_IO_LEDS_OFFSET ); }
    int gpo( int mask, int value )
    {
        int ret = 0;
        int aux = gpo();
        aux = ((aux & ~mask) | (value & mask));
        ret = uio_write(gpio_minor, GENERAL_IO_LEDS_OFFSET, aux);
        return ret;
    }

    int buttons() { return uio_read( gpio_minor, GENERAL_IO_BUTTONS_OFFSET ); }
    ir_t ircomm()
    {
        int aux{0};
        for (int i = 0; i < 4; i++)
            aux += ( uio_read( ZynqMinors::UIO_AXI_IR, IR_C_MSG_FLAGS_B1 + i) << (8 * i));
        ir_t state;
        state.mask = aux;
        state.code = uio_read( ZynqMinors::UIO_AXI_IR, IR_C_K_CODE_IR );
        return state;
    }
    int ircomm( const ir_t & state )
    {
        for ( int i = 0; i < 4; i++ )
            uio_write( ZynqMinors::UIO_AXI_IR, IR_C_MSG_FLAGS_B1 + i, ((state.mask >> (8 * i)) & 0x000000FF));
        return uio_write(ZynqMinors::UIO_AXI_IR, IR_C_K_CODE_IR, state.code);
    }

    const char *signature()
    {
        typedef struct
        {
           int board_id0_l;
           int board_id0_h;
           int board_id1;
           int board_id2;
           int fw_l;
           int fw_h;
           int dna_b1;
           int dna_b2;
           int dna_b3;
           int dna_b4;
           int dna_b5;
           int dna_b6;
           int dna_b7;
           int dna_b8;
        }hw_info_t;

        typedef struct
        {
           int control;
           int int_ctrl;
           int int_flag;
           int version;
        }control_registers_t;

        typedef struct
        {
           control_registers_t control;
           int spare[12];
           hw_info_t info;
        }axi_versions_t;

        static std::string tmp;
        if( tmp.empty() )
            tmp.resize(200);

        auto p_axi_versions = static_cast<axi_versions_t*>(nullptr);
        if( error.test( enumSqueeze(ZynqMinors::UIO_AXI_VERSIONS) ) == false)
            p_axi_versions = static_cast<axi_versions_t*>(uio_get_mem_map( devices[ZynqMinors::UIO_AXI_VERSIONS], 0));
        if( p_axi_versions == nullptr )
        {
            tmp = fmt::format(": uio-minors mask {0}\n", error.to_string() );
            return tmp.c_str();
        }

        int n{0};
        n += sprintf( tmp.data() + n, "\t\tHW    : PBA[A%d-%02d-%c]\n",
                      p_axi_versions->info.board_id0_l + (p_axi_versions->info.board_id0_h << 8),
                      p_axi_versions->info.board_id1,
                      p_axi_versions->info.board_id2);
        n += sprintf( tmp.data() + n, "\t\tFPGA  : DNA[%04x%04x%04x%04x] HDL[V%02dR%02d.%02d]\n",
                      ((p_axi_versions->info.dna_b8 << 8) + p_axi_versions->info.dna_b7),
                      ((p_axi_versions->info.dna_b6 << 8) + p_axi_versions->info.dna_b5),
                      ((p_axi_versions->info.dna_b4 << 8) + p_axi_versions->info.dna_b3),
                      ((p_axi_versions->info.dna_b2 << 8) + p_axi_versions->info.dna_b1),
                      (p_axi_versions->info.fw_h & 0xF000), (p_axi_versions->info.fw_h & 0x0F00), (p_axi_versions->info.fw_l & 0xFF));
        return tmp.c_str();
    }

    int  updateReg( const reg_t & r ){ return uio_write( static_cast<ZynqMinors>(r.minor), r.offset, r.state ); }
    void interrupts( const uint32_t mask ){ uio_write( gpio_minor, GENERAL_IO_INT_ENABLE_REG_OFFSET, mask ); }

    int uio_read( const ZynqMinors minor, int offset ) const
    {
        uint32_t tmp{0};
        if( devices.find(minor) != devices.end() )
            if( uio_read32( devices.find(minor)->second, 0, offset * REGISTER_SIZE_IN_BYTES, & tmp) < 0 )
                std::cerr << fmt::format("{0} on minor ({1}):offset({2})", warning("uio.read.Failured"), offset );
        return tmp;
    }

    int uio_write( const ZynqMinors minor, const int offset, const uint32_t value ) const
    {
        int ret_val;
        if( ( ret_val = ( devices.find(minor) != devices.end() ) ) )
            if( ( ret_val = uio_write32( devices.find(minor)->second, 0, offset * REGISTER_SIZE_IN_BYTES, value ) ) < 0 )
                std::cerr << fmt::format("{0} on minor ({1}):offset({2}):setting({3})", warning("uio.write.Failured"), offset, value );
        return ret_val;
    }

    friend iFPGADevice;

};

cZynQ7FPGA::cZynQ7FPGA() { priv = std::make_unique<Private>(); }
cZynQ7FPGA::~cZynQ7FPGA()
{
    ( std::clog << fmt::format("{0} \n", mark1(__PRETTY_FUNCTION__)) ).flush();
}

template<typename T> bool iFPGADevice<T>::boot(){ return static_cast<T&>(*this).priv->boot(); }
template<typename T> void iFPGADevice<T>::shutdown(){ return static_cast<T&>(*this).priv->shutdown(); }
template<typename T> minor_t iFPGADevice<T>::read(const minor_t & m ) {
    return match( m, {
                      [&]( gpi_t    & ){ return gpi_t   { .port_val = static_cast<T&>(*this).priv->gpi() }; },
                      [&]( gpo_t    & ){ return gpo_t   { .port_val = static_cast<T&>(*this).priv->gpo() }; },
                      [&]( motion_t & ){ return motion_t{ .state    = static_cast<T&>(*this).priv->motion() }; },
                      [&]( lines_t  & ){ return lines_t { .state    = static_cast<T&>(*this).priv->lines() }; },
                      [&]( ir_t     & ){ return static_cast<T&>(*this).priv->ir(); },
                      [&]( auto & ){return minor_t(); }
                  }
                  );
}
template<typename T> int iFPGADevice<T>::write(const minor_t &m){
    return match( m, {
                      [&]( reg_t    & r  ){ return static_cast<T&>(*this).priv->updateReg(r); },
                      [&]( lines_t  & l  ){ return static_cast<T&>(*this).priv->lines(l); },
                      [&]( motion_t & m  ){ return static_cast<T&>(*this).priv->motion(m); },
                      [&]( ir_t     & ir ){ return static_cast<T&>(*this).priv->ir(ir); },
                      [&]( auto & ){ return -1; }
                  }
                  );
}
template<typename T> const char *iFPGADevice<T>::signature(){ return static_cast<T&>(*this).priv->signature(); }
template<typename T> void iFPGADevice<T>::maskInterrupts(const uint32_t & mask){ static_cast<T&>(*this).priv->interrupts(mask); }


static std::unique_ptr<iFPGADevice<cZynQ7FPGA>> driver;

extern "C" {

int ModuleBoot()
{
    driver = std::make_unique<cZynQ7FPGA>();
    if( driver->boot() == false )
        std::cerr << fmt::format( "ZynQFPGA boot failures as : {0}", warning( driver->signature() ) ) ;
    return 0;
}

int ModuleShutDown()
{
    driver->shutdown();
    return 0;
}

}

