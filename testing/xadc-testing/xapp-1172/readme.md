## Directory Layout
```
.
├── build
├── CMakeLists.txt
├── CMakeLists.txt.user
├── dependencies -> ../dependencies
├── readme.md
├── xadc_lib
│   ├── CMakeLists.txt
│   ├── linux
│   │   ├── events.h
│   │   └── types.h
│   ├── xadc_core.cpp
│   ├── xadc_core.h
│   └── xadc_core_if.h
├── xadc_main.cpp
└── xadc_webserver
    └── src
        ├── webserver_linux.c
        └── xadc_web_if.c
```

## Build steps

If cloned properly you should have a proper layout already installed, then

```
mkdir build
cd build
cmake .. -DCMAKE_TOOLCHAIN_FILE=../dependencies/builder/zynq7-linux.cmake
make
```

Tweak CMakelist with your zynq-7000 target

```
make install
```

## Latency Measurements

```
root@popov:/opt/shared/bin/xadc# ./xadc_get_value_temp 
int get_iio_node(const char*): xadc device mapped on : /sys/bus/iio/devices/iio:device0(iio:device0)
main(int, char**)::<lambda(std::__cxx11::string)>: proccess
Timer(int main(int, char**)): <t> = 100.366us, std = 11.6542us, 86.894us <= t <= 409.538us (n=4096)
```
