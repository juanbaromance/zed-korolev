#include "cIIOAlarm.h"
#include "util.h"
#include <filesystem>
#include <fstream>
#include <experimental/filesystem>

enum SysfsAction { Sync = true, Read = false };
template <typename T>
static bool sysfsParam( std::string const & filename, T && var, SysfsAction action = Read )
{
    bool ret_val{false};
    if ( action == Sync )
    {
        std::ofstream fs;
        fs.open( filename );
        if( fs.is_open() )
        {
            fs << static_cast<size_t>(var);
            fs.close();
            ret_val = true;
        }
    }
    else
    {
        std::ifstream fs;
        fs.open( filename );
        if( fs.is_open() )
        {
            fs >> std::dec >> var;
            fs.close();
            ret_val = true;
        }
    }
    return ret_val;
};


#include <type_traits>
template< typename T=size_t>
inline static auto scaler( const cIIOchannel::converter & c, T val )
{
    if constexpr ( std::is_same<T,size_t>::value )
        return round( ( val + std::get<0>(c) ) * std::get<1>(c) );
    return round( ( val/ std::get<1>(c) ) - std::get<0>(c) );
}

#include <thread>

void cIIOAlarm::deployAlarms( const std::vector<cIIOchannel> & channels, const char* sys_path )
{
    using namespace std;
    sysfs.assign( sys_path + string("/events"));

    using namespace ranges;

    for_each( channels,[&]( auto & channel ) {
        bool alarm_exists{false}, voltage{true};
        string surname{"_thresh_either_en"};
        string tmp( sysfs + "/" + basename( channel.sys_name().c_str()) + surname );

        namespace fs = std::experimental::filesystem;
        if( exists( fs::path(tmp.c_str()) ) )
            alarm_exists = true;
        else if( tmp = sysfs + "/" + basename( channel.sys_name().c_str()) + (surname="thresh_rising_en");
                 exists( fs::path(tmp.c_str()) ))
        {
            alarm_exists = true;
            voltage = false;
        }

        if( alarm_exists )
        {
            string alarm_sysfs{sysfs + "/" + basename( channel.sys_name().c_str()) + surname };
            tmp = string(channel.whoami()).size() ? channel.whoami() : basename( channel.sys_name().c_str());
            alarms[ tmp ] = {alarm_sysfs, false, 0, 4096, voltage, channel.scaling(), channel.xadc_index() };

            if( auto v = splitme<bool>( basename( channel.sys_name().c_str()),"_"); v.size() > 0 )
                for( auto r : {"voltage(\\d+)","temp(\\d+)"} )
                    if ( cmatch what; regex_match( v[1].c_str(), what, regex(r) ) )
                        if ( what[1].matched )
                            kernel_space[ v[1] ] = tmp;
        }
    });

    std::cout << "\n";
    std::stringstream oss;
    oss << "IIO.Alarms state\n";
    for_each( alarms, [&]( auto & a ) {

        Alarm & alarm = a.second;
        size_t high{},low{};
        cIIOchannel::converter & c = std::get<Scaler>(alarm);

        auto alarm_state = [&](){
            bool enabled{false};
            sysfsParam( std::get<SysFsPath>(alarm), enabled );
            std::get<Type>(alarm) = Voltage;

            if( auto v = splitme<bool>(std::get<SysFsPath>(alarm),"thresh"); v.size() == 2 )
            {
                sysfsParam( v[0] + "thresh_rising_value", high );
                if( sysfsParam( v[0] + "thresh_falling_value", low ) == false )
                {
                    sysfsParam(v[0] + "thresh_rising_hysteresis", low );
                    std::get<Type>(alarm) = Temperature;
                }
            }
            std::get<Enabled>(alarm) = enabled;
            std::get<High>(alarm) = scaler( c, high );
            std::get<Low>(alarm) = scaler( c, low );
            user_space[ std::get<XADCIndex>(alarm) ] = a.first;
            operation_mask.set( std::get<XADCIndex>(alarm) );
            return enabled;
        };

        oss << setw(15) << a.first << " "
            << ( alarm_state() ? "Enabled" : "Disabled" ) << setw(10)
            << "(" << high << "," << low << ")/"
            << "(" << scaler( c, high ) << ","
            << scaler( c, low ) << ")"
            << "#" << std::get<XADCIndex>(alarm)
            << "\n";

    });

    if( alarms.size() )
        std::thread( & cIIOAlarm::callback<false>,this, string( basename(splitme<bool>(sysfs,"/events")[0].c_str()) )).detach();
    auditor( oss );
}

cIIOAlarm::eventScope cIIOAlarm::subscribeAlarm( const eventScope & mask, std::shared_ptr<iNotifier<>> notifier )
{
    std::stringstream oss;
    eventScope aux( operation_mask.to_ulong() & mask.to_ulong() ), tmp(aux);

    oss << "Attemp to subscribe " <<  notifier->whoAmI() << " 0x" << std::hex << mask.to_ulong();
    for( size_t i = 0; i < mask.size(); i++ )
        if( ( aux.test(i) == false ) & mask.test(i) )
            oss << "\nwarning : index " << std::dec << i
                << " from 0x" << std::hex << mask.to_ulong()
                << " not available on 0x" << std::setfill('0') << std::setw(8) << operation_mask.to_ulong() <<"\n";
    if( tmp.any() )
    {
        subscriptors.push_back( { notifier, tmp } );
        oss << "attached to mask 0x" << std::hex << tmp.to_ulong();
    }
    auditor( oss );
    std::cout << "\n";
    return tmp;
}

size_t cIIOAlarm::setAlarmParams( const alarm_params_t & v )
{
    std::bitset<32> m{0};
    using namespace ranges;
    std::stringstream oss;
    const char *backtrace = __FUNCTION__ ;

    oss << "Attemp to setup " << v.size() << " alarms\n";
    for_each( v,[&]( auto & a ){

        auto [ xadc_id, high, low ] = a;

        if( user_space.find(xadc_id) == user_space.end() )
        {
            oss << backtrace << ": index " << xadc_id << " not available on 0x"
                << std::setfill('0') << std::setw(8) << std::hex << operation_mask.to_ulong();
            return;
        }


        Alarm & alarm = alarms.find( user_space.find(xadc_id)->second )->second;
        if( std::get<cIIOAlarm::Type>(alarm) == Voltage )
        {
            if( low > high )
            {
                oss << backtrace << ": " << high  - low << " only positives diferentials available";
                return;
            }
        }
        else if( std::get<cIIOAlarm::Type>(alarm) == Temperature ) { low = high - low; }

        std::string name = alarms.find( user_space.find(xadc_id)->second )->first;
        cIIOchannel::converter & c = std::get<Scaler>(alarm);
        for( auto probe : { high, low } )
        {
            if( size_t tmp = scaler( c, probe ); tmp > 4096 )
            {
                oss << backtrace << ": " << probe << " ooranged > " <<  scaler( c, size_t{4096} );
                return;
            }
        }

        sysfs_path path{std::get<cIIOAlarm::SysFsPath>(alarm)};
        if( auto v = splitme<bool>(path,"thresh"); v.size() == 2 )
        {
            sysfs_path tmp;
            path.assign(v[0]);
            const char *surname = std::get<cIIOAlarm::Type>(alarm) == Voltage ? "thresh_falling_value" : "thresh_rising_hysteresis";
            if( sysfsParam( tmp.assign(path + "thresh_rising_value"), scaler(c,high), Sync ) )
            {
                if( sysfsParam( tmp.assign(path +  surname), scaler(c,low), Sync ) )
                {
                    if( sysfsParam( std::get<cIIOAlarm::SysFsPath>(alarm), 1, Sync ) )
                    {
                        iAlarm::low  & prev_low  = std::get<cIIOAlarm::Low>(alarm);
                        iAlarm::high & prev_high = std::get<cIIOAlarm::High>(alarm);
                        oss << std::setw(10) << name << " current high/low "
                            << prev_high << "/" << prev_low
                            << "(" << scaler(c,prev_high) << "/" << scaler(c,prev_low)  << ")"
                            << " replaced by settings " << high << "/" << low
                            << "(" << scaler(c,high) << "/" << scaler(c,low) << ") : synced\n";
                        std::get<cIIOAlarm::High>(alarm) = high;
                        std::get<cIIOAlarm::Low>(alarm) = low;
                        m.set( xadc_id );
                        return;
                    }
                }
            }
            oss << name << ".alarm settings : warning syncing " << tmp << " errno(" << errno << "): " << strerror(errno);
        }

    });

    auditor( oss );
    return m.to_ulong();
}

#include <linux/iio/events.h>
#include <fcntl.h>
#include <sys/ioctl.h>

template <bool debug>
int cIIOAlarm::callback( const std::string & device )
{
    static const char *backtrace = __PRETTY_FUNCTION__ ;
    auto auditory = [&]( const std::string & auditory, int fd = -1 )
    {
        std::stringstream oss;
        oss << backtrace << " " << auditory << " " << (( fd >= 0 ) ? strerror(errno) : "");
        if( fd >= 0)
            close( fd );
        return auditor( oss, fd >= 0 ? Warning : Verbose );
    };

    int event_fd;
    {
        std::string tmp{ std::string{"/dev/"} + device };
        if ( int fd = open(tmp.c_str(),O_RDONLY); fd >= 0 )
        {
            if ( ioctl( fd, IIO_GET_EVENT_FD_IOCTL, & event_fd ) < 0 )
                return auditory( std::string(": ioctl ") + tmp, fd );
            close( fd );
        }
        else return auditory( std::string(": open ") + tmp );
    }

    while( 1 )
    {

        if constexpr ( debug ) auditory( ": waiting iio-event activity on event " );
        struct iio_event_data event;
        if( int ret = read( event_fd, &event, sizeof(event) ) ; ret < 0 )
        {
            if( errno == EAGAIN )
            {
                auditory(": nothing available");
                continue;
            }
            return auditory( ":failed to read event from device" );
        }

        if constexpr ( debug )
        {
            std::stringstream oss;
            oss << "Triggered on 0x" << std::hex << event.id << "\n";
            auditory( oss.str() );
        }

        iioEventT iio_event{
            static_cast<enum iio_event_type>(IIO_EVENT_CODE_EXTRACT_TYPE(event.id)),
            static_cast<enum iio_chan_type>(IIO_EVENT_CODE_EXTRACT_CHAN_TYPE(event.id)),
            static_cast<enum iio_event_direction>(IIO_EVENT_CODE_EXTRACT_DIR(event.id)),
            IIO_EVENT_CODE_EXTRACT_CHAN(event.id) };

        eventProcessor( iio_event );

    }
    return 0;
}

#include <algorithm>
void cIIOAlarm::eventProcessor(const cIIOAlarm::iioEventT & e )
{
    using namespace std;
    stringstream oss;
    string key{""};

    {
        array<enum iio_event_type,1> type = {IIO_EV_TYPE_THRESH};
        if( std::find(type.begin(),type.end(),std::get<0>(e)) == type.end() )
            oss << "unknown type(" << std::get<0>(e) << ")";
    }

    {
        array<enum iio_chan_type,2> type = { IIO_TEMP, IIO_VOLTAGE };
        if( std::find(type.begin(),type.end(),std::get<1>(e)) == type.end() )
            oss << "unknown channel type(" << std::get<1>(e) << ")";
        else
        {
            key = ( std::get<1>(e) == IIO_TEMP ) ? "temp" : "voltage";
        }
    }

    {
        array<enum iio_event_direction,3> directions =
            { IIO_EV_DIR_EITHER,IIO_EV_DIR_RISING,IIO_EV_DIR_FALLING};
        if( std::find(directions.begin(),directions.end(),std::get<2>(e)) == directions.end() )
            oss << "unknown direction(" << std::get<2>(e) << ")";
    }

    string tmp{string(key)+ to_string(std::get<3>(e))};
    if( kernel_space.find(tmp) == kernel_space.end() )
        oss << "unknown event " << tmp;

    if( oss.str().empty() )
        eventForwarding( alarms.find(kernel_space.find(tmp)->second)->second );

    auditor( oss );
}

template <bool verbose>
void cIIOAlarm::eventForwarding(const Alarm &alarm )
{
    static const char* backtrace = __FUNCTION__ ;
    std::string activity{std::get<0>(alarm)};
    size_t index{std::get<XADCIndex>(alarm)};

    ranges::for_each( subscriptors, [=]( Subscription & s ) {
        auto [ notifier, mask ] = s;
        if( notifier.lock() )
        {
            if constexpr ( verbose )
            {
                std::stringstream oss;
                oss << backtrace
                    << "(" << activity << ")\n" << notifier.lock()->whoAmI()
                    << " sampling on 0b" << mask
                    << " on index " << index;
                auditor(oss);
            }
            if( mask.test( index ) )
                if( iChannel::state_t state{ channel.channelState( 1 << index ) }; state.size() )
                    notifier.lock()->event( 1 << index, std::get<iChannel::analog>(state[0]) );
        }
    } );
}




