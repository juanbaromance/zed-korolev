#ifdef __0
#define FPGA_FREQ_CLK_BASE 1000000
#define GET_DUTY_PERCEN(d) (d * 100.0 / 255)

int fpga_write_rgb(fpga_color_t *r, fpga_color_t *g, fpga_color_t *b)
{
   int ret=0;
   int freq_div = 0;
   int value=0;



   ret = fpga_write_reg(GENERAL_IO_PWM_R_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, 0x00000001);
   freq_div = FPGA_FREQ_CLK_BASE / r->freq;
   if (freq_div)
      freq_div--;
   for (int i = 0; i < 4; i++)
   {
      ret += fpga_write_reg(GENERAL_IO_PWM_R_PERIOD_B1_OFFSET + i, UIO_AXI_GENERAL_IO, ( (freq_div >> (8 * i)) & 0x000000FF) );
   }
   ret += fpga_write_reg(GENERAL_IO_PWM_R_DUTY_OFFSET, UIO_AXI_GENERAL_IO, GET_DUTY_PERCEN(r->duty));
   fpga_read_reg(GENERAL_IO_PWM_R_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, &value);
   value |= 0x00000006;
   ret += fpga_write_reg(GENERAL_IO_PWM_R_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, value);
   value &= (~0x00000007);
   ret += fpga_write_reg(GENERAL_IO_PWM_R_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, value);



   ret = fpga_write_reg(GENERAL_IO_PWM_G_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, 0x00000001);
   freq_div = FPGA_FREQ_CLK_BASE / g->freq;
   if (freq_div)
      freq_div--;
   for (int i = 0; i < 4; i++)
   {
      ret += fpga_write_reg(GENERAL_IO_PWM_G_PERIOD_B1_OFFSET + i, UIO_AXI_GENERAL_IO, ( (freq_div >> (8 * i)) & 0x000000FF) );
   }
   ret += fpga_write_reg(GENERAL_IO_PWM_G_DUTY_OFFSET, UIO_AXI_GENERAL_IO, GET_DUTY_PERCEN(g->duty));
   fpga_read_reg(GENERAL_IO_PWM_G_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, &value);
   value |= 0x00000006;
   ret += fpga_write_reg(GENERAL_IO_PWM_G_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, value);
   value &= (~0x00000007);
   ret += fpga_write_reg(GENERAL_IO_PWM_G_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, value);




   ret = fpga_write_reg(GENERAL_IO_PWM_B_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, 0x00000001);
   freq_div = FPGA_FREQ_CLK_BASE / b->freq;
   if (freq_div)
      freq_div--;
   for (int i = 0; i < 4; i++)
   {
      ret += fpga_write_reg(GENERAL_IO_PWM_B_PERIOD_B1_OFFSET + i, UIO_AXI_GENERAL_IO, ( (freq_div >> (8 * i)) & 0x000000FF) );
   }
   ret += fpga_write_reg(GENERAL_IO_PWM_B_DUTY_OFFSET, UIO_AXI_GENERAL_IO, GET_DUTY_PERCEN(b->duty));
   fpga_read_reg(GENERAL_IO_PWM_B_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, &value);
   value |= 0x00000006;
   ret += fpga_write_reg(GENERAL_IO_PWM_B_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, value);
   value &= (~0x00000007);
   ret += fpga_write_reg(GENERAL_IO_PWM_B_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, value);

   return 0;
}

int fpga_write_blink_ctrl(int value)
{
   int ret=0;


   ret += fpga_write_reg(GENERAL_IO_PWM_BL_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, value);
   return ret;
}

int fpga_write_blink(fpga_color_t *blink)
{
   int freq_div=0;
   int ret=0;
   int value=0;



   ret = fpga_write_blink_ctrl(0x00000001);

   freq_div = FPGA_FREQ_CLK_BASE / blink->freq;
   if (freq_div)
        freq_div--;

   for (int i = 0; i < 4; i++)
   {
      ret += fpga_write_reg(GENERAL_IO_PWM_BL_PERIOD_B1_OFFSET + i, UIO_AXI_GENERAL_IO, ( (freq_div >> (8 * i)) & 0x000000FF) );
   }
   ret += fpga_write_reg(GENERAL_IO_PWM_BL_DUTY_OFFSET, UIO_AXI_GENERAL_IO, GET_DUTY_PERCEN(blink->duty));
   fpga_read_reg(GENERAL_IO_PWM_BL_CONTROL_OFFSET, UIO_AXI_GENERAL_IO, &value);
   value |= 0x00000006;
   ret += fpga_write_blink_ctrl(value);
   value &= (~0x00000007);
   ret += fpga_write_blink_ctrl(value);

   return ret;
}

#endif
