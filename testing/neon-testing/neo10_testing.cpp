#include "neo10_testing.h"
#include "neon-10/NE10.h"
#include <valarray>
#include <rtimers/cxx11.hpp>
#include <rtimers/posix.hpp>

template <int length=1024, bool verbose = false, size_t iterations = 1 << 10, bool core = false >
void neo_add_testing(void)
{

    static ne10_float32_t v[length], k, o[length];
    for (int i = 0; i < length; i++)
        v[i] = (ne10_float32_t)rand() / RAND_MAX * 5.0f;
    k = (ne10_float32_t)rand() / RAND_MAX * 5.0f;

    using namespace rtimers;
    {
        cxx11::DefaultTimer tmr("neo10 :");
        for( size_t i = 0; i < iterations; i++ )
        {
            tmr.start();
            ne10_addc_float( o, v, k, length );
            tmr.stop();
        }
    }

    using namespace std;
    valarray<ne10_float32_t> va(length),vb;
    va = va.apply( []( ne10_float32_t){ return ( ne10_float32_t)rand() / RAND_MAX * 5.0f; });
    {
        cxx11::DefaultTimer tmr("c++:" + std::to_string( va.size() ));
        for( size_t i = 0; i < iterations; i++ )
        {
            tmr.start();
            vb = va.apply( []( ne10_float32_t n ){ return n + k; } );
            tmr.stop();
        }
    }

    {
        cxx11::DefaultTimer tmr("core :");
        for( size_t i = 0; i < iterations; i++ )
        {
            tmr.start();
            for( size_t j = 0; j < length; j++ )
                o[j] = v[j] + k;
            tmr.stop();
        }
    }

    if( verbose )
    {
        printf("%s(%d,%d):\n", __FUNCTION__ , length, iterations );
        for (int i = 0; i < length; i++)
            if( ( i % static_cast<size_t>( round( length * 0.1 ) ) ) == 0 )
                printf("\tne10_addc_float:\t%f + %f = %f : %f + %f = %f\n",
                       v[i], k, o[i], va[i], k, vb[i] );
    }
}







#include "util.h"
#include <arm_neon.h>
void intrinsics_testing()
{
    using namespace rtimers;
    {
        cxx11::DefaultTimer tmr( __FUNCTION__ );

        std::array<float32_t,4> a{1,2,3,4}, b,c,d,e,f,g,h,k;
        auto tmp = vld1q_f32( a.data());
        for( size_t i = 0; i < ( 1 << 10 ); i++ )
        {
            tmr.start();
            vst1q_f32( b.data(), vmulq_n_f32(tmp, 1.1));
            vst1q_f32( c.data(), vmulq_n_f32(tmp, 1.2));
            vst1q_f32( d.data(), vmulq_n_f32(tmp, 1.3));
            vst1q_f32( e.data(), vmulq_n_f32(tmp, 1.4));
            vst1q_f32( f.data(), vmulq_n_f32(tmp, 1.5));
            vst1q_f32( g.data(), vmulq_n_f32(tmp, 1.6));
            vst1q_f32( h.data(), vmulq_n_f32(tmp, 1.7));
            vst1q_f32( k.data(), vmulq_n_f32(tmp, 1.8));
            tmr.stop();
        }

        ranges::for_each(a,[](float32_t i){ std::cout << i << " "; });
        std::cout << "\n";
        ranges::for_each(b,[](float32_t i){ std::cout << i << " "; });
        std::cout << "\n";
    }

    {
        std::array<float32_t,16> a{
            1 ,  2,  3,  4,
            5 ,  6,  7,  8,
            9 , 10, 11, 12,
            13, 14, 15, 16
        };
        std::array<float32_t,4> b,c,d,e;
        cxx11::DefaultTimer tmr( __FUNCTION__ );
        float32x4x4_t tmp = vld4q_f32( a.data() );
        for( size_t i = 0; i < ( 1 << 10 ); i++ )
        {
            tmr.start();
            vst1q_f32( b.data(), vmulq_n_f32(tmp.val[0], 1));
            vst1q_f32( c.data(), vmulq_n_f32(tmp.val[1], 2));
            vst1q_f32( d.data(), vmulq_n_f32(tmp.val[2], 3));
            vst1q_f32( e.data(), vmulq_n_f32(tmp.val[3], 4));
            tmr.stop();
        }
        ranges::for_each(a,[](float32_t i){ std::cout << i << " "; });
        std::cout << "\n";
        ranges::for_each(b,[](float32_t i){ std::cout << i << " "; });
        std::cout << "\n";
        ranges::for_each(c,[](float32_t i){ std::cout << i << " "; });
        std::cout << "\n";
        ranges::for_each(d,[](float32_t i){ std::cout << i << " "; });
        std::cout << "\n";
        ranges::for_each(e,[](float32_t i){ std::cout << i << " "; });
        std::cout << "\n";
    }
}
