#!/bin/bash
# Builds a QEMU image using the petalinux system-linux bsp output artefacts.
# below assumes default names for the artefacts

C4='\033[00;34m'
C5='\033[00;32m'
RC='\033[0m'
export LANG=""
auditor="${C4}`basename $0`:${RC}"

FSBL="zynq_fsbl.elf"
PLBitStream="system.bit"
QEMU_IMAGE="qemu_sd.img"
#QEMU_IMAGE="test"

KERNEL="zImage"
KERNEL_DTB="system.dtb"
PETALINUX_SETTINGS="./petalinux-settings.sh"

printf "\n"
date

which petalinux-package >& /dev/null
[ $? = 1 ] &&
{
    printf "${auditor} ${C5}Petalinux${RC} environment not sampled: let's fix it\n"
    [ ! -f ${PETALINUX_SETTINGS} ] &&
    {
        printf "${auditor} ooops fault again, missed ${C5}${PETALINUX_SETTINGS}${RC} environment fixer: unable to proceed\n\n"
        exit;
    }
    source ${PETALINUX_SETTINGS}
}

[ -z "`lsmod | grep nbd`" ] && sudo modprobe nbd
nbd_device="/dev/nbd0"

[ "$1" == "umount" ] && sudo qemu-nbd -d ${nbd_device} && { [ $? = 0 ] && printf "\n${auditor} image released\n\n"; exit; }
[ -z "$1" ] && printf "${auditor} image file missed\n\n" && exit
printf "\n${auditor} coupling ${C5}$1${RC} to Network block devide ${C5}${nbd_device}${RC}\n"
sudo qemu-nbd -f raw -c ${nbd_device} $1
[ $? = 0 ] && { read -t 1; printf "${auditor} partitions map\n `cat /proc/partitions | grep nbd`\n\n"; }

