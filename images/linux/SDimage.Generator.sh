#!/bin/bash
# Builds a QEMU image using the petalinux system-linux bsp output artefacts.
# below assumes default names for the artefacts

C4='\033[00;34m'
C5='\033[00;32m'
RC='\033[0m'
export LANG=""
auditor="${C4}`basename $0`:${RC}"

FSBL="zynq_fsbl.elf"
PLBitStream="system.bit"
QEMU_IMAGE="qemu_sd.img"
#QEMU_IMAGE="test"

KERNEL="zImage"
KERNEL_DTB="system.dtb"
PETALINUX_SETTINGS="./petalinux-settings.sh"

printf "\n"
date

which petalinux-package >& /dev/null
[ $? = 1 ] &&
{
    printf "${auditor} ${C5}Petalinux${RC} environment not sampled: let's fix it\n"
    [ ! -f ${PETALINUX_SETTINGS} ] &&
    {
        printf "${auditor} ooops fault again, missed ${C5}${PETALINUX_SETTINGS}${RC} environment fixer: unable to proceed\n\n"
        exit;
    }
    source ${PETALINUX_SETTINGS}
}

printf "${auditor} Generating BOOT.BIN -> FSBL( ${FSBL} ) + PLBitStream( ${PLBitStream} )\n"
petalinux-package --boot --fsbl ${FSBL} --fpga ${PLBitStream} --u-boot --force

printf "${auditor} Setting up image ${C5}${QEMU_IMAGE}${RC}\n"
[ -f "${QEMU_IMAGE}" ] && {
    
    printf "A previous ${C5}${QEMU_IMAGE}${RC} image has been detected\n`ls -al -h ${QEMU_IMAGE}`\n"
    printf "\nPress any key to proceed ${C5}compression${RC} or ctrl-c to abort\n"
    read -t 5 -n  1
    [ $? = 0 ] && {
        gzip ${QEMU_IMAGE} -S .`date +%H%M%S.%d%m%Y`.gz
        ls -al -h ${QEMU_IMAGE}*
    }
}

dd if=/dev/zero of=${QEMU_IMAGE} bs=1024M count=1
mkfs.ext4 ${QEMU_IMAGE}

[ ! -d "./tmp" ] && mkdir tmp;
sudo mount -o loop qemu_sd.img tmp/
printf "${auditor} Transfering artefacts to ${QEMU_IMAGE}\n\n"
sudo cp BOOT.BIN ${KERNEL} ${KERNEL_DTB} tmp/
cd tmp;
sudo tar xvzf ../rootfs.tar.gz
sudo cp -dpR ../addons/* .
cd ..
sudo umount tmp

# mkfs.vfat -F 32 ${QEMU_IMAGE}
# mcopy -i ${QEMU_IMAGE} BOOT.BIN ::/
# mcopy -i ${QEMU_IMAGE} ${KERNEL} ::/
# mcopy -i ${QEMU_IMAGE} ${KERNEL_DTB} ::/
