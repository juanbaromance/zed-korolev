#include <chrono>
#include <memory>
#include <rtimers/cxx11.hpp>
#include <rtimers/posix.hpp>
#include "neon-10/NE10.h"
#include <algorithm>
#include "util.h"
#include "neo10_testing.h"
#include "filtering/cBiquad.h"
#include "filtering/cFFTView.h"
#include "filtering/cFIR.h"
#include "interpolators/interpolators.h"

template <typename T>
bool testing( T && operative ){ return operative.testing(); }

#include "afin/cAfin.h"

int main()
{

    using namespace std;
    auto timenow = chrono::system_clock::to_time_t(chrono::system_clock::now());
    cout << endl << ctime( & timenow );
    cout << __PRETTY_FUNCTION__ << endl << endl;

    if ( ne10_init() == NE10_OK )
    {
        // neo_add_testing();
        // intrinsics_testing();
        testing( cFFTView<>() );

        return 0;
        testing( cInterpolators() );

        {
            testing( cBiQuad("gBiQuad", 1000, 20) );
            testing( cBiQuad2("cBiQuad2",1000,20) );
            testing(cFIR());

            testing( cFFTView<>() );
            testing( cAfin() );

        }
    }

    return 0;

}
