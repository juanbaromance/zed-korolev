#pragma once

#include "util.h"
#include <vector>
#include <tuple>

#include <linux/iio/types.h>

#include <bitset>
#include "cIIOChannel.h"

class iAlarm {
public:
    using high = double;
    using low = double;
    using xadc_id = size_t;
    using alarm_params_t = std::vector<std::tuple<xadc_id,high,low>>;

    virtual size_t setAlarmParams( const alarm_params_t & ) = 0;
    virtual void deployAlarms( const std::vector<cIIOchannel> & channels, const char* device )= 0;
    using eventScope = std::bitset<32>;

    template <typename T=double>
    struct iNotifier {
        virtual const char* whoAmI() = 0;
        virtual void event( const eventScope & mask, const T & ) = 0;
    };

    virtual eventScope subscribeAlarm ( const eventScope & mask , std::shared_ptr<iNotifier<>> notifier) = 0;
    virtual ~iAlarm(){}
};


class cIIOAlarm : public Module<cIIOAlarm>, public iAlarm
{

public:
    cIIOAlarm( iChannel & channel_ ) :  Module<cIIOAlarm>( "","cIIOAlarm"), channel( channel_ ) {}
    void deployAlarms( const std::vector<cIIOchannel> & channels, const char* device );
    size_t setAlarmParams(const alarm_params_t & );
    eventScope subscribeAlarm ( const eventScope & mask , std::shared_ptr<iNotifier<>> notifier);

private:
    std::string internalAuditory() const { return ""; }
    template <bool debug=false>
    int callback( const std::string & device );

    friend Module<cIIOAlarm>;

    enum {
        SysFsPath,
        Enabled,
        High,
        Low,
        Type,
        Scaler,
        XADCIndex
    };

    enum { Voltage = false, Temperature = true };
    using sysfs_path = std::string;
    using Alarm = std::tuple<sysfs_path,bool,high,low,bool,cIIOchannel::converter,xadc_id>;
    using alarm_index = std::string;

    std::map<alarm_index,Alarm> alarms;
    std::string sysfs;

    std::map<std::string,alarm_index> kernel_space;
    std::map<size_t,alarm_index> user_space;

    using channel_id = size_t;
    using iioEventT = std::tuple<enum iio_event_type, enum iio_chan_type, enum iio_event_direction, channel_id >;
    void eventProcessor( const iioEventT & );

private:

    using Subscription = std::tuple< std::weak_ptr<iNotifier<>>,eventScope>;
    std::vector<Subscription> subscriptors;
    template <bool verbose=false>
    void eventForwarding( const Alarm & alarm );
    eventScope operation_mask;
    iChannel & channel;
};

