#!/bin/sh

for i in "$@";
do
   echo generating patch for "$i" module;
   
   AUX=`echo "$i" | sed s/-/_/g`;
   echo "substituing axi-general-io with "$i"";
   echo "substituing axi_general_io with "$AUX"";

#   cat uio.patch | sed s/axi-general-io/"$i"/g > "$i".patch;
   sed -i s/axi_general_io/"$AUX"/g "$i".patch;
done

exit 0;
