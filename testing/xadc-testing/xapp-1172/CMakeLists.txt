# Created by and for Qt Creator This file was created for editing the project sources only.
# You may attempt to use it for building too, by modifying this file here.

cmake_minimum_required (VERSION 3.5)
project (xadc-testing)
set ( artefact ${PROJECT_NAME} )

set ( pwd ${CMAKE_SOURCE_DIR})
# set ( CMAKE_INCLUDE_CURRENT_DIR ON)
set ( CMAKE_CXX_STANDARD 17)
# set ( CMAKE_CXX_STANDARD_REQUIRED ON)
set ( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -fno-permissive" )
set( GLOB CMAKE_ASM_FLAGS "-mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9")

include_directories(
    ./dependencies/include
    ./dependencies/include/xadc-testing/
    ./dependencies/include/kernel
)

enable_language(C ASM)
set ( CMAKE_TOOLCHAIN_FILE ./dependencies/builder/zynq7-linux.cmake)

add_subdirectory(xadc_lib)
# add_subdirectory(geometry)

set ( sources xadc_main.cpp )
add_executable(${artefact}.x ${sources} )
target_include_directories(${artefact}.x PUBLIC ./dependencies/include)
target_link_libraries(${artefact}.x xadc_lib pthread )


link_directories(${pwd}/dependencies/lib)
set ( CMAKE_EXE_LINKER_FLAGS "-L${pwd}/dependencies/lib -Wl,--as-needed")

set ( deploy_path /opt/shared/bin/xadc )
install (CODE "execute_process( \
    COMMAND scp ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}./${artefact}.x root@popov:${deploy_path} \
    COMMAND ssh root@popov \"printf Status@; hostname; ls -al ${deploy_path}/*.x\")")
