#include "cXADCps.h"
#include <iostream>
#include <bitset>
#include <array>
#include <vector>
#include <cmath>
#include "bsp/timers.h"
#include "bsp/xadcps.h"
#include "bsp/xscugic.h"
#include "bsp/event_groups.h"

#define DELAY_1_SECOND		1000UL


static void XAdcInterruptHandler( void *CallBackRef )
{
    xadc_irq_wrapper *xadc = ( xadc_irq_wrapper *)CallBackRef;
    XAdcPs *XAdcPtr = xadc->XAdcPtr;
    u32 IntrStatusValue = XAdcPs_IntrGetStatus(XAdcPtr);
    if (IntrStatusValue & XADCPS_INTX_ALM0_MASK)
    {
        xadc->action = 1 << XADC_Alarm0;
        XAdcPs_IntrDisable(XAdcPtr,XADCPS_INTX_ALM0_MASK);

    }
    if (IntrStatusValue & XADCPS_INTX_ALM5_MASK) {
        xadc->action |= 1 << XADC_Alarm5;
        XAdcPs_IntrDisable(XAdcPtr,XADCPS_INTX_ALM5_MASK);
    }

    if( xadc->action )
    {
        BaseType_t xHPTWoken{pdFALSE};
        if( xEventGroupSetBitsFromISR( xadc->events, xadc->action, & xHPTWoken ) == pdPASS )
            portYIELD_FROM_ISR( xHPTWoken );
    }
    XAdcPs_IntrClear(XAdcPtr, IntrStatusValue);
}


cXadcPS::cXadcPS(cXadcPS::device_id dev_id)
{
    XAdcPs_Config *cfg = XAdcPs_LookupConfig(dev_id);
    XAdcPs_CfgInitialize( phy, cfg, cfg->BaseAddress );
    irq_wrapper.XAdcPtr = phy;
    irq_wrapper.action = irq_wrapper.mask = 0;
}

cXadcPS &cXadcPS::autotest() {  enabled = ( XAdcPs_SelfTest( phy ) == XST_SUCCESS ) ? true : false; return *this; }

int cXadcPS::prepareInterrupts(u16 interrupt_id)
{
    XScuGic_Config *cfg = XScuGic_LookupConfig( XPAR_SCUGIC_SINGLE_DEVICE_ID );
    if ( XScuGic_CfgInitialize( gic, cfg, cfg->CpuBaseAddress ) != XST_SUCCESS)
        return XST_FAILURE;
    if (XScuGic_Connect(gic, interrupt_id, (Xil_InterruptHandler)XAdcInterruptHandler, (void *)( & irq_wrapper) ) != XST_SUCCESS)
        return XST_FAILURE;
    XScuGic_Enable( gic, interrupt_id);
    Xil_ExceptionInit();
    Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,(Xil_ExceptionHandler) XScuGic_InterruptHandler, gic);
    Xil_ExceptionEnable();
    return XST_SUCCESS;
}

void cXadcPS::trampolineOnBottomHalf(void *argument){ static_cast<cXadcPS*>(argument)->bottomHalf(); }

#include "bsp/FreeRTOS.h"
#include "bsp/task.h"

bool cXadcPS::run()
{
    XAdcPs_SetSequencerMode( phy, XADCPS_SEQ_MODE_SAFE);
    XAdcPs_SetAlarmEnables( phy ,0x0000);

    u16 tmp = XAdcPs_GetAdcData( phy, XADCPS_CH_TEMP);
    XAdcPs_SetAlarmThreshold( phy, XADCPS_ATR_TEMP_UPPER,(tmp - 0x07FF));
    XAdcPs_SetAlarmThreshold( phy, XADCPS_ATR_TEMP_LOWER,(tmp + 0x07FF));

    u16 VccpauxData = XAdcPs_GetAdcData( phy, XADCPS_CH_VCCPAUX);
    XAdcPs_SetAlarmThreshold(phy, XADCPS_ATR_VCCPAUX_UPPER,(VccpauxData-0x07FF));
    XAdcPs_SetAlarmThreshold(phy, XADCPS_ATR_VCCPAUX_LOWER,(VccpauxData+0x07FF));
    std::bitset<16> alarm_mask{XADCPS_CFR1_ALM_VCCPAUX_MASK | XADCPS_CFR1_ALM_TEMP_MASK};
    XAdcPs_SetAlarmEnables( phy, irq_wrapper.mask = static_cast<u16>(alarm_mask.to_ulong()) );

    if ( prepareInterrupts() != XST_SUCCESS)
    {
        std::cout << __PRETTY_FUNCTION__ << ": interrupts-prepare failured\n";
        return false;
    }

    XAdcPs_IntrClear( phy, XAdcPs_IntrGetStatus(phy));
    XAdcPs_IntrEnable( phy,(XADCPS_INTX_ALM5_MASK | XADCPS_INTX_ALM0_MASK));

    std::cout << __PRETTY_FUNCTION__ << ": Waiting Alarms on mask 0x" << std::hex << irq_wrapper.mask << "\n";

    XAdcPs_SetSequencerMode( phy, XADCPS_SEQ_MODE_INDEPENDENT);

    irq_wrapper.events = xEventGroupCreate();
    xTaskCreate( trampolineOnBottomHalf, "xADC-BottomHalf", configMINIMAL_STACK_SIZE, this, tskIDLE_PRIORITY, & xHandle );
    previous = xTaskGetTickCount();
    return true;
}

void cXadcPS::bottomHalf()
{
    const TickType_t xDelay{ 500 / portTICK_PERIOD_MS};
    int deadline{ 10000 / portTICK_PERIOD_MS };

    while (1)
    {
        elapsed += xTaskGetTickCount() - previous;
        previous = xTaskGetTickCount();
        deadline -= elapsed;
        if( deadline <= 0 )
            break;

        std::cout << __FUNCTION__ << ":" << std::dec << elapsed << " on sampler(0x"
                  << std::hex << irq_wrapper.mask << ") on activity-mask(0x" << irq_wrapper.action << ") : ";
        for (  const auto & i : { XADCPS_CH_VCCPAUX, XADCPS_CH_TEMP } )
            std:: cout << XAdcPs_GetAdcData( phy, i ) << " ";
        std::cout << "\n";

        std::bitset<32> activity( xEventGroupWaitBits( irq_wrapper.events, operation_mask, pdTRUE, AnyTriggered, xDelay ) );
        if( activity.test( EndOfACQ ) )
        {
            std::cout << __FUNCTION__ << " EOAcquisition triggered\n";
            break;
        }

        {
            std::bitset<32> activity( irq_wrapper.action );
            for( size_t i = 0; i < activity.size(); i++ )
                if ( activity.test(i) )
                    std::cout << __FUNCTION__ << ": Alarms triggered on " << i << "\n";
        }
    }

    std::cout << __FUNCTION__ <<  "suspends\n";
    XAdcPs_SetSequencerMode(phy, XADCPS_SEQ_MODE_SAFE);
    vTaskSuspend( nullptr );

}
