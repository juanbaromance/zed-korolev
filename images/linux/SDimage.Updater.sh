#!/bin/bash
# Loads on the Zynq target openamp either linux or baremetal artefacs
C4='\033[00;34m'
C5='\033[00;32m'
BD='\033[00;1m'
RC='\033[0m'
auditor="${C5}`basename $0`:${RC}"

export LANG=""
printf "\n"
date

[ $# -ne 1 ] && printf "${auditor} : missed image [elf|x|ub]\n\n" && exit -1;
target=$1
[ ! -e ${target} ] && printf "${auditor} : ${target} no such file\n\n" && exit -1;

uZedQemu="uZed"
u96="u96"
uZedChallengeX="uZed.ChallengeX"
u96ChallengeX="u96.ChallengeX"
popov="uZedPopov"

zynqBoard=${popov}

# 1.- guess remote directory using input extension
bare_metal="n"
remote_path="/tmp"
if [ "${target##*.}" = "elf" ]; then
    remote_path="/lib/firmware"
    bare_metal="y"
    [ ! -z "`readelf ${target} -A | grep Realtime`" ] && zynqBoard="u96";
elif [ "${target##*.}" = "ub" ]; then { remote_path="/run/media/mmcblk0p1"; }
elif [ "${target##*.}" = "x"  ]; then remote_path="/opt/shared/bin"
fi

sshCommand="ssh"
scpCommand="scp"

# 2.- fix remote ip
case ${zynqBoard} in
    ${uZedQemu}|${uZedChallengeX}|${popov})
	[ ${zynqBoard} == ${uZedChallengeX} ] && host="192.6.1.131"
	[ ${zynqBoard} == ${uZedQemu}       ] && host="10.0.2.15"
	[ ${zynqBoard} == ${popov}          ] && host="192.168.0.11"
	;;

    ${u96}|${u96ChallengeX})
	host="localhost"
	forwardPort=1440
	sshCommand="${sshCommand} -p ${forwardPort}"
	scpCommand="${scpCommand} -P ${forwardPort}"
	;;
    
    *)
	printf "${auditor} environment \"${zynqBoard}\" : Unknown, nothing to do\n";
	# todo : print default environments
	exit -1;
	;;
esac

printf "\n${auditor} Installing ${C4}${target}${RC} on ${host}:${remote_path}(${C4}${zynqBoard}${RC})\n"

# 3.- Test if the remote directory specification is available
# remote_path="popov"
${sshCommand} root@$host "[ -d "${remote_path}" ] \
  || { printf \"${auditor} ${remote_path} no such directory \n\n\"; exit 1; } \
  && exit 0"
[  $? = 1 ] && exit -1;
# exit

# 4.- Prepare backups auditory and git stuff
snapshot="`date +%H%M%S%d%m%Y`";
backup="${target}.$snapshot";
remote_auditory="auditory.log"
${sshCommand} root@$host "[ -f "${remote_path}/${target}" ] \
&& { \
  printf \"${auditor} backup required as ${backup}\n\"; \
  gzip ${remote_path}/${target} -S .${snapshot}.gz -N -k; \
  [ $? = 0 ] \
  && { \
      cd ${remote_path}; \
	  printf \"`date` backup ${target} as ${backup}.gz\n\" >> ${remote_auditory}; \
	  git add ${remote_auditory}; \
	  git commit -m \"update@${host} on `date` backup stuff @ ${backup}.gz\"; \
	  git log; \
	  exit 0; \
  } || exit 1; \
} || exit 1"
echo $?
[ $? = 1 ] && exit -1;

${scpCommand} ${target} root@${host}:${remote_path};
[ $? = 1 ] && exit -1;

${sshCommand} root@$host "ls -l ${remote_path}"
exit

[ "${bare_metal}" == "y" ] && 
{
   ${sshCommand} root@$host "ls -al ${remote_path};${remote_path}/ZynQopenAMP.sh `basename ${target}`"
   exit 0 ;
}

${sshCommand} root@${host} ${remote_path}/ZynQopenAMP.sh recycle
exit 0;
