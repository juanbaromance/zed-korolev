#ifndef __FPGA_A3633_DRV__
#define __FPGA_A3633_DRV__

#include <linux/types.h>

#define FPGA_CONTROL_RESET_BIT		0x01	//Activo nivel alto
#define FPGA_CONTROL_DUTY_BIT		0x02
#define FPGA_CONTROL_FREQ_BIT		0x04
#define FPGA_CONTROL_POS_NEG_BIT	0x08

struct fpga_color_regs
{
	__u8  control;
	__u32 freq_div;
	__u8 duty_div;
}__attribute__ ((packed));

struct fpga_version_regs
{
	__u16 code;
	__u8 ver;
	__u8 rev;
	__u16 firmware;
	__u8 dna[8];
}__attribute__ ((packed));

struct fpga_regs
{
	struct fpga_version_regs version;
	__u32 ppc_reset;
	__u8 gpi; //uSwitches
	__u8 gpo; //leds
	struct fpga_color_regs r, g, b, blink;
	__u32 ir_mask;
	__u8 beep_ctrl;
	__u32 watchdog_ctrl;
	__u32 heartbeat_ctrl;
	__u8 reset_node;
	__u8 ir_code;
}__attribute__ ((packed));

typedef struct
{
	int cmd;
	struct
	{
		int code;
		int ver;
		char rev;
		int firm;
		int dna_l;
		int dna_h;
	} version;
	struct
	{
		int freq_div;
		int duty_div;
	} r, g, b, blink;
	int offset;
	int size;
	int value;
	int ir_mask;
	int ir_code;
} fpga_gate_t;

enum
{
	FPGA_WRITE_RGB,
	FPGA_WRITE_BLINK,
	FPGA_READ_VERSION,
	FPGA_WRITE_REG,
	FPGA_READ_REG
};

#endif

