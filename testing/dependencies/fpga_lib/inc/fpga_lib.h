#pragma once

#ifdef __cplusplus

extern  "C" {
#endif

typedef struct
{
    int freq;
    int duty;
}fpga_color_t;

typedef void (*fpga_slot_cb)( unsigned int data, void *user_arg );

int fpga_boot(void);
int fpga_dump_version(char *version);
int fpga_init_gpi_polling (fpga_slot_cb);
int fpga_stop_gpi_polling(void);

int fpga_write_rgb(fpga_color_t *r, fpga_color_t *g, fpga_color_t *b);
int fpga_write_blink_ctrl(int value);
int fpga_write_blink(fpga_color_t *blink);
int fpga_write_gpo(int mask, int value);
int fpga_write_beep_ctrl(int value);
int fpga_write_watchdog_ctrl(int value);
int fpga_write_heartbeat_ctrl(int value);
int fpga_write_reset_ctrl    (int node, int value);
int fpga_write_ir_ctrl       (int mask, int code);
int fpga_write_reg           (int offset, int size, int value);

int fpga_read_ppc_reset_word (int *value);
int fpga_read_gpi            (int *value);
int fpga_read_gpo            (int *value);
int fpga_read_beep_ctrl      (int *value);
int fpga_read_watchdog_ctrl  (int *value);
int fpga_read_heartbeat_ctrl (int *value);
int fpga_read_reset_ctrl     (int *node, int *value);
int fpga_read_ir_ctrl        (int *mask, int *code);
int fpga_read_reg            (int offset, int size, int *value);

int fpga_configure_gpi(int mask);

#ifdef __cplusplus
}

#include <string>
#include <variant>

typedef void (*callback_t)( unsigned int data, void *ctx );
typedef struct { int state; } gpi_t;
typedef struct { int state, mask; } gpo_t;
typedef struct { int state; } beep_t;
typedef struct { int state; } watchdog_t;
typedef struct { int state; } heartbeat_t;
typedef struct { int state, mask; } motion_t;
typedef struct { int state, mask; } lines_t;
typedef struct { int node, state; } reset_t;
typedef struct { int code, mask; } ir_t;
typedef struct { int minor, offset, state; } reg_t;
using minor_t = std::variant<gpi_t,gpo_t,beep_t,watchdog_t,heartbeat_t,reset_t,ir_t,reg_t,motion_t,lines_t>;

template <typename T>
class iFPGADevice {
public:
    bool boot();
    void shutdown();
    const char *signature();
    void maskInterrupts( const uint32_t & mask );
    minor_t read    ( const minor_t & m );
    int     write   ( const minor_t & m );
    int Subscribe   ( const std::string & slot_id, const std::string & subscription, callback_t callback, void *context = nullptr );
    int unSubscribe ( const std::string & subscription );
};

// template<typename T> void fpga_boot( iFPGADevice<T>& object ){ object.boot(); }

#include <memory>
class cZynQ7FPGA : public iFPGADevice<cZynQ7FPGA>
{
    class Private;

public:
    cZynQ7FPGA();
    virtual ~cZynQ7FPGA();

private:
    std::unique_ptr<Private> priv;
    friend iFPGADevice;
};

#endif


