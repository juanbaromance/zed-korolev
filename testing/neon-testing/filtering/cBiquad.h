#ifndef __BIQUAD_iface
#define __BIQUAD_iface

#include "neon-testing/main.h"
#include <vector>
#include <string>
#include "cFilter.h"


class cBiQuad : public iFilter<cBiQuad>
{
    enum Numerology {
        LowPass,
        Notch,
    };

public:
    cBiQuad( std::string name_, const std::vector<float>& poles, const std::vector<float>& zeros, int order );
    cBiQuad (std::string name_, size_t Fs_, size_t Fc_, double Q_ = 0.7071, enum Numerology topology_ = LowPass );
    void  tune( size_t Fs_, size_t Fc_, double Q_ ){ ZeroPoleMap( Fs = Fs_, Fc = Fc_, Q = Q_, topology ); }
    virtual ~cBiQuad();
    bool  testing( );
    std::string name() const { return name_; }

    template<StrategyEnum strategy>
    float step ( const float & measure );

    // iFilter interface implementation
private:
    int reset( float input = 0, bool hardcore = false );

    inline float state(){ return o; }
    std::string reportImpl();
    inline void  samplingImpl( int Fs ){ return (void)Fs; }
    iFilter<cBiQuad>::spec_t specs();

private:
    void ZeroPoleMap(size_t Fs, size_t Fc, double Q = 0.7071, enum Numerology type = LowPass );
    const std::string name_;
    std::vector <float> z;
    std::vector <float> poles, zeros;
    float o;
    double Q;
    size_t Fc, Fs;
    Numerology topology;
    friend iFilter;
};

#include <array>
#include <memory>
#include <arm_neon.h>

class cBiQuad2 {
public:
    cBiQuad2( const std::string & name_, const std::array<float,2> &poles ={}, const std::array<float,3> &zeros ={});
    cBiQuad2( const std::string & name_, size_t Fs, size_t Fc, double Q = 0.7071);
    void settings( size_t Fs, size_t Fc, double Q = 0.7071);
    void reset();

    template<StrategyEnum strategy>
    float step( const float & sample );

    bool  testing( );
    std::string report(){ return bq->report(); };
    std::string name() const { return name_; }

private:
    template< bool debug=false>
    float stepCompiler( const float &sample );
    template< bool debug=false>
    float stepSIMD( const float &sample );

    std::array<float32_t,4> w;
    std::unique_ptr<iFilter<cBiQuad>> bq;
    iFilter<cBiQuad>::spec_t specs;
    float32x4_t weights;
    float acc;
    const std::string name_;
};

#endif


/**
  * @class  cBiQuad
  * @brief IIR BiQuad support
  *
  * @ingroup Signal Filtering
  *
  * @fn cBiQuad::cBiQuad (std::string _name, size_t Fs, size_t Fc, double Q = 0.7071, enum Numerology _topology = LowPass );
  * @brief BiQuad 2nd order calculator using Frequency domain parameters.
  * @param _name The Filter namea.
  * @param _Fs Sampling time in Hz.
  * @param _Fc Cutting Hz where -3db is applied
  * @param Q Boost/Flat(ButterWorth)/Reductor factor.
  * @param _topology Only LowPass and Notch modalities are calculated.
  *
  * @fn std::string cBiQuad::report();
  * @brief Reports the current poles-zero configuration
  *
  * @fn void cBiQuad::tune( size_t Fs, size_t Fc, double Q ){ ZeroPoleMap( Fs, Fc, Q, topology ); }
  * @brief Tune current BiQuad specifications (aks the zero/poles map) using Frequency domain parameters.
  * @param _Fs Sampling time in Hz.
  * @param _Fc Cutting Hz where -3db is applied
  * @param Q Boost/Flat(ButterWorth)/Reductor factor.
*/
