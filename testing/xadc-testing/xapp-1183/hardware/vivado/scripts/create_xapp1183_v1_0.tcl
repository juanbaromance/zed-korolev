# Default variables

set package_name xapp1184
set design_top axi_xadc_top 
set board_name "axi_xadc_dma_zynq"
set action "compile"
set proj_dir deployment
set device xc7z020clg400-1

proc generate_project { package_spec design_top } {
    global proj_dir
    global board_name
    global device

    puts "\nProject generation : ${package_spec} on ${design_top}\n"
    create_project -name ${design_top} -force -dir "./${proj_dir}" -part ${device}
    set_property ip_repo_paths ./../../sources/ip_package [current_fileset]
    update_ip_catalog

    puts "\nIP Integration\n"
    source ./${package_spec}_ipi.tcl
    make_wrapper -files [get_files ./${proj_dir}/${design_top}.srcs/sources_1/bd/${board_name}/${board_name}.bd] -top -fileset [get_filesets sources_1] -import
}

proc compile_project { package_spec design_top  } {
    global proj_dir
    global board_name
    global device

    puts "\nProject building : ${package_spec} on ${design_top}\n"
    open_project ./${proj_dir}/${design_top}.xpr
    update_compile_order -fileset sources_1
    generate_target all [get_files ./${proj_dir}/${design_top}.srcs/sources_1/bd/${board_name}/${board_name}.bd]
    
    #reset_run synth_1
    #synth_design -top  [lindex [find_top] 0] -part ${device}

    #file mkdir ./${proj_dir}/${design_top}.sdk
    #set wrapper ${board_name}_wrapper
    #file copy -force ./${proj_dir}/${design_top}.runs/impl_1/${wrapper}.sysdef ./${proj_dir}/${design_top}.sdk/${wrapper}.hdf
}

if { $argc > 0 } {
    set package_name [lindex $argv 0]
    if { $argc > 1 } { set action [ lindex $argv 1] }
}

switch $action {
    compile {  compile_project ${package_name} ${design_top} }
    project {  generate_project ${package_name} ${design_top} }
    default {  puts "Nothing to do with \"$action\" on ${package_name}\n"  }
}





