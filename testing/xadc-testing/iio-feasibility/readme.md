# Purpose

This is a feasibility test program related with the [iio](https://www.kernel.org/doc/html/v4.14/driver-api/iio/index.html) Linux stack applied on the xadc device found on the Zynq7000 hardware piece
![xadc](https://bitbucket.org/juanbaromance/zed-korolev/downloads/Peripheral.png)

It will try to estimate the functional differences with the stack provided on the Xilinx application note [**xapp1172**](https://bitbucket.org/juanbaromance/zed-korolev/downloads/xapp1172_zynq_ps_xadc.pdf)

The iio **buffer** capability is coupled to the device tree *compatible* entry **xlnx,axi-xadc-1.00.a**, which requires the [**AXI/PL interface**](https://bitbucket.org/juanbaromance/zed-korolev/downloads/pg019_axi_xadc.pdf) to address the XADC operation at the **1MS** full rate. 
,it will be tested in a second approach. The [**JTAG PS interface**]( 
https://www.xilinx.com/video/soc/methods-communicating-xadc-zynq.html) it's software mapped through the **xlnx,zynq-xadc-1.00.a** compatible device entry, it's maximum expected throughtput is **100Khz** but this is an unrealistic context cause only have in account hardware operations.

The artefact will use the dynamic dependency of the [**iio library**](https://wiki.analog.com/software/linux/docs/iio/iio), so it must be properly deployed on the target. If it's required, apply on the Makefile rule **deploy** to fix it.

## Directory Layout

```
.
├── cIIOAlarm.cpp
├── cIIOAlarm.h
├── build
├── build.zynq
├── cIIOChannel.cpp
├── cIIOChannel.h
├── cIIODevice.cpp
├── cIIODevice.h
├── CMakeLists.txt
├── CMakeLists.txt.user
├── dependencies -> ../dependencies
├── main.cpp
├── main.h
├── readme.md
└── xilinx-xadc.txt -> ../kernel/Documentation/devicetree/bindings/iio/adc/xilinx-xadc.txt
```

## Build steps

If the project is properly cloned, you should have the filesystem layout ready to compile the sources

```
mkdir build
cd build
cmake .. -DCMAKE_TOOLCHAIN_FILE=../dependencies/builder/zynq7-linux.cmake
make
```

## Target install

Tweak the top [**CMakelist.txt**](https://bitbucket.org/juanbaromance/zed-korolev/src/master/testing/xadc-testing/iio-feasibility/CMakeLists.txt) with your zynq-7000 target, afterwards apply on the **build.zynq** directory entry

```
make install
```

## Current Performance

The program preamble deploys the required objects to test the io interface
```
root@popov:/opt/shared/bin/iio# ./iio-testing.x 
LOG (  1747 )( 18:47:30:076 )  cIIOdevice: 

Fri May  1 18:47:30 2020
context info: local.v0r19git(490c4aa)
Linux popov 4.19.0-xilinx-v2019.2 #1 SMP PREEMPT Sun Apr 5 22:05:45 UTC 2020 armv7l
LOG (  1747 )( 18:47:30:076 )  cIIOdevice: xadc attribs(1) maps 9 channels on iio:device0
 sample size(0) buffer attribs(0)

  ch#0   voltage5(input)   vccoddr:1489.75
  ch#1   voltage0(input)    vccint:993.896
  ch#2   voltage4(input)   vccpaux:1792.24
  ch#3      temp0(input)          :56598.8
  ch#4   voltage7(input)     vrefn:-2.92969
  ch#5   voltage1(input)    vccaux:1794.43
  ch#6   voltage2(input)   vccbram:993.896
  ch#7   voltage3(input)   vccpint:992.432
  ch#8   voltage6(input)     vrefp:1242.92
```

Now the I/O interface provided on top of the [channel and attribute](https://analogdevicesinc.github.io/libiio/group__Channel.html#gacb37ef830dcc100a9908d0592d1ff190) iio data types can be sampled
```

Latency testing #

ch#  0 voltage5   vccoddr : 1492.68/2038
ch#  1 voltage0    vccint : 993.896/1357
ch#  2 voltage4   vccpaux : 1793.7/2449
ch#  3 temp0           : 56844.8/2681
ch#  4 voltage7     vrefn : -2.92969/-4
ch#  5 voltage1    vccaux : 1794.43/2450
ch#  6 voltage2   vccbram : 993.896/1357
ch#  7 voltage3   vccpint : 993.896/1357
ch#  8 voltage6     vrefp : 1245.85/1701
LOG (  1747 )( 18:47:31:053 )  cIIOdevice: 9.51544Khz
Timer(iio-latency9)): <t> = 0.945831ms, std = 0.0389382ms, 0.897736ms <= t <= 1.20959ms (n=1024)
```

Measures are iterated on 1024 times acquiring the nine available channels on the default configuration
