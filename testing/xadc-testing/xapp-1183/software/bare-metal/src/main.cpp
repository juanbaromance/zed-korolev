﻿#include <iostream>

#include <bsp/xplatform_info.h>
#include "bsp/FreeRTOS.h"
#include "bsp/task.h"

#include "cXBareMetalDMA.h"
#include "cXADCps.h"

template<const u16 id=100>
static void system_watchdog(void *)
{
    int counter{0};
    while( 1 )
    {
        std::cout << __FUNCTION__ << ": " << std::dec << counter++ << "\n";
        vTaskDelay( TickType_t{ 1000 / portTICK_PERIOD_MS} );
    }
}

int main()
{
    printf("Hello World on platform.id(%x)\n\r", XGetPlatform_Info());
    cXdmaAXI().autotest();
    cXdmaPS().autotest();
    cXadcPS().autotest().run();

    xTaskCreate( system_watchdog, "Sane", configMINIMAL_STACK_SIZE, nullptr, 1, nullptr );
    vTaskStartScheduler();
    for( ;; );
    printf("%s(WTF)\n", __PRETTY_FUNCTION__ );
    return 0;

}
